## Introduction

The objective is create a generic encrypted ledger using Amazon QLDB, the current use is in a personal finance manager. The application 

The server is built in java, it's a REST API and a GraphQL Server, the server current stores the data on memory, but is going to interact with the database throught lambdas.

The front end application is a React/Next application using redux.

Also a sample graphql server using Apollo, used to test the frontend application without running the java server.

