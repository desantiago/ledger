import en from "./en.json";
import sp from "./sp.json";

const translations = {
  en,
  sp,
};

export default translations;
