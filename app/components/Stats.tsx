import { connect } from 'react-redux';
import { Row, Col} from 'antd';
import styled, { css } from 'styled-components'

var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  
    // These options are needed to round to whole numbers if that's what you want.
    //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
    //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
});
  
const Stats = ({totalData}) => {

    // console.log(totalData);

    if (totalData.totalDebit === 0) {
        return (
            <StatsContainer>
                <Row>
                    <Col span={8}>
                        <TextAmount2>Total :</TextAmount2>
                    </Col>
                    <ColRight span={16}>
                        <Amount2 >{formatter.format(totalData.totalCredit)}</Amount2>
                    </ColRight>
                </Row>
            </StatsContainer>
        )
    }

    let blue = undefined;
    let red = 'red';
    let total = 0;

    if (totalData.total < 0) {
        blue = undefined;
        red = 'red';
        total = totalData.total * -1;
    }
    else {
        blue = 'blue';
        red = undefined;
        total = totalData.total;
    }

    return (
        <StatsContainer>
            <Row>
                <Col span={8}>
                    <TextAmount2>Total Credit:</TextAmount2>
                </Col>
                <ColRight span={16}>
                    <Amount2>{formatter.format(totalData.totalCredit)}</Amount2>
                </ColRight>
            </Row>
            <Row>
                <Col span={8}>
                    <TextAmount2>Total Debit:</TextAmount2>
                </Col>
                <ColRight span={16}>
                    <Amount2>{formatter.format(totalData.totalDebit)}</Amount2>
                </ColRight>
            </Row>
            <Row>
                <Col span={8}>
                    <TextAmount2>Total :</TextAmount2>
                </Col>
                <ColRight span={16}>
                    <Amount2 blue={blue} red={red}>{formatter.format(total)}</Amount2>
                </ColRight>
            </Row>
        </StatsContainer>
    )
}

const StatsContainer = styled.div`
    padding-right: 1.0rem;
    padding-left: 1.0rem;
`;

// const TextAmount = styled.span`

//     font-family: Poppins; 
//     font-size:40px; 
//     color: #cbcbcb;
//     text-align: left;
// `;

const TextAmount2 = styled.div`
    margin-top: 1.0rem;
    font-family: Poppins; 
    font-size:20px; 
    color: #ababab;
    text-align: left;
`;

const Amount = styled.h2`

    font-family: Poppins; 
    font-size:40px; 
    color: #595959;
    text-align: right;

  /* background: transparent;
  border-radius: 3px;
  border: 2px solid palevioletred;
  color: palevioletred;
  margin: 0 1em;
  padding: 0.25em 1em; */

  /* ${props =>
    props.primary &&
    css`
      background: palevioletred;
      color: white;
    `}; */
`

const Amount2 = styled.span`
    font-family: Poppins; 
    font-size:40px; 
    color: #595959;

    ${props => props.red && css`color: #da0c2e;`}; 
    ${props => props.blue && css`color: #0c76da;`}; 
`
// https://medium.com/javascript-in-plain-english/the-modern-way-to-style-with-styled-components-c3c51b750b5f
const ColRight = styled(Col)`
    text-align: right;
`

const mapStateToProps = state => state;

export default connect(mapStateToProps, null)(Stats);
