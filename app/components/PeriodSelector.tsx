import { Select, DatePicker, Space, List, Skeleton, Divider } from 'antd';
import { useState } from 'react';
import styled, { css } from 'styled-components'
import { useIntl, FormattedMessage } from "react-intl";
import moment from 'moment';

import { Options } from '../data/Contants';
import { PeriodRange } from '../data/Periods';
import { PERIOD, RANGE } from '../data/Contants';

const { RangePicker } = DatePicker;

type PeriodSelectorProps = {
    selectedPeriod: string;
    selectedPeriodType: string;
    onChange: (type: string, value: any, ) => void; 
}

interface PeriodText {
    id: string;
    text: string;
}

const PeriodSelector = ({ selectedPeriod, onChange, selectedPeriodType }: PeriodSelectorProps) => {
    // const [ period, setPeriod ] = useState(selectedPeriod);
    // const { Option } = Select;
    const intl = useIntl();
    const dateFormat = 'YYYY/MM/DD';

    // const setOption = (option: string) => {
    //     setPeriod(option);
    //     onChange(PERIOD, option);
    // }

    const onChangeRange = (dates, dateStrings) => {
        const rangePeriod: PeriodRange = {
            start: dates[0].startOf('day'),
            end: dates[1].endOf('day')
        };
        onChange(RANGE, rangePeriod);
    }

    const changePeriod = (e) => {
        const periodId = e.currentTarget.getAttribute('data-id');
        onChange(PERIOD, periodId);
    }

    // const propsPeriod = {
    //     ...( selectedPeriodType === PERIOD && { style: {fontWeight: 'bold'} } )
    // };

    // const propsRange = {
    //     ...( selectedPeriodType === RANGE && { style: {fontWeight: 'bold'} } )
    // };
    // data: DataPoint[];
    // const lista: PeriodText[] = [];
    // let fruits: string[] = ['Apple', 'Orange', 'Banana'];


    const listb: PeriodText[] = Options.map(option => {return { 
        id: option.id,
        text: intl.formatMessage({
            id: option.id,
            defaultMessage: '',
        })
    }});

    return (
            <Space direction="vertical" >
            {/* <span {...propsPeriod}>Select Period:</span> */}
            <PeriodTitle active={selectedPeriodType === PERIOD}>
                <FormattedMessage id="periodSelectorSelectPeriod" defaultMessage="Select Period" />:
            </PeriodTitle>

            {/* <Select onChange={(e) => setOption(e.toString())} value={period} style={{width: '100%'}} >
                {
                    Options.map(option => 
                        <Option value={option.id} key={option.id}>{ option.text }</Option>
                    )
                }
            </Select> */}

            <List
                className="demo-loadmore-list"
                loading={false}
                itemLayout="horizontal"
                // loadMore={loadMore}
                dataSource={listb}
                renderItem={
                    item => {
                        const propsList = {
                            ...( selectedPeriodType === PERIOD && item.id === selectedPeriod && { style: {backgroundColor: '#eaeaea'} } )
                        };

                        return (
                            <List.Item
                                actions={[<a data-id={item.id} onClick={changePeriod}>Select</a>]}
                                {...propsList}
                            >
                                <Skeleton title={false} loading={false} active >
                                    <div>{item.text}</div>
                                </Skeleton>
                            </List.Item>
                        )
                    }
                }
            />

            <Divider/>

            <PeriodTitle active={selectedPeriodType === RANGE}>
                <FormattedMessage id="periodSelectorSelectRange" defaultMessage="Select Range" />:
            </PeriodTitle>

            {/* <br/>
            <span {...propsRange}>Select Range:</span> */}
            <RangePicker
                defaultValue={[moment(), moment()]}
                format={dateFormat}
                onChange={onChangeRange}
            />
            </Space>
    )
}

const PeriodTitle = styled.p`
    ${({ active }) => active && `
        font-weight: bold;
    `}
`

export default PeriodSelector;