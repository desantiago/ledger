import { Drawer, Button, Row, Col, Space } from 'antd';
import { connect } from 'react-redux';
import { useState } from 'react';
import { useIntl, FormattedMessage } from "react-intl";

import PeriodSelector from '../components/PeriodSelector';

import { CHANGE_PERIOD, SAVE_TEMP_RECORDS } from '../store/reducer';
import { Options, PERIOD, RANGE } from '../data/Contants';

import {
    CalendarOutlined
} from '@ant-design/icons';

const CurrentPeriod = ({selectedPeriod, selectedRange, changePeriod, selectedPeriodType, saveTemporalRecords}) => {
    const intl = useIntl();
    const placement = 'right';
    const [ visible, setVisible ] = useState(false);

    const showDrawer = () => {
        setVisible(true);
    };
  
    const onClose = () => {
        setVisible(false);
    };

    const onChangePeriod = (type: string, value: any) => {
        // console.log("Period Selected", type);
        changePeriod(type, value);
    };

    // const onSave = () => {
    //     saveTemporalRecords();
    // }

    const getText = (selectedPeriodType, selectedPeriod) => {
        if (selectedPeriodType === PERIOD) {
            // const elements = Options.filter(option => option.id === selectedPeriod);
            // if (elements.length > 0) {
                //return `Showing ${elements[0].text} Records`;

            const periodText = intl.formatMessage({
                id: selectedPeriod,
                defaultMessage: '',
            });

            return intl.formatMessage({
                id: "recordsPeriod",
                defaultMessage: `Showing ${periodText} Records`,
            },
            {
                period: periodText
            });
            // }
        }
        else if (selectedPeriodType === RANGE) {
            const start = selectedRange.start;
            const end = selectedRange.end;
            // return `Showing Records from ${start.format('MM-DD-YYYY')} to ${end.format('MM-DD-YYYY')}`
            return intl.formatMessage({
                id: "recordsRange",
                defaultMessage: `Showing Records from ${start.format('MM-DD-YYYY')} to ${end.format('MM-DD-YYYY')}`,
            },
            {
                startDate: start.format('MM-DD-YYYY'),
                endDate: end.format('MM-DD-YYYY')
            });
        }
        return '';
    };

    const drawerTitle = intl.formatMessage({
        id: 'drawerChangePeriodTitle',
        defaultMessage: 'Change Period',
    });

    return (
        <>
            {getText(selectedPeriodType, selectedPeriod)}
            &nbsp;<a onClick={showDrawer}><CalendarOutlined/></a>

            <Drawer
                title={drawerTitle}
                placement={placement}
                closable={true}
                onClose={onClose}
                visible={visible}
                key={placement}
                width={320}
            >
                <PeriodSelector
                    selectedPeriodType={selectedPeriodType}
                    selectedPeriod={selectedPeriod}
                    onChange={onChangePeriod}/>
            </Drawer>
        </>
    )
}

const mapStateToProps = state => state;

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        changePeriod: (type, value) => dispatch({ type: CHANGE_PERIOD, payload: { type, value } }),
        saveTemporalRecords: () => dispatch({type: SAVE_TEMP_RECORDS, payload: {} })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CurrentPeriod);