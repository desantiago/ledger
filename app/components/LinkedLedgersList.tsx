import { Row, Col, Tag } from 'antd';
import styled, { css } from 'styled-components'
import { LinkedRecord } from '../data/Types';

interface Props {
    linkedRecords: LinkedRecord[],
    tagColors: {},
}

const LinkedLedgersList: React.FC<Props> = ({ tagColors, linkedRecords }: Props) => {
    return (
        <>
        { linkedRecords.map(linkedRecord => {
            return (
                <>
                    <RowLedger>
                    <Col span="8">{linkedRecord.ledgerName}</Col>
                    <Col span="12">{linkedRecord.description}</Col>
                    <Col span="4"><Tag color={tagColors[linkedRecord.category]}>{linkedRecord.category}</Tag></Col>
                    </RowLedger>
                </>
            )
          })
        }
        </>
    )
}

export default LinkedLedgersList;

const RowLedger = styled(Row)`
    padding-top: 2px;
    padding-bottom: 2px;
`
const RowDivider = styled.div`
    display: flex;
    clear: both;
    width: 100%;
    min-width: 100%;
    border-top: 1px solid #f0f0f0;
`