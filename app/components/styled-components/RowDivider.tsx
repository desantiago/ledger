import styled from 'styled-components';

const RowDivider = styled.div`
    display: flex;
    clear: both;
    width: 100%;
    min-width: 100%;
    border-top: 1px solid #f0f0f0;
`

export default RowDivider;