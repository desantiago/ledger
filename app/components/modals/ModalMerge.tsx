import { useEffect, useState, useCallback } from 'react'

import { Modal, Row, Col, Form, Select, Button, Input, Checkbox, DatePicker } from 'antd';
import moment from 'moment';

import Data from '../../data/Data';
import { Ledger } from '../../data/Types';

interface ModalMergeProps {
    visibleModalAdd: boolean;
    modalAddOk: Function;
    modalAddCancel: Function;
    ledgers: Ledger[];
    // categories: string[];
    selectedLedger: string;
    ledgersToRelate: Ledger[];
}

const { Option } = Select;

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

const ModalMerge: React.FC<ModalMergeProps> = ({
    visibleModalAdd, 
    modalAddOk, 
    modalAddCancel, 
    ledgers, 
    // categories, 
    selectedLedger, 
    ledgersToRelate
}: ModalMergeProps) => {

    const [form] = Form.useForm();
    const [nameLedgerToAdd, setNameLedgerToAdd] = useState('');
    // const [totalLedger, setTotalLedger] = useState(0);
    const [ledgerKey, setLedgerKey] = useState('');
    const [nameSelLedger, setNameSelLedger] = useState('');
    // const [closeCheck, setCloseCheck] = useState(false);

    const data = Data.getInstance();

    useEffect(() => {
        setNameSelLedger(ledgers.find(ledger => ledger.key === selectedLedger).name);
        if (ledgersToRelate[0]) { 
            setLedgerKey(ledgersToRelate[0].key);
            // setValues(ledgersToRelate[0].key) 
        };
    }, [ledgersToRelate, selectedLedger]);

    // useEffect(() => {
    //     form.setFieldsValue({
    //         date: moment(),
    //         close: false
    //     });
    //     setCloseCheck(false);
    // }, []);

    const modalOk = (values) => {
        values.ledgerKey = ledgerKey;
        // values.total = totalLedger;
        modalAddOk(values);
    }
    
    const modalCancel = () => {
        modalAddCancel();
    }

    const onChange = (value) => {
        setLedgerKey(value);
        // setValues(value);
    }

    // const setValues = (value) => {
    //     const total = data.getTotalAllRecords(value);
    //     setNameLedgerToAdd(ledgersToRelate.filter(ledger => ledger.key === value)[0].name);
    //     setTotalLedger(total.total);
    // }

    // const onSearch = (val) => {
    //     console.log('search:', val);
    // }

    // const onChangeClose = (e) => {
    //     // console.log("onChangeClose ", e);
    //     setCloseCheck(e.target.checked);
    // }

    // setValues(otherLedgers[0].key);

    return (
        <Modal
            visible={visibleModalAdd}
            title="Merge Ledgers"
            // width={700}
            //onOk={modalOk}            
            onCancel={modalCancel}
            okText="Merge"
            cancelText="Cancel"
            onOk={() => {
                form
                    .validateFields()
                    .then(values => {
                        form.resetFields();
                        form.setFieldsValue({
                            ledgerKey: ''
                        });
                        modalOk(values);
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info);
                    });
            }}
        >
            <Form initialValues={{ close: true, name: moment() }} form={form} name="control-hooks" {...layout}>
                <Form.Item name="ledgerKey" label="Ledger To Merge" rules={[{ required: true }]}>
                    <Select
                        // showSearch
                        style={{ width: 250 }}
                        placeholder="Select a ledger"
                        onChange={onChange}
                    >
                        {
                            ledgersToRelate.map(ledger => <Option key={ledger.key} value={ledger.key}>{ledger.name}</Option> )
                        }
                    </Select>
                </Form.Item>
                { nameLedgerToAdd && <p>The result of {nameLedgerToAdd} of will be added as a record to {nameSelLedger}</p> }
            </Form>
        </Modal>
    )
}

export default ModalMerge;