import { useEffect, useState, useCallback } from 'react'
import { Modal, Row, Col, Form, Input} from 'antd';
import styled, { css } from 'styled-components'

import Data from '../../data/Data';

interface ModalAddNewLedgerProps {
    visibleModalAdd: boolean;
    modalAddOk: Function;
    modalAddCancel: Function;
}

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

// const tailFormItemLayout = {
//     wrapperCol: {
//         xs: {
//             span: 24,
//             offset: 0,
//         },
//         sm: {
//             span: 16,
//             offset: 8,
//         },
//     },
// };

const ModalAddNewLedger: React.FC<ModalAddNewLedgerProps> = ({
    visibleModalAdd, 
    modalAddOk, 
    modalAddCancel, 
}: ModalAddNewLedgerProps) => {

    const [form] = Form.useForm();

    // const data = Data.getInstance();
    // const colors = Object.values(data.getAvailableColorsName());

    const modalOk = (values) => {
        // values.color = selectedColor;
        modalAddOk(values);
    }
    
    const modalCancel = () => {
        modalAddCancel();
    }

    // const onChangeClose = (e) => {
    //     console.log("onChangeClose ", e);
    // }

    // const [ selectedColor, setSelectedColor ] = useState(colors[0]);

    // const click = (e) => {
    //     const key = e.currentTarget.getAttribute('data-key');
    //     setSelectedColor(key);
    // }

    return (
        <Modal
            visible={visibleModalAdd}
            title="Add New Ledger"
            // width={700}
            //onOk={modalOk}
            onCancel={modalCancel}
            okText="Add"
            cancelText="Cancel"
            onOk={() => {
                form
                    .validateFields()
                    .then(values => {
                        form.resetFields();
                        form.setFieldsValue({
                            name:'',
                            description: ''
                        });
                        modalOk(values);
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info);
                    });
            }}
        >
            <Form 
                initialValues={{ name: '', description: '' }} 
                form={form} 
                name="control-hooks" 
                {...layout}>

                <Form.Item name="name" label="Ledger Name" rules={[{ required: true }]}>
                    <Input/>
                </Form.Item>
                <Form.Item name="description" label="Description">
                    <Input.TextArea />
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default ModalAddNewLedger;

// const CategoryColor = styled.span`
//     cursor: pointer;
//     background-color: ${props => props.color || "#000"};
//     width: 20px;
//     height: 20px;
//     display: block;

//     ${props => props.selected && css`
//         border: 2px solid #989898;
//     `}    
// `