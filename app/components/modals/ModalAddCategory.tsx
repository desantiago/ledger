import { useEffect, useState, useCallback } from 'react'
import { Modal, Row, Col, Form, Input} from 'antd';
import styled, { css } from 'styled-components'

import Data from '../../data/Data';
// import { Ledger } from '../data/Types';

interface ModalAddLedgerProps {
    visibleModalAdd: boolean;
    modalAddOk: Function;
    modalAddCancel: Function;
}

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

const ModalAddCategory: React.FC<ModalAddLedgerProps> = ({
    visibleModalAdd, 
    modalAddOk, 
    modalAddCancel, 
}: ModalAddLedgerProps) => {

    const [form] = Form.useForm();

    const data = Data.getInstance();
    const colors = Object.values(data.getAvailableColorsName());

    const modalOk = (values) => {
        values.color = selectedColor;
        modalAddOk(values);
    }
    
    const modalCancel = () => {
        modalAddCancel();
    }

    const onChangeClose = (e) => {
        console.log("onChangeClose ", e);
        // setCloseCheck(e.target.checked);
    }

    const [ selectedColor, setSelectedColor ] = useState(colors[0]);

    const click = (e) => {
        const key = e.currentTarget.getAttribute('data-key');
        setSelectedColor(key);
    }

    return (
        <Modal
            visible={visibleModalAdd}
            title="Add New Category"
            width={700}
            //onOk={modalOk}
            onCancel={modalCancel}
            okText="Add"
            cancelText="Cancel"
            onOk={() => {
                form
                    .validateFields()
                    .then(values => {
                        form.resetFields();
                        form.setFieldsValue({
                            name:''
                        });
                        setSelectedColor(colors[0]);
                        modalOk(values);
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info);
                    });
            }}
        >
            <Form 
                initialValues={{ name: '' }} 
                form={form} 
                name="control-hooks" 
                {...layout}>

                <Form.Item name="name" label="Category Name" rules={[{ required: true }]}>
                    <Input/>
                </Form.Item>
                <Form.Item  {...tailFormItemLayout}>
                    <Row>
                    {
                        colors.map(color => {
                            return (
                            <Col span="2" key={color+"col"}>
                                <CategoryColor 
                                    key={color}
                                    data-key={color} 
                                    onClick={click} 
                                    color={color} 
                                    selected={color === selectedColor ? true : false}
                                />
                            </Col>
                            )
                        })
                    }
                    </Row>

                </Form.Item>
            </Form>
        </Modal>
    )
}

export default ModalAddCategory;

const CategoryColor = styled.span`
    cursor: pointer;
    background-color: ${props => props.color || "#000"};
    width: 20px;
    height: 20px;
    display: block;

    ${props => props.selected && css`
        border: 2px solid #989898;
    `}    
`