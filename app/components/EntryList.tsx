import React, { useState, useEffect, useRef } from 'react';
// import { VariableSizeGrid as Grid } from 'react-window';
// import ResizeObserver from 'rc-resize-observer';
// import classNames from 'classnames';
import { Table, Space, Button, Tag, Modal  } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import styled, { css } from 'styled-components'

import { connect } from 'react-redux';
// import PeriodSelector from './PeriodSelector';

import { EDIT_TEMP_RECORD, DELETE_TEMP_RECORD, CLONE_RECORD, CANCEL_RECORD } from '../store/reducer';
import { STATUS_ACTIVE, STATUS_CANCELED, STATUS_RELATED } from '../data/Contants';

import moment from 'moment';

//import * as Period from '../data/Periods';

// function VirtualTable(props) {
//   const { columns, scroll } = props;
//   const [tableWidth, setTableWidth] = useState(0);

//   const widthColumnCount = columns.filter(({ width }) => !width).length;
//   const mergedColumns = columns.map(column => {
//     if (column.width) {
//       return column;
//     }

//     return {
//       ...column,
//       width: Math.floor(tableWidth / widthColumnCount),
//     };
//   });

//   const gridRef = useRef<any>();
//   const [connectObject] = useState<any>(() => {
//     const obj = {};
//     Object.defineProperty(obj, 'scrollLeft', {
//       get: () => null,
//       set: (scrollLeft: number) => {
//         if (gridRef.current) {
//           gridRef.current.scrollTo({ scrollLeft });
//         }
//       },
//     });

//     return obj;
//   });

//   const resetVirtualGrid = () => {
//     gridRef.current  &&
//     gridRef.current.resetAfterIndices({
//       columnIndex: 0,
//       shouldForceUpdate: false,
//     });
//   };

//   useEffect(() => resetVirtualGrid, [tableWidth]);

//   const renderVirtualList = (rawData: object[], { scrollbarSize, ref, onScroll }: any) => {
//     ref.current = connectObject;
//     const totalHeight = rawData.length * 54;

//     return (
//       <Grid
//         ref={gridRef}
//         className="virtual-grid"
//         columnCount={mergedColumns.length}
//         columnWidth={index => {
//           const { width } = mergedColumns[index];
//           return totalHeight > scroll.y && index === mergedColumns.length - 1
//             ? width - scrollbarSize - 1
//             : width;
//         }}
//         height={scroll.y}
//         rowCount={rawData.length}
//         rowHeight={() => 54}
//         width={tableWidth}
//         onScroll={({ scrollLeft }) => {
//           onScroll({ scrollLeft });
//         }}
//       >
//         {({ columnIndex, rowIndex, style }) => (
//           <div
//             className={classNames('virtual-table-cell', {
//               'virtual-table-cell-last': columnIndex === mergedColumns.length - 1,
//             })}
//             style={style}
//           >
//             {rawData[rowIndex][mergedColumns[columnIndex].dataIndex]}
//           </div>
//         )}
//       </Grid>
//     );
//   };

//   return (
//     <ResizeObserver
//       onResize={({ width, height }) => {
//         console.log("Resizing: ", width, height);
//         setTableWidth(width);
//       }}
//     >
//       <div style={{border:"1px solid #343434"}}>
//         <Table
//           {...props}
//           className="virtual-table"
//           columns={mergedColumns}
//           pagination={false}
//           components={{
//             body: renderVirtualList,
//           }}
//         />
//       </div>
//     </ResizeObserver>
//   );
// }

var formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',

  // These options are needed to round to whole numbers if that's what you want.
  //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
  //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
});


// const data = [
//   {
//     key: '1',
//     name: 'John Brown',
//     age: 32,
//     address: 'New York No. 1 Lake Park',
//     tags: ['nice', 'developer'],
//   },
//   {
//     key: '2',
//     name: 'Jim Green',
//     age: 42,
//     address: 'London No. 1 Lake Park',
//     tags: ['loser'],
//   },
//   {
//     key: '3',
//     name: 'Joe Black',
//     age: 32,
//     address: 'Sidney No. 1 Lake Park',
//     tags: ['cool', 'teacher'],
//   },
// ];

// Usage
// const columns = [
//   { title: 'A', dataIndex: 'key', width: 100 },
//   { title: 'B', dataIndex: 'key', width: 100 },
//   { title: 'C', dataIndex: 'key', width: 100 },
//   { title: 'D', dataIndex: 'key', width: 100 },
//   { title: 'E', dataIndex: 'key', width: 100 },
//   { title: 'F', dataIndex: 'key', width: 100 },
// ];

// const data = Array.from({ length: 100000 }, (_, key) => ({ key }));

// <VirtualTable columns={columns} dataSource={data} scroll={{ y: 800, x: '100vw' }} />

const EntryList = ({ records, editTempRecord, deleteTempRecord, cloneRecord, cancelRecord }) => {

  const [ pageSize, setPageSize ] = useState(15);
  // const [ visible, setVisible ] = useState(false);

  // let selectedKey = '';

  // const onConfirm = () => {
  //   cancelRecord(selectedKey)    
  //   // setVisible(false);
  // }

  // const onCancel = () => {
  //   // setVisible(false);
  // }

  const columns = [
    {
      title: '',
      key: 'temp',
      dataIndex: 'temp',
      width: 50,
      render: temp => {
        if (temp) {
          return (
            <Tag color="geekblue" key="P">&nbsp;</Tag>
          )
        }
        else {
          return <></>
        }
      }
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
      render: (text, record) => {
        if (record.status === STATUS_ACTIVE) {
          return (<a>{text}</a>)
        }
        else {
          return (<a style={{color:'red'}}>{text}</a>)
        }
      },
      width: 300
    },
    {
      title: 'Category',
      dataIndex: 'category',
      key: 'category',
      width: 100,
    },
    {
      title: 'Type',
      dataIndex: 'type',
      key: 'type',
      width: 100,
    },
    {
      title: 'Date',
      dataIndex: 'date',
      key: 'date',
      width: 200,
      render: date => <span>{date.format('LL')}</span>
    },
    {
      title: 'Amount',
      dataIndex: 'amount',
      key: 'amount',
      className: 'column-money',
      align: 'right',
      render: (amount, record) => {
        if (record.status === STATUS_ACTIVE) {
          return (<span>{formatter.format(amount)}</span>)
        }
        else {
          return (<CancelText>{formatter.format(amount)}</CancelText>)
        }
      }
    },
    // {
    //   title: 'Tags',
    //   key: 'tags',
    //   dataIndex: 'tags',
    //   render: tags => (
    //     <>
    //       {tags.map(tag => {
    //         let color = tag.length > 5 ? 'geekblue' : 'green';
    //         if (tag === 'loser') {
    //           color = 'volcano';
    //         }
    //         return (
    //           <Tag color={color} key={tag}>
    //             {tag.toUpperCase()}
    //           </Tag>
    //         );
    //       })}
    //     </>
    //   ),
    // },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => {
        if (record.temp) {
          return (
            <Space size="middle">
              <a data-key={record.key} onClick={onEditTempRecord}>Edit</a>
              <a data-key={record.key} onClick={onDeleteTempRecord}>Delete</a>
            </Space>
          )
        }
        else {
          if (record.status === STATUS_ACTIVE) {
            // console.log("record key", record.key);
            return (
              <Space size="middle">
                <a data-key={record.key} onClick={showPopconfirm}>Cancel</a>
                <a data-key={record.key} onClick={onCloneRecord}>Clone</a>
              </Space>
            )
          }
          else {
            return (
              <Space size="middle">
                <a data-key={record.key} onClick={onCloneRecord}>Clone</a>
              </Space>
            )
          }
        }
      }
    },  
  ]; 

  const change = () => {
    setPageSize(5);
  }

  const change2 = () => {
    // const pageSize = (15 * window.innerHeight) / 1000;    
    const pageSize = ((window.innerHeight - 55 - 145) / 55);
    console.log(window.innerHeight, pageSize);
    setPageSize(pageSize);
  }

  const onDeleteTempRecord = (e) => {
    const key = e.currentTarget.getAttribute('data-key');
    // console.log(key);
    deleteTempRecord(key);
  }
  
  const onEditTempRecord = (e) => {
    const key = e.currentTarget.getAttribute('data-key');
    // console.log(key);
    //const record = records.find(record => record.key === key);
    editTempRecord(key);
  }

  const onCloneRecord = (e) => {
    const key = e.currentTarget.getAttribute('data-key');
    cloneRecord(key);
  }

  const showPopconfirm = (e) => {
    const key = e.currentTarget.getAttribute('data-key');
    //cancelRecord(key);
    // selectedKey = key;
    //setVisible(true);
    Modal.confirm({
      title: 'Confirm',
      icon: <ExclamationCircleOutlined />,
      content: 'Are you sure you want to cancel this record?',
      okText: 'Yes',
      cancelText: 'No',
      onOk() {
        // console.log('OK');
        cancelRecord(key);
      },      
    });
  }
  
  // console.log(moment(), moment().add(2, 'second'));
  // console.log("Records :", records);

  return (
    <>
      <Table columns={columns} dataSource={records} pagination={{ pageSize: pageSize, position: ['none', 'bottomRight'] }} scroll={{ y: 'calc(100vh - 16em)' }}/>
      <Button onClick={change}>Change</Button>
      <Button onClick={change2}>Change2</Button>
      <pre className="language-bash">{JSON.stringify(records, null, 2)}</pre>
    </>
  )
};

const mapStateToProps = state => state;

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    editTempRecord: (record) => dispatch({type: EDIT_TEMP_RECORD, payload: record}),
    deleteTempRecord: (key) => dispatch({type: DELETE_TEMP_RECORD, payload: key}),
    cloneRecord: (key) => dispatch({type: CLONE_RECORD, payload: key}),
    cancelRecord: (key) => dispatch({type: CANCEL_RECORD, payload: key}),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EntryList);

// https://masqueprogramar.wordpress.com/2018/01/09/tachar-texto-con-css/
const CancelText = styled.span`
  position: relative;
  &:before {
    position: absolute;
    content: "";
    left: 0;
    top: 50%;
    right: 0;
    border-top: 1px solid;
    border-color: red;
     
    -webkit-transform:rotate(-10deg);
    -moz-transform:rotate(-10deg);
    -ms-transform:rotate(-10deg);
    -o-transform:rotate(-10deg);
    transform:rotate(-10deg);
  }
`
