import { Drawer, Button, Row, Col, Space } from 'antd';
import { connect } from 'react-redux';
import { useState } from 'react';

import PeriodSelector from '../components/PeriodSelector';

import { CHANGE_PERIOD, SAVE_TEMP_RECORDS } from '../store/reducer';
import { Options, PERIOD, RANGE } from '../data/Contants';

const CurrentPeriod = ({selectedPeriod, selectedRange, changePeriod, selectedPeriodType, saveTemporalRecords}) => {

    const placement = 'right';
    const [ visible, setVisible ] = useState(false);

    const showDrawer = () => {
        setVisible(true);
    };
  
    const onClose = () => {
        setVisible(false);
    };

    const onChangePeriod = (type: string, value: any) => {
        console.log("Period Selected", type);
        changePeriod(type, value);
    };

    const onSave = () => {
        saveTemporalRecords();
    }

    const getText = (selectedPeriodType, selectedPeriod) => {
        if (selectedPeriodType === PERIOD) {
            const elements = Options.filter(option => option.id === selectedPeriod);
            if (elements.length > 0) {
                return `Showing ${elements[0].text} Records`;
            }
        }
        else if (selectedPeriodType === RANGE) {
            const start = selectedRange.start;
            const end = selectedRange.end;
            return `Showing Records from ${start.format('MM-DD-YYYY')} to ${end.format('MM-DD-YYYY')}`
        }

        return '';
    };

    return (
        <>
            <Row>
                <Col>
                    <h3>{getText(selectedPeriodType, selectedPeriod)}</h3> 
                </Col>
                <Col>
                    <Button onClick={showDrawer}>Change</Button>
                </Col>
                {/* <Col>
                    <Button onClick={onSave}>Save Temp Records</Button>
                </Col> */}
            </Row>

            <Drawer
                title="Change Period"
                placement={placement}
                closable={true}
                onClose={onClose}
                visible={visible}
                key={placement}
                width={320}
            >
                <PeriodSelector
                  selectedPeriodType={selectedPeriodType}
                  selectedPeriod={selectedPeriod}
                  onChange={onChangePeriod}/>
            </Drawer>
        </>
    )
}

const mapStateToProps = state => state;

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        changePeriod: (type, value) => dispatch({ type: CHANGE_PERIOD, payload: { type, value } }),
        saveTemporalRecords: () => dispatch({type: SAVE_TEMP_RECORDS, payload: {} })
    }
}
  
export default connect(mapStateToProps, mapDispatchToProps)(CurrentPeriod);
