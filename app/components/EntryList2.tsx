import React from "react";
import { useEffect, useState } from 'react'

import { Row, Col, Tag, Button, Modal } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';

import styled, { css } from 'styled-components'
import { connect } from 'react-redux';
import { FormattedMessage } from "react-intl";

import { SampleRecords } from '../data/TestData';
import { STATUS_ACTIVE, STATUS_CANCELED, STATUS_RELATED, STATUS_TEMP, STATUS_RESULT, STATUS_MERGED } from '../data/Contants';
import { EDIT_TEMP_RECORD, DELETE_TEMP_RECORD, CLONE_RECORD, CANCEL_RECORD, VIEW_RELATED_LEDGER } from '../store/reducer';

import { Ledger } from "../data/Types";

import RowDivider from './styled-components/RowDivider';
import CancelText from './styled-components/CancelText';

// const colors = {
//     'Home': 'blue',
//     'Food': 'green',
//     'Gas' : 'orange',
//     'Entertainment' : 'magenta'
// };

var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    // These options are needed to round to whole numbers if that's what you want.
    //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
    //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
});

const EntryList2 = ({ records, editTempRecord, deleteTempRecord, cloneRecord, cancelRecord, viewLedger, tagColors, selectedLedger, ledgers }) => {

    const [ ledgerData, setLedgerData ] = useState<Ledger>({
        key: '',
        name: '',
        timeStamp: null,
        closed: false
    });

    useEffect(() => {
        const dataledger = ledgers.find(ledger => ledger.key === selectedLedger);
        setLedgerData(dataledger);
    }, [selectedLedger]);

    const onDeleteTempRecord = (e) => {
        const key = e.currentTarget.getAttribute('data-key');
        // console.log(key);
        deleteTempRecord(key);
    }
    
    const onEditTempRecord = (e) => {
        const key = e.currentTarget.getAttribute('data-key');
        // console.log(key);
        editTempRecord(key);
    }

    const onCloneRecord = (e) => {
        const key = e.currentTarget.getAttribute('data-key');
        cloneRecord(key);
    }
    
    const onView = (e) => {
        const key = e.currentTarget.getAttribute('data-key');
        viewLedger(key);
    }

    const showPopconfirm = (e) => {
        const key = e.currentTarget.getAttribute('data-key');
        //cancelRecord(key);
        // selectedKey = key;
        //setVisible(true);
        Modal.confirm({
            title: 'Confirm',
            icon: <ExclamationCircleOutlined />,
            content: 'Are you sure you want to cancel this record?',
            okText: 'Yes',
            cancelText: 'No',
            onOk() {
                // console.log('OK');
                cancelRecord(key);
            },      
        });
    }

    return  (
        <>
        {
            records.map(record => {
                return (
                    <>
                        <RowLedger key={record.key} styled={{pop: record.temp}}>
                            <Col span={10}>
                                <DescriptionValue>{record.description}</DescriptionValue> <br/>
                                { record.ledgerNameRelated && <LedgerName>{record.ledgerNameRelated}</LedgerName> }
                                { record.ledgerRelated && !record.closed && record.status === STATUS_RESULT && <span> Updating</span> }
                            </Col>
                            <Col span={4}><Tag color={tagColors[record.category]}>{record.category}</Tag></Col>
                            <Col span={3}><DateValue>{record.date.format('MM-DD-YYYY')}</DateValue></Col>
                            <ColRight span={3}>
                                {(record.status === STATUS_TEMP || record.status === STATUS_ACTIVE || record.status === STATUS_RESULT || record.status === STATUS_MERGED) && <Value>{formatter.format(record.amount)}</Value>}
                                {(record.status === STATUS_CANCELED || record.status === STATUS_RELATED) && <Value><CancelText>{formatter.format(record.amount)}</CancelText></Value>}
                            </ColRight>
                            <ColRight span={4}>
                                {
                                    !ledgerData.closed && record.status === STATUS_TEMP && <>
                                    <ActionButton data-key={record.key} onClick={onEditTempRecord}>
                                        <FormattedMessage id="recordOptionClone" defaultMessage="Clone" />
                                    </ActionButton>&nbsp;
                                    <ActionButton data-key={record.key} onClick={onDeleteTempRecord}>
                                        <FormattedMessage id="recordOptionDelete" defaultMessage="Delete" />
                                    </ActionButton>
                                    </>
                                }
                                {
                                    !ledgerData.closed && record.status === STATUS_ACTIVE && <>
                                    <ActionButton data-key={record.key} onClick={onCloneRecord}>
                                        <FormattedMessage id="recordOptionClone" defaultMessage="Clone" />
                                    </ActionButton>&nbsp;
                                    <ActionButton data-key={record.key} onClick={showPopconfirm}>
                                        <FormattedMessage id="recordOptionCancel" defaultMessage="Cancel" />
                                    </ActionButton>
                                    </>
                                }
                                {
                                    !ledgerData.closed && (record.status === STATUS_RELATED || record.status === STATUS_CANCELED) && <>
                                    <ActionButton data-key={record.key} onClick={onCloneRecord}>
                                        <FormattedMessage id="recordOptionClone" defaultMessage="Clone" />
                                    </ActionButton>
                                    </>
                                }
                                {
                                    !ledgerData.closed && record.status === STATUS_RESULT || record.status === STATUS_MERGED && <>
                                    <ActionButton data-key={record.ledgerRelated} onClick={onView}>
                                        <FormattedMessage id="recordOptionViewLedger" defaultMessage="View Ledger" />
                                    </ActionButton>
                                    </>
                                }
                            </ColRight>
                        </RowLedger>
                        <RowDivider key={record.key+"divider"}/>
                    </>
                )
            })
        }

        <pre className="language-bash">{JSON.stringify(records, null, 2)}</pre>

        </>
    )
        // <Row>
        //     <Col span="24" >
        //         <EntryForm />
        //     </Col>
        // </Row>    
}

const mapStateToProps = state => state;

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        editTempRecord: (record) => dispatch({type: EDIT_TEMP_RECORD, payload: record}),
        deleteTempRecord: (key) => dispatch({type: DELETE_TEMP_RECORD, payload: key}),
        cloneRecord: (key) => dispatch({type: CLONE_RECORD, payload: key}),
        cancelRecord: (key) => dispatch({type: CANCEL_RECORD, payload: key}),
        viewLedger: (key) => dispatch({type: VIEW_RELATED_LEDGER, payload: key}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EntryList2);

const LedgerName = styled.span`
    font-size:0.8rem;
    font-weight: 500;
`;

/* -------------------------------------------------- */

// const RowDivider = styled.div`
//     display: flex;
//     clear: both;
//     width: 100%;
//     min-width: 100%;
//     border-top: 1px solid #f0f0f0;
// `

// const CancelText = styled.span`
//   position: relative;
//   &:before {
//     position: absolute;
//     content: "";
//     left: 0;
//     top: 50%;
//     right: 0;
//     border-top: 1px solid;
//     border-color: red;
     
//     -webkit-transform:rotate(-10deg);
//     -moz-transform:rotate(-10deg);
//     -ms-transform:rotate(-10deg);
//     -o-transform:rotate(-10deg);
//     transform:rotate(-10deg);
//   }
// `

// const Circle = styled.span`
//     width: 6px;
//     height: 6px;
//     font-size: 20px;
//     color: #fff;
//     text-align: center;
//     line-height: 0;
//     border-radius: 50%;
//     background: #09f;
//     float: left;
//     margin-top: 0.5rem;
//     margin-right: 0.5rem;
// `

const ActionButton = styled(Button)`
    height: auto;
    padding: 0;
    font-size: 0.7rem;
    padding-left: 2px;
    padding-right: 2px;
`

const Value = styled.span`
    font-weight: bold;
`

const DateValue = styled.span`
    font-size:'0.7rem'; 
    margin-top: '0.1rem';
`

const DescriptionValue = styled.span`
    font-family: 'Poppins'
`
const RowLedger = styled(Row)`
    padding-top: 6px;
    padding-bottom: 6px;

    // ${({ temp }) => temp && `
    //     background: #f9f9f9;
    // `}

    ${({styled: {pop}}) => pop && `
        background: #f9f9f9;
    `}
`

const ColRight = styled(Col)`
    text-align: right;
`