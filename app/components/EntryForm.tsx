import { Form, Input, InputNumber, Button, Checkbox, Select, DatePicker, Row, Col  } from 'antd';
import { useState, useRef, useEffect } from 'react'
import moment from 'moment';

import { connect } from 'react-redux';
import { NextPageContext } from "next";
import { v4 as uuidv4 } from 'uuid';

import { ADD_CATEGORY, ADD_RECORD, NEW_RECORD_MODE, State } from "../store/reducer";
import { EDIT_RECORD, NEW_RECORD, STATUS_ACTIVE } from '../data/Contants';
import { Record } from '../data/Types';

const { Option } = Select;

const layout = {
  labelCol: { span: 5 },
  wrapperCol: { span: 17 },
};
const tailLayout = {
  wrapperCol: { offset: 5, span: 17 },
};

interface FieldData {
  name: string | number | (string | number)[];
  value?: any;
  touched?: boolean;
  validating?: boolean;
  errors?: string[];
}

interface CustomizedFormProps {
  onChange: (fields: FieldData[]) => void;
  fields: FieldData[];
  addRecord: any,
  addCategory: any,
  categories: [],
  editMode: string,
  newRecordMode: any,
  recordKey: string
}

// export default class EntryForm extends React.Component {
// const EntryForm = ({addRecord, addCategory, categories, editMode, editingRecord}) => {
const CustomizedForm: React.FC<CustomizedFormProps> = ({ onChange, fields, addRecord, addCategory, categories, editMode, newRecordMode, recordKey }) => {

  const [form] = Form.useForm();
  // console.log('CustomizeForm key', recordKey);
  // let defaultOptions = categories;
  const typeOptions = ['Credit', 'Debit'];
  // const [ description, setDescription ] = useState('');
  const [ category, setCategory ] = useState('');
  const [ type, setType ] = useState('');
  // const [ amount, setAmount ] = useState(0);
  // const [ date, setDate] = useState(moment());
  // const [ newCategory, setNewCategory] = useState('');
  const [ showNewCategory, setShowNewCategory] = useState(false);

  let descriptionInput = useRef(null);
  let newCategoryInput = useRef(null);

  useEffect(() => {
    if (showNewCategory) {
      newCategoryInput.current.focus();
    }
  }, [showNewCategory]);

  useEffect(() => {
    // console.log("useEffect", fields);
    if (category === '') {
      const categoryValue = fields.find(field => field.name[0] === 'category');
      if (categoryValue) {
        // console.log("useEffect", category.value);
        setCategory(categoryValue.value);
      }
    }

    const type = fields.find(field => field.name[0] === 'type');
    if (type) {
      // console.log("useEffect", category.value);
      setType(type.value);
    }
  }, [fields])

  const onFinish = values => {
    // console.log('Success:', values);
    save(values);
    addNewCategory(values);
    cleanForm();
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  const cleanForm = () => {
    form.setFieldsValue({ description: '' });
    form.setFieldsValue({ newCategory: '' });
    form.setFieldsValue({ amount: '0' });
    descriptionInput.current.focus();
  }

  const addNewCategory = (values: any) => {
    if (showNewCategory) {
      addCategory(values['newCategory']);
      setShowNewCategory(false);
    }
  }

  const getKey = () => {
    if (editMode === NEW_RECORD) {
      return '';
    }
    else {
      return recordKey;
    }
  }

  const save = (values: any) => {
    // console.log(getKey());
    const record = {
      key: getKey(),
      description: values.description,
      category : showNewCategory ? values.newCategory : category,
      type: values.type,
      amount: values.amount,
      date: values.date,
      timeStamp: moment(),
      temp: values.temp,
    }
    addRecord(record);
  };

  const onCancelEdit = () => {
    // console.log("remake");
    newRecordMode();
  }

  const buttonText = editMode === NEW_RECORD ? 'Save' : 'Update';

  // if (editMode === EDIT_RECORD) {
  //   console.log("EDITING", editingRecord);
  //   //form.setFieldsValue({ description: editingRecord.description });
  // }

  return (
    <Form
      {...layout}
      name="basic"
      //initialValues={{ temp: true, description: editingRecord.description, category: categories[0], type: typeOptions[0], amount: 0, date: moment() }}
      // fields={editingRecord}
      // onFieldsChange={(_, allFields) => {
      //   onChange(allFields);
      // }}
      fields={fields}
      onFieldsChange={(_, allFields) => {
        onChange(allFields);
      }}
      form={form}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item
        label="Description"
        name="description"
        rules={[{ required: true, message: 'Please input your a description' }]}
      >
        <Input 
          ref={descriptionInput}
          // onChange={(e) => setDescription(e.target.value)}
        />
      </Form.Item>
      <Form.Item
        label="Type"
        name="type"
        rules={[{ required: false }]}
      >
        <Select 
          onChange={(e) => setType(e.toString())}
          >
          {typeOptions.map(option => <Option value={option} key={option}>{option}</Option>)}
        </Select>
      </Form.Item>
      { type === 'Credit' && !showNewCategory &&
        <Form.Item
          label="Category"
          name="category"
          rules={[{ required: false }]} 
        >
          {/* <Select 
            onChange={(e) => setCategory(e.toString())}
          >
            {categories.map(option => (<Option value={option} key={option}>{option}</Option>))}
          </Select> */}

          <Row>
            <Col span={21}>
              <Select 
                value={category}
                onChange={(e) => setCategory(e.toString())}
              >
                {categories.map(option => (<Option value={option} key={option}>{option}</Option>))}
              </Select>
            </Col>
            <Col span={3}>
              <Button type="ghost" onClick={() => setShowNewCategory(!showNewCategory)}>
                { !showNewCategory && "+" }
                { showNewCategory && "-" }
              </Button>
            </Col>
          </Row>
        </Form.Item>
      }
      { type === 'Credit' && showNewCategory &&
        <Form.Item
          label="Category"
          name="newCategory"
          rules={[{ required: false }]} 
        >
          <Row>
            <Col span={21}>
              <Input 
                ref={newCategoryInput}
                // onChange={(e) => setNewCategory(e.target.value)}
              />
            </Col>
            <Col span={3}>
              <Button type="ghost" onClick={() => setShowNewCategory(!showNewCategory)}>
                { !showNewCategory && "+" }
                { showNewCategory && "-" }
              </Button>
            </Col>
          </Row>
        </Form.Item>
      }

      <Form.Item
        label="Amount"
        name="amount"
        rules={[
          { required: true, message: 'Please input your an amount' },
          {
            validator: (rule: any, value: number, cb: (msg?: string) => void) => {
              value <= 0 ? cb("Amount should be greater than 0") : cb();
            }
          }
        ]}
      >
        <InputNumber 
          // onChange={(e) => setAmount(Number(e.toString()))}
          />
      </Form.Item>
      <Form.Item
        label="Date"
        name="date"
        rules={[{ required: true, message: 'Please input a date' }]}
      >
        <DatePicker 
          // onChange={(date, dateString) => setDate(date)}
          />
      </Form.Item>

      <Form.Item {...tailLayout} name="temp" valuePropName="checked">
        <Checkbox>Temporal</Checkbox>
      </Form.Item>

      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit">
          {buttonText}
        </Button>
        {
          editMode === EDIT_RECORD && (
            <Button htmlType="button" onClick={onCancelEdit}>
              Cancel
            </Button>
          )
        }        
      </Form.Item>
    </Form>
  )
}

const convertRecordToFields = (editingRecord: Record) => {
  return [
    { name: ['description'], value: editingRecord.description },
    { name: ['category'], value: editingRecord.category },
    { name: ['type'], value: editingRecord.type },
    { name: ['amount'], value: editingRecord.amount },
    { name: ['date'], value: moment(editingRecord.date) },
    { name: ['temp'], value: editingRecord.temp },
  ]
}

const EntryForm = ({addRecord, addCategory, categories, editMode, editingRecord, newRecordMode}) => {
  // console.log("EntryForm", editMode, editingRecord);
  // console.log("Entryform", editingRecord.date);
  const [fields, setFields] = useState<FieldData[]>(convertRecordToFields(editingRecord));
  const [recordKey, setRecordKey] = useState('');

  useEffect(() => {
    setFields(fields);
  }, [fields]);  

  useEffect(() => {
    setFields(convertRecordToFields(editingRecord));
    setRecordKey(editingRecord.key);
  }, [editingRecord]);

  // console.log("EntryForm", fields);

  return (
    <>
      <CustomizedForm
        fields={fields}
        onChange={newFields => {
          setFields(newFields);
        }}
        addRecord={addRecord}
        addCategory={addCategory}
        categories={categories}
        editMode={editMode}
        newRecordMode={newRecordMode}
        recordKey={recordKey}
      />
      <pre className="language-bash">{JSON.stringify(fields, null, 2)}</pre>
    </>
  );
};

EntryForm.getInitialProps = async ({ store, query, req, pathname }: NextPageContext<State>) => {
  // console.log("2. Page.getInitialProps uses the store to dispatch things", {
  //   pathname,
  //   query
  // });

  if (req) {
    // All async actions must be await'ed
    await store.dispatch({ type: "PAGE", payload: "server" });
    // Some custom thing for this particular page
    return { pageProp: "server" };
  }

  // await is not needed if action is synchronous
  store.dispatch({ type: "PAGE", payload: "client" });
  // Some custom thing for this particular page
  return { pageProp: "client" };
};

const mapStateToProps = state => state;

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    addRecord: (record) => dispatch({type: ADD_RECORD, payload: record}),
    addCategory: (category) => dispatch({type: ADD_CATEGORY, payload: category}),
    newRecordMode: () => dispatch({type: NEW_RECORD_MODE, payload: {}})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EntryForm);
