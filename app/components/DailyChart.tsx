
import { useState } from 'react'
import { connect } from 'react-redux';

import { ResponsiveLine } from '@nivo/line'
import moment from 'moment';

import { Record } from '../data/Types';

// interface DataPoint {
//   x: string;
//   y: number;
// }

// const getDaysMonthList = (daysInMonth: number): DataPoint[] => {
//   let days: DataPoint[] = [];
//   for (let i=1; i <= daysInMonth; i++) {
//     days.push({
//       x: `${i}`,
//       y: 0
//     });
//   }
//   return days;
// }

// const getCategories = (records: Record[]) => {
//   return records.reduce(
//     (group, record) => {
//       if (!group.hasOwnProperty(record.category)) {
//         group[record.category] = 0;
//       }
//       group[record.category] = group[record.category] + Number(record.amount);
//       return group;
//     },
//     {}
//   )
// }

// const getData = (records: Record[]) => {
//   // const month = moment().month()  // jan=0, dec=11
//   // const year = moment().year()
//   const daysInMonth = moment().daysInMonth();
//   const categories = getCategories(records);

//   let data = Object.keys(categories).reduce(
//     (data, key) => {
//       data.push({
//         id: key,
//         data: getDaysMonthList(daysInMonth)
//       });
//       return data;
//     },
//     []
//   );

//   // console.log('records', getCategories(records));
//   console.log(data);

//   records.forEach(record => {
//     // days[record.date.date()-1].y = Number(record.amount);
//     const pos = data.findIndex(elm => elm.id === record.category);
//     // console.log(pos, record.category);
//     data[pos].data[record.date.date()-1].y = Number(record.amount);
//   });

//   console.log("data", data);
//   return data;
// }

// const data = [
//     {
//       "id": "japan",
//       "color": "hsl(180, 70%, 50%)",
//       "data": [
//         {
//           "x": "plane",
//           "y": 135
//         },
//         {
//           "x": "helicopter",
//           "y": 65
//         },
//         {
//           "x": "boat",
//           "y": 37
//         },
//         {
//           "x": "train",
//           "y": 206
//         },
//         {
//           "x": "subway",
//           "y": 270
//         },
//         {
//           "x": "bus",
//           "y": 74
//         },
//         {
//           "x": "car",
//           "y": 92
//         },
//         {
//           "x": "moto",
//           "y": 275
//         },
//         {
//           "x": "bicycle",
//           "y": 189
//         },
//         {
//           "x": "horse",
//           "y": 270
//         },
//         {
//           "x": "skateboard",
//           "y": 276
//         },
//         {
//           "x": "others",
//           "y": 29
//         }
//       ]
//     },
//     {
//       "id": "france",
//       "color": "hsl(193, 70%, 50%)",
//       "data": [
//         {
//           "x": "plane",
//           "y": 224
//         },
//         {
//           "x": "helicopter",
//           "y": 245
//         },
//         {
//           "x": "boat",
//           "y": 156
//         },
//         {
//           "x": "train",
//           "y": 143
//         },
//         {
//           "x": "subway",
//           "y": 165
//         },
//         {
//           "x": "bus",
//           "y": 108
//         },
//         {
//           "x": "car",
//           "y": 133
//         },
//         {
//           "x": "moto",
//           "y": 256
//         },
//         {
//           "x": "bicycle",
//           "y": 251
//         },
//         {
//           "x": "horse",
//           "y": 30
//         },
//         {
//           "x": "skateboard",
//           "y": 74
//         },
//         {
//           "x": "others",
//           "y": 289
//         }
//       ]
//     },
//     {
//       "id": "us",
//       "color": "hsl(320, 70%, 50%)",
//       "data": [
//         {
//           "x": "plane",
//           "y": 201
//         },
//         {
//           "x": "helicopter",
//           "y": 223
//         },
//         {
//           "x": "boat",
//           "y": 153
//         },
//         {
//           "x": "train",
//           "y": 233
//         },
//         {
//           "x": "subway",
//           "y": 193
//         },
//         {
//           "x": "bus",
//           "y": 191
//         },
//         {
//           "x": "car",
//           "y": 8
//         },
//         {
//           "x": "moto",
//           "y": 73
//         },
//         {
//           "x": "bicycle",
//           "y": 111
//         },
//         {
//           "x": "horse",
//           "y": 92
//         },
//         {
//           "x": "skateboard",
//           "y": 254
//         },
//         {
//           "x": "others",
//           "y": 296
//         }
//       ]
//     },
//     {
//       "id": "germany",
//       "color": "hsl(16, 70%, 50%)",
//       "data": [
//         {
//           "x": "plane",
//           "y": 177
//         },
//         {
//           "x": "helicopter",
//           "y": 56
//         },
//         {
//           "x": "boat",
//           "y": 226
//         },
//         {
//           "x": "train",
//           "y": 208
//         },
//         {
//           "x": "subway",
//           "y": 100
//         },
//         {
//           "x": "bus",
//           "y": 295
//         },
//         {
//           "x": "car",
//           "y": 17
//         },
//         {
//           "x": "moto",
//           "y": 204
//         },
//         {
//           "x": "bicycle",
//           "y": 23
//         },
//         {
//           "x": "horse",
//           "y": 89
//         },
//         {
//           "x": "skateboard",
//           "y": 12
//         },
//         {
//           "x": "others",
//           "y": 194
//         }
//       ]
//     },
//     {
//       "id": "norway",
//       "color": "hsl(265, 70%, 50%)",
//       "data": [
//         {
//           "x": "plane",
//           "y": 20
//         },
//         {
//           "x": "helicopter",
//           "y": 107
//         },
//         {
//           "x": "boat",
//           "y": 211
//         },
//         {
//           "x": "train",
//           "y": 297
//         },
//         {
//           "x": "subway",
//           "y": 231
//         },
//         {
//           "x": "bus",
//           "y": 162
//         },
//         {
//           "x": "car",
//           "y": 255
//         },
//         {
//           "x": "moto",
//           "y": 175
//         },
//         {
//           "x": "bicycle",
//           "y": 299
//         },
//         {
//           "x": "horse",
//           "y": 131
//         },
//         {
//           "x": "skateboard",
//           "y": 152
//         },
//         {
//           "x": "others",
//           "y": 198
//         }
//       ]
//     }
// ]
  
const DailyChart = ({dailyChartData}) => {
  // console.log(getData(records));
  // console.log(data);
    // const [ chartData, setChartData] = useState(getData(records));
    // console.log("----");
    // console.log(getData(records));
    // console.log(chartData);

    // const chartData = getData(records);

    // const reloadChart = () => {
    //     const data = [
    //     {
    //         "id": "japan",
    //         "color": "hsl(180, 70%, 50%)",
    //         "data": [
    //             {
    //                 "x": "plane",
    //                 "y": 135
    //             },
    //             {
    //                 "x": "helicopter",
    //                 "y": 65
    //             },
    //             {
    //                 "x": "boat",
    //                 "y": 37
    //             },
    //             {
    //                 "x": "train",
    //                 "y": 206
    //             },
    //             {
    //                 "x": "subway",
    //                 "y": 270
    //             },
    //             {
    //                 "x": "bus",
    //                 "y": 74
    //             },
    //             {
    //                 "x": "car",
    //                 "y": 92
    //             },
    //             {
    //                 "x": "moto",
    //                 "y": 275
    //             },
    //             {
    //                 "x": "bicycle",
    //                 "y": 189
    //             },
    //             {
    //                 "x": "horse",
    //                 "y": 270
    //             },
    //             {
    //                 "x": "skateboard",
    //                 "y": 276
    //             },
    //             {
    //                 "x": "others",
    //                 "y": 29
    //             }
    //         ]
    //     }];
    //     setChartData(data);
    // }
    
    return (
        <ResponsiveLine
            data={dailyChartData}
            margin={{ top: 10, right: 10, bottom: 70, left: 60 }}
            xScale={{ type: 'point' }}
            yScale={{ type: 'linear', min: 'auto', max: 'auto', stacked: false, reverse: false }}
            axisTop={null}
            axisRight={null}
            axisBottom={{
                orient: 'bottom',
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: 'transportation',
                legendOffset: 36,
                legendPosition: 'middle'
            }}
            axisLeft={{
                orient: 'left',
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: 'count',
                legendOffset: -40,
                legendPosition: 'middle'
            }}
            //colors={{ scheme: 'nivo' }}
            pointSize={10}
            pointColor={{ theme: 'background' }}
            pointBorderWidth={2}
            pointBorderColor={{ from: 'serieColor' }}
            pointLabel="y"
            pointLabelYOffset={-12}
            useMesh={true}
            legends={[
                {
                    anchor: 'bottom',
                    direction: 'row',
                    justify: false,
                    translateX: 30,
                    translateY: 60,
                    itemsSpacing: 0,
                    itemDirection: 'left-to-right',
                    itemWidth: 110,
                    itemHeight: 20,
                    itemOpacity: 0.75,
                    symbolSize: 12,
                    symbolShape: 'circle',
                    symbolBorderColor: 'rgba(0, 0, 0, .5)',
                    effects: [
                        {
                            on: 'hover',
                            style: {
                                itemBackground: 'rgba(0, 0, 0, .03)',
                                itemOpacity: 1
                            }
                        }
                    ]
                }
            ]}
        />
    )
}

const mapStateToProps = state => state;

// const mapDispatchToProps = (dispatch, ownProps) => {
//   return {
//     addRecord: (record) => dispatch({type: ADD_RECORD, payload: record})
//   }
// }

export default connect(mapStateToProps, null)(DailyChart);
