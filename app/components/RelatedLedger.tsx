import { connect } from 'react-redux';
import { Row, Col, Tag, Button, Modal } from 'antd';
import { HIDE_RELATED_LEDGER } from '../store/reducer';
import styled from 'styled-components'

import { STATUS_ACTIVE, STATUS_CANCELED, STATUS_RELATED, STATUS_TEMP, STATUS_RESULT } from '../data/Contants';

var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
});

const RelatedLedger = ({recordsRelatedLedger, hide, selectedRelatedLedger, ledgers, tagColors}) => {
    console.log('recordsRelatedLedger', recordsRelatedLedger);
    const ledger = ledgers.find(ledger => ledger.key === selectedRelatedLedger) || {name:''};
    
    const recordsToSum = recordsRelatedLedger.filter(record => record.status != STATUS_CANCELED);

    const totalCredit = recordsToSum.reduce((total, record) => {
        if (record.type === 'Credit') total += Number(record.amount);
        return total
    }, 0);
    const totalDebit = recordsToSum.reduce((total, record) => {
        if (record.type === 'Debit') total += Number(record.amount);
        return total
    }, 0);
    const total = (totalDebit - totalCredit) * -1;

    const onClose = () => {
        hide();
    }

    return (
        <>
        <Header>
            <Row>
                <Col span="16">
                    <h1>{ledger.name}</h1>
                </Col>
                <ColRight span="8">
                    <Button onClick={onClose}>Close</Button>
                </ColRight>
            </Row>
        </Header>
        {
            recordsRelatedLedger.map(record => {
                return (
                    <>
                        <RowLedger key={record.key} styled={{pop: record.temp}}>
                            <Col span={14}><DescriptionValue>{record.description}</DescriptionValue></Col>
                            <Col span={4}><Tag color={tagColors[record.category]}>{record.category}</Tag></Col>
                            <Col span={3}><DateValue>{record.date.format('MM-DD-YYYY')}</DateValue></Col>
                            <ColRight span={3}>
                                {(record.status === STATUS_TEMP || record.status === STATUS_ACTIVE || record.status === STATUS_RESULT) && <Value>{formatter.format(record.amount)}</Value>}
                                {(record.status === STATUS_CANCELED || record.status === STATUS_RELATED) && <Value><CancelText>{formatter.format(record.amount)}</CancelText></Value>}
                            </ColRight>
                        </RowLedger>
                        <RowDivider/>
                    </>
                )
            })
        }
        <RowLedger styled={{pop: false}}>
            <Col span={18}></Col>
            <Col span={3}>
                Total
            </Col>
            <ColRight span={3}>
                <Value>{formatter.format(total)}</Value>
            </ColRight>
        </RowLedger>
        {/* <pre className="language-bash">{JSON.stringify(recordsRelatedLedger, null, 2)}</pre> */}
        </>
    )
}

const mapStateToProps = state => state;

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        hide: () => dispatch({type: HIDE_RELATED_LEDGER, payload: {}}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RelatedLedger);

const Header = styled.div`
    background-color:#eaeaea;
    padding:1.0rem;
`

/*
--------------------------------------------------------------------------------
*/

const RowLedger = styled(Row)`
    padding-top: 6px;
    padding-bottom: 6px;

    ${({styled: {pop}}) => pop && `
        background: #f9f9f9;
    `}
`
const RowDivider = styled.div`
    display: flex;
    clear: both;
    width: 100%;
    min-width: 100%;
    border-top: 1px solid #f0f0f0;
`
const DateValue = styled.span`
    font-size:'0.7rem'; 
    margin-top: '0.1rem';
`

const DescriptionValue = styled.span`
    font-family: 'Poppins'
`

const ColRight = styled(Col)`
    text-align: right;
`

const Value = styled.span`
    font-weight: bold;
`

const CancelText = styled.span`
  position: relative;
  &:before {
    position: absolute;
    content: "";
    left: 0;
    top: 50%;
    right: 0;
    border-top: 1px solid;
    border-color: red;
     
    -webkit-transform:rotate(-10deg);
    -moz-transform:rotate(-10deg);
    -ms-transform:rotate(-10deg);
    -o-transform:rotate(-10deg);
    transform:rotate(-10deg);
  }
`
