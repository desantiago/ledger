import { useEffect, useState, useCallback } from 'react'

import { Modal, Row, Col, Form, Select, Button, Input, Checkbox, DatePicker } from 'antd';
import moment from 'moment';

import Data from '../data/Data';
import { Ledger } from '../data/Types';

interface ModalAddLedgerProps {
    visibleModalAdd: boolean;
    modalAddOk: Function;
    modalAddCancel: Function;
    ledgers: Ledger[];
    categories: string[];
    selectedLedger: string;
    ledgersToRelate: Ledger[];
}

const { Option } = Select;

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

const ModalAddLedger: React.FC<ModalAddLedgerProps> = ({
    visibleModalAdd, 
    modalAddOk, 
    modalAddCancel, 
    ledgers, 
    categories, 
    selectedLedger, 
    ledgersToRelate
}: ModalAddLedgerProps) => {

    const [form] = Form.useForm();
    const [category, setCategory] = useState(categories[0]);
    // const [description, setDescription] = useState('');
    const [nameLedgerToAdd, setNameLedgerToAdd] = useState('');
    const [totalLedger, setTotalLedger] = useState(0);
    const [ledgerKey, setLedgerKey] = useState('');
    const [nameSelLedger, setNameSelLedger] = useState('');
    const [closeCheck, setCloseCheck] = useState(false);

    const data = Data.getInstance();
    //const otherLedgers = ledgers.filter(ledger => ledger.key !== selectedLedger);
    // const nameSelLedger = ledgers.find(ledger => ledger.key === selectedLedger).name;

    // useEffect(()=> {
    //     const l = ledgers.filter(ledger => ledger.key !== selectedLedger && !relatedLedgers.includes(ledger.key));
    //     console.log('setLedgersToRelate', l);
    //     setLedgersToRelate(l);
    // }, [relatedLedgers]);

    useEffect(() => {
        // console.log("--- Use Effect ---");
        setNameSelLedger(ledgers.find(ledger => ledger.key === selectedLedger).name);
        if (ledgersToRelate[0]) { 
            setLedgerKey(ledgersToRelate[0].key);
            setValues(ledgersToRelate[0].key) 
        };
    }, [ledgersToRelate, selectedLedger]);

    useEffect(() => {
        form.setFieldsValue({
            date: moment(),
            close: false
        });
        setCloseCheck(false);
    }, []);

    const modalOk = (values) => {
        values.ledgerKey = ledgerKey;
        values.total = totalLedger;
        // console.log("Modal Add Ok", values);
        modalAddOk(values);
    }
    
    const modalCancel = () => {
        // console.log("Modal Add Cancel");
        modalAddCancel();
    }

    const onChange = (value) => {
        // console.log(`selected ${value}`);
        setLedgerKey(value);
        setValues(value);
        // const total = data.getTotalAllRecords(value);
        // setLedgerKey(value);
        // setNameLedgerToAdd(ledgers.filter(ledger => ledger.key === value)[0].name);
        // setTotalLedger(total.total);
    }

    const setValues = (value) => {
        const total = data.getTotalAllRecords(value);
        setNameLedgerToAdd(ledgersToRelate.filter(ledger => ledger.key === value)[0].name);
        setTotalLedger(total.total);
    }

    const onSearch = (val) => {
        console.log('search:', val);
    }

    const onChangeClose = (e) => {
        // console.log("onChangeClose ", e);
        setCloseCheck(e.target.checked);
    }

    // setValues(otherLedgers[0].key);

    return (
        <Modal
            visible={visibleModalAdd}
            title="Add Ledger Result"
            width={700}
            //onOk={modalOk}            
            onCancel={modalCancel}
            okText="Add"
            cancelText="Cancel"
            onOk={() => {
                form
                    .validateFields()
                    .then(values => {
                        form.resetFields();
                        form.setFieldsValue({
                            date: moment(),
                            close: false
                        });
                        setCloseCheck(false);

                        modalOk(values);
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info);
                    });
            }}
            // footer={[
            //     <Button key="back" onClick={modalCancel}>
            //         Return
            //     </Button>,
            //     <Button key="submit" type="primary" onClick={modalOk}>
            //         Submit
            //     </Button>,
            // ]}
        >
            <Row>
                <Col span={12}>
                    <p>Select the ledger to add to the current ledger</p>
                    <Select
                        showSearch
                        style={{ width: 200 }}
                        placeholder="Select a ledger"
                        optionFilterProp="children"
                        onChange={onChange}
                        value={ledgerKey}
                        // defaultValue={{ value: otherLedgers[0].key }}
                        // onFocus={onFocus}
                        // onBlur={onBlur}
                        onSearch={onSearch}
                        filterOption={(input, option) =>
                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                    >
                        {
                            ledgersToRelate.map(ledger => <Option key={ledger.key} value={ledger.key}>{ledger.name}</Option> )
                        }
                    </Select>
                    <br/><br/>
                    { nameLedgerToAdd && <p>The result of {nameLedgerToAdd} of {totalLedger} will be added as a record to {nameSelLedger}</p> }
                </Col>
                <Col span={12}>
                    <Form initialValues={{ close: true, name: moment() }} form={form} name="control-hooks" {...layout}>
                        <Form.Item name="description" label="Description" rules={[{ required: true }]}>
                            <Input/>
                        </Form.Item>
                        <Form.Item name="category" label="Category" rules={[{ required: true }]}>
                            <Select
                                value={category}
                                onChange={(e) => setCategory(e.toString())}
                            >
                                { categories.map(option => (<Option value={option} key={option}>{option}</Option>)) }
                            </Select>
                        </Form.Item>
                        <Form.Item name="close" valuePropName="checked"  {...tailFormItemLayout}>
                            <Checkbox onChange={onChangeClose}>Close <strong>{nameLedgerToAdd}</strong> ledger and make it not editable</Checkbox>
                        </Form.Item>
                        {
                            closeCheck && <Form.Item label="Date" name="date" extra="Select the date to show in the ledger">
                                <DatePicker />
                            </Form.Item>
                        }
                    </Form>
                </Col>
            </Row>
        </Modal>
    )
}

export default ModalAddLedger;