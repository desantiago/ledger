// install (please make sure versions match peerDependencies)
// yarn add @nivo/core @nivo/pie
import { ResponsivePie } from '@nivo/pie';
import { connect } from 'react-redux';
// make sure parent container have a defined height when using
// responsive component, otherwise height will be 0 and
// no chart will be rendered.
// website examples showcase many properties,
// you'll often use just a few of them.

// const data = [
//     {
//       "id": "c",
//       "label": "c",
//       "value": 549,
//     //   "color": "hsl(315, 70%, 50%)"
//     },
//     {
//       "id": "scala",
//       "label": "scala",
//       "value": 505,
//     //   "color": "hsl(183, 70%, 50%)"
//     },
//     {
//       "id": "javascript",
//       "label": "javascript",
//       "value": 365,
//     //   "color": "hsl(117, 70%, 50%)"
//     },
//     {
//       "id": "elixir",
//       "label": "elixir",
//       "value": 461,
//     //   "color": "hsl(111, 70%, 50%)"
//     },
//     {
//       "id": "ruby",
//       "label": "ruby",
//       "value": 534,
//     //   "color": "hsl(194, 70%, 50%)"
//     }
// ]

const PieChart = ({categoryChartData}) => (
    <ResponsivePie
        data={categoryChartData}
        margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
        innerRadius={0.5}
        padAngle={0.7}
        cornerRadius={3}
        // colors={{ scheme: 'nivo' }}
        colors={{ datum: 'data.color' }}
        borderWidth={1}
        borderColor={{ from: 'color', modifiers: [ [ 'darker', 0.2 ] ] }}
        radialLabelsSkipAngle={10}
        radialLabelsTextColor="#333333"
        radialLabelsLinkColor={{ from: 'color' }}
        sliceLabelsSkipAngle={10}
        sliceLabelsTextColor="#333333"
        defs={[
            {
                id: 'dots',
                type: 'patternDots',
                background: 'inherit',
                color: 'rgba(255, 255, 255, 0.3)',
                size: 4,
                padding: 1,
                stagger: true
            },
            {
                id: 'lines',
                type: 'patternLines',
                background: 'inherit',
                color: 'rgba(255, 255, 255, 0.3)',
                rotation: -45,
                lineWidth: 6,
                spacing: 10
            }
        ]}
        // fill={[
        //     {
        //         match: {
        //             id: 'ruby'
        //         },
        //         id: 'dots'
        //     },
        //     {
        //         match: {
        //             id: 'c'
        //         },
        //         id: 'dots'
        //     },
        //     {
        //         match: {
        //             id: 'go'
        //         },
        //         id: 'dots'
        //     },
        //     {
        //         match: {
        //             id: 'python'
        //         },
        //         id: 'dots'
        //     },
        //     {
        //         match: {
        //             id: 'scala'
        //         },
        //         id: 'lines'
        //     },
        //     {
        //         match: {
        //             id: 'lisp'
        //         },
        //         id: 'lines'
        //     },
        //     {
        //         match: {
        //             id: 'elixir'
        //         },
        //         id: 'lines'
        //     },
        //     {
        //         match: {
        //             id: 'javascript'
        //         },
        //         id: 'lines'
        //     }
        // ]}
        // legends={[
        //     {
        //         anchor: 'bottom',
        //         direction: 'row',
        //         justify: false,
        //         translateX: 0,
        //         translateY: 56,
        //         itemsSpacing: 0,
        //         itemWidth: 100,
        //         itemHeight: 18,
        //         itemTextColor: '#999',
        //         itemDirection: 'left-to-right',
        //         itemOpacity: 1,
        //         symbolSize: 18,
        //         symbolShape: 'circle',
        //         effects: [
        //             {
        //                 on: 'hover',
        //                 style: {
        //                     itemTextColor: '#000'
        //                 }
        //             }
        //         ]
        //     }
        // ]}
    />
)

const mapStateToProps = state => state;

// const mapDispatchToProps = (dispatch, ownProps) => {
//   return {
//     addRecord: (record) => dispatch({type: ADD_RECORD, payload: record})
//   }
// }

export default connect(mapStateToProps, null)(PieChart);

// {"mergedItemId":"amzn1.p11cat.merged-video.2060ae8d-7d48-5147-befd-bcb947194e02","structuralInfo":{"type":"work","children":[{"type":"expression","id":"amzn1.p11cat.merged-video.5a6c32a1-0c67-5e87-8306-46e1ba39ca36"},{"type":"expression","id":"amzn1.p11cat.merged-video.05e80936-a1f4-5e53-b453-29db4ec52262"},{"type":"expression","id":"amzn1.p11cat.merged-video.465d27573dfe85c4695725b24dc064d7"},{"type":"expression","id":"amzn1.p11cat.merged-video.013145eb-d29f-58e4-82cb-d1bcb8592135"},{"type":"expression","id":"amzn1.p11cat.merged-video.73382898-b39c-5aaa-a369-b6722abc32b5"}]},"videoItems":[{"itemTypeId":"amzn1.p11cat.item-type.movie","contentDetails":{"title":[{"value":"Wild Target","locale":"en"}],"alternateTitles":[{"value":"Wild Target - Romanze in Blei","locale":"de-DE"},{"value":"Wild Target","locale":"en-US"},{"value":"Wild Target"},{"value":"Rakoncátlan célpont","locale":"hu-HU"},{"value":"Dziki cel","locale":"pl-PL"},{"value":"ターゲット","locale":"ja-JP"},{"value":"Agrios stohos","locale":"el-GR"},{"value":"Shënjestër e pakapshme","locale":"sq-AL"},{"value":"Petits meurtres à lu0027anglaise","locale":"fr-FR"},{"value":"Wild Target - Una valigia per 3","locale":"it-IT"},{"value":"Wild Target","locale":"en-AU"},{"value":"Karštas taikinys","locale":"lt-LT"},{"value":"Divlja meta","locale":"hr-HR"},{"value":"Дика штучка","locale":"uk-UA"},{"value":"Wild Target (Una valigia per tre)","locale":"it-IT"},{"value":"Pöörane sihtmärk","locale":"et-EE"},{"value":"Wild Target - Sein schärfstes Ziel","locale":"de-DE"},{"value":"Divja tarca","locale":"sl-SI"},{"value":"Wild Target","locale":"en-GB"},{"value":"Matador em Perigo","locale":"pt-BR"},{"value":"Дива мишена","locale":"bg-BG"},{"value":"Sevgili hedefim","locale":"tr-TR"},{"value":"Punct ochit, punct ... iubit!","locale":"ro-RO"},{"value":"Wild Target","locale":"en"},{"value":"Дикая штучка","locale":"ru-RU"},{"value":"Blanco escurridizo","locale":"eu-ES"}],"shortDescription":[{"value":"A hitman tries to retire, but a beautiful thief may change his plans.","locale":"en"}],"longDescription":[{"value":"Victor Maynard (Bill Nighy) is a middle-aged, solitary assassin, who lives to please his formidable mother, Louisa (Dame Eileen Atkins), despite his own peerless reputation for lethal efficiency. His professional routine is interrupted when he finds himself drawn to one of his intended victims, Rose (Emily Blunt). He spares her life, unexpectedly acquiring in the process a young apprentice, Tony (Rupert Grint). Believing Victor to be a private detective, his two new companions tag along, while he attempts to thwart the murderous attentions of his unhappy client.","locale":"en"}],"genres":[{"genreName":{"value":"Action","locale":"en"}},{"genreName":{"value":"Comedy","locale":"en"}},{"genreName":{"value":"Crime","locale":"en"}}]},"videoMediaDetails":{"audioLanguages":["en"]},"metadataInfo":{"providerName":"imdb","providerId":"tt1235189"},"externalIds":[{"value":"tt1235189","scheme":"imdb"}]}],"type":"video","itemType":"MOVIE"}

// {"mergedItemId":"amzn1.p11cat.merged-video.5a6c32a1-0c67-5e87-8306-46e1ba39ca36","structuralInfo":{"type":"expression","parent":{"type":"work","id":"amzn1.p11cat.merged-video.2060ae8d-7d48-5147-befd-bcb947194e02"}},"videoItems":[{"itemTypeId":"amzn1.p11cat.item-type.movie","contentDetails":{"title":[{"value":"Matador em Perigo","locale":"pt-BR"}],"shortDescription":[{"value":"Victor é um conhecido assassino de aluguel que deseja se aposentar. Ele acaba adiando os planos quando a encantadora golpista Rose entra em sua vida.","locale":"pt-BR"}],"longDescription":[{"value":"Victor é um conhecido assassino de aluguel que deseja se aposentar. Ele acaba adiando os planos quando a encantadora golpista Rose entra em sua vida.","locale":"pt-BR"}],"genres":[{"genreName":{"value":"Comedy","locale":"en-US"}},{"genreName":{"value":"Action/Adventure","locale":"en-US"}},{"genreName":{"value":"Comédie","locale":"fr-FR"}},{"genreName":{"value":"コメディー","locale":"ja-JP"}},{"genreName":{"value":"Action","locale":"fr-FR"}},{"genreName":{"value":"アクション","locale":"ja-JP"}}]},"videoMediaDetails":{},"metadataInfo":{"providerName":"avc_vending","providerId":"amzn1.dv.gti.cab9af6f-e9ad-87bc-a68c-9e2e14746e1f"},"externalIds":[{"value":"amzn1.dv.gti.cab9af6f-e9ad-87bc-a68c-9e2e14746e1f","scheme":"gti"},{"value":"tt1235189","scheme":"imdb"},{"value":"B08D731H1Y","scheme":"asin_row_na"},{"value":"B08D7125KJ","scheme":"asin_row_fe"},{"value":"B08D71R3VY","scheme":"asin_row_eu"},{"value":"B08D71QPFW","scheme":"asin_roe_eu"},{"value":"B08F2M2RR7","scheme":"asin"}],"alternateIds":[{"value":"B08D731H1Y","scheme":"aiv_row_na"},{"value":"B08D7125KJ","scheme":"aiv_row_fe"},{"value":"B08D71R3VY","scheme":"aiv_row_eu"},{"value":"B08D71QPFW","scheme":"aiv_roe_eu"}]},{"itemTypeId":"amzn1.p11cat.item-type.movie","contentDetails":{"title":[{"value":"Wild Target","locale":"en-US"}],"shortDescription":[{"value":"A hitman tries to retire, but a beautiful thief may change his plans.","locale":"en-US"}],"longDescription":[{"value":"A hitman tries to retire, but a beautiful thief may change his plans.","locale":"en-US"}],"genres":[{"genreName":{"value":"Action/Adventure","locale":"en-US"}},{"genreName":{"value":"Action","locale":"fr-FR"}},{"genreName":{"value":"アクション","locale":"ja-JP"}}]},"videoMediaDetails":{},"metadataInfo":{"providerName":"avc_vending","providerId":"amzn1.dv.gti.eab94f20-df3c-7d29-7138-b95b1b7c83c5"},"externalIds":[{"value":"amzn1.dv.gti.eab94f20-df3c-7d29-7138-b95b1b7c83c5","scheme":"gti"},{"value":"tt1235189","scheme":"imdb"},{"value":"B089YWB2YC","scheme":"asin_row_na"},{"value":"B089ZLKGXR","scheme":"asin_row_fe"},{"value":"B089ZH9V6D","scheme":"asin_row_eu"},{"value":"B089YSNTQ4","scheme":"asin_roe_eu"},{"value":"B089YVQRNB","scheme":"asin_in"}],"alternateIds":[{"value":"B089YWB2YC","scheme":"aiv_row_na"},{"value":"B089ZLKGXR","scheme":"aiv_row_fe"},{"value":"B089ZH9V6D","scheme":"aiv_row_eu"},{"value":"B089YSNTQ4","scheme":"aiv_roe_eu"}]},{"itemTypeId":"amzn1.p11cat.item-type.movie","contentDetails":{"title":[{"value":"Wild Target – Sein schärfstes Ziel","locale":"de-DE"}],"shortDescription":[{"value":"Victor Maynard, routinierter Profikiller und mit 52 immer noch Junggeselle, hat ein Problem: Er hat sich verliebt! Und zwar ausgerechnet in sein Opfer. Statt die bildhübsche Rose mit präziser Kaltblütigkeit aus der Welt zu schaffen, lässt er sich von ihr als Beschützer anheuern. Jetzt hat Victor nicht nur seinen Auftraggeber auf den Fersen, sondern auch den zweitbesten Killer Englands, der Vict...","locale":"de-DE"}],"longDescription":[{"value":"Victor Maynard, routinierter Profikiller und mit 52 immer noch Junggeselle, hat ein Problem: Er hat sich verliebt! Und zwar ausgerechnet in sein Opfer. Statt die bildhübsche Rose mit präziser Kaltblütigkeit aus der Welt zu schaffen, lässt er sich von ihr als Beschützer anheuern. Jetzt hat Victor nicht nur seinen Auftraggeber auf den Fersen, sondern auch den zweitbesten Killer Englands, der Victors „Patzer“ wieder in Ordnung bringen soll. Unerwartete Hilfe erhalten die Flüchtenden von Tony, einem jungen Mechaniker, der sich im Gebrauch von Waffen als Naturtalent erweist. Niemand hatte allerdings damit gerechnet, dass Victors Mutter sich ins Geschehen einmischt: Die alte Dame ist im Umgang mit Messern äußerst geschickt...","locale":"de-DE"}],"genres":[{"genreName":{"value":"Komödie","locale":"en-US"}},{"genreName":{"value":"Comédie","locale":"fr-FR"}},{"genreName":{"value":"コメディー","locale":"ja-JP"}}]},"videoMediaDetails":{},"metadataInfo":{"providerName":"avc_vending_de","providerId":"amzn1.dv.gti.eea9f6de-7ec1-43f5-bd1d-50d773d0e187"},"externalIds":[{"value":"amzn1.dv.gti.eea9f6de-7ec1-43f5-bd1d-50d773d0e187","scheme":"gti"},{"value":"B00PWVSW8E","scheme":"asin_de"}],"alternateIds":[{"value":"B00PWVSW8E","scheme":"aiv_de"}]},{"itemTypeId":"amzn1.p11cat.item-type.movie","contentDetails":{"title":[{"value":"Wild Target- Sein schärfstes Ziel [dt./OV]","locale":"de-DE"}],"shortDescription":[{"value":"Der äußerlich unauffällige, stets gepflegte und konzentrierte Junggeselle Victor Maynard gilt als unerreichter Meister in der Zunft der Auftragsmörder. Nun jedoch könnte er zum ersten Mal in seiner langen Karriere versagen, bringt er es doch nicht übers Herz, eine zur Vernichtung erkorene junge Kunstfälscherin um die Ecke zu bringen.","locale":"de-DE"}],"longDescription":[{"value":"Der äußerlich unauffällige, stets gepflegte und konzentrierte Junggeselle Victor Maynard gilt als unerreichter Meister in der Zunft der Auftragsmörder. Nun jedoch könnte er zum ersten Mal in seiner langen Karriere versagen, bringt er es doch nicht übers Herz, eine zur Vernichtung erkorene junge Kunstfälscherin um die Ecke zu bringen.","locale":"de-DE"}],"genres":[{"genreName":{"value":"Komödie u003e Sonstiges","locale":"en-US"}},{"genreName":{"value":"Kriminalfilm u003e Komödie","locale":"en-US"}},{"genreName":{"value":"Kriminalfilm","locale":"en-US"}},{"genreName":{"value":"Komödie","locale":"en-US"}},{"genreName":{"value":"Comédie","locale":"fr-FR"}},{"genreName":{"value":"コメディー","locale":"ja-JP"}},{"genreName":{"value":"Suspense","locale":"fr-FR"}},{"genreName":{"value":"サスペンス","locale":"ja-JP"}},{"genreName":{"value":"Suspense - Policier","locale":"fr-FR"}},{"genreName":{"value":"サスペンス - 犯罪","locale":"ja-JP"}}]},"videoMediaDetails":{},"metadataInfo":{"providerName":"avc_vending","providerId":"amzn1.dv.gti.fea9f6b6-9094-91af-13da-fb39d63d029d"},"externalIds":[{"value":"amzn1.dv.gti.fea9f6b6-9094-91af-13da-fb39d63d029d","scheme":"gti"},{"value":"tt1235189","scheme":"imdb"},{"value":"B07QZSQ3P3","scheme":"asin_row_na"},{"value":"B07QVM519N","scheme":"asin_row_fe"},{"value":"B07QYNRQXW","scheme":"asin_row_eu"},{"value":"B07QXLCN53","scheme":"asin_roe_eu"}],"alternateIds":[{"value":"B07QZSQ3P3","scheme":"aiv_row_na"},{"value":"B07QVM519N","scheme":"aiv_row_fe"},{"value":"B07QYNRQXW","scheme":"aiv_row_eu"},{"value":"B07QXLCN53","scheme":"aiv_roe_eu"}]}],"type":"video","itemType":"MOVIE"}

// "externalIds":[
//     {
//        "value":"amzn1.dv.gti.eab94f20-df3c-7d29-7138-b95b1b7c83c5",
//        "scheme":"gti"
//     },
//     {
//        "value":"tt1235189",
//        "scheme":"imdb"
//     },
//     {
//        "value":"B089YWB2YC",
//        "scheme":"asin_row_na"
//     },
//     {
//        "value":"B089ZLKGXR",
//        "scheme":"asin_row_fe"
//     },
//     {
//        "value":"B089ZH9V6D",
//        "scheme":"asin_row_eu"
//     },
//     {
//        "value":"B089YSNTQ4",
//        "scheme":"asin_roe_eu"
//     },
//     {
//        "value":"B089YVQRNB",
//        "scheme":"asin_in"
//     }
//  ],
//  "alternateIds":[
//     {
//        "value":"B089YWB2YC",
//        "scheme":"aiv_row_na"
//     },
//     {
//        "value":"B089ZLKGXR",
//        "scheme":"aiv_row_fe"
//     },
//     {
//        "value":"B089ZH9V6D",
//        "scheme":"aiv_row_eu"
//     },
//     {
//        "value":"B089YSNTQ4",
//        "scheme":"aiv_roe_eu"
//     }
//  ]


// {"mergedItemId":"amzn1.p11cat.merged-video.05e80936-a1f4-5e53-b453-29db4ec52262","structuralInfo":{"type":"expression","parent":{"type":"work","id":"amzn1.p11cat.merged-video.2060ae8d-7d48-5147-befd-bcb947194e02"}},"videoItems":[{"itemTypeId":"amzn1.p11cat.item-type.movie","contentDetails":{"title":[{"value":"Wild Target - Romanze in Blei","locale":"de"}],"shortDescription":[{"value":"Ein Profikiller verliebt sich und wechselt die Seiten.","locale":"de"}],"longDescription":[{"value":"Victor Maynard, routinierter Profikiller und mit 52 immer noch Junggeselle, hat ein Problem: Er hat sich ausgerechnet in sein letztes Opfer verliebt. Statt die bildhübsche Rose mit präziser Kaltblütigkeit aus der Welt zu schaffen, lässt er sich von ihr zu ihrem Beschützer anheuern. Damit hat Victor nicht nur seinen Auftraggeber auf den Fersen, sondern auch den zweitbesten Killer Englands, der Victors Patzer wieder in Ordnung bringen soll.","locale":"de"}],"genres":[{"genreName":{"value":"Action","locale":"en-US"}},{"genreName":{"value":"Comedy drama","locale":"en-US"}}]},"videoMediaDetails":{},"metadataInfo":{"providerName":"ontv_de","providerId":"MV006392840000"},"externalIds":[{"value":"MV006392840000","scheme":"tms"}]}],"type":"video","state":"DELETED","itemType":"MOVIE"}
// "externalIds":[
//     {
//        "value":"MV006392840000",
//        "scheme":"tms"
//     }
//  ]

// {"mergedItemId":"amzn1.p11cat.merged-video.465d27573dfe85c4695725b24dc064d7","structuralInfo":{"type":"expression","parent":{"type":"work","id":"amzn1.p11cat.merged-video.2060ae8d-7d48-5147-befd-bcb947194e02"}},"videoItems":[{"itemTypeId":"amzn1.p11cat.item-type.movie","contentDetails":{"title":[{"value":"Wild Target- Sein schärfstes Ziel [dt./OV]","locale":"de-DE"}],"shortDescription":[{"value":"Der äußerlich unauffällige, stets gepflegte und konzentrierte Junggeselle Victor Maynard gilt als unerreichter Meister in der Zunft der Auftragsmörder. Nun jedoch könnte er zum ersten Mal in seiner langen Karriere versagen, bringt er es doch nicht übers Herz, eine zur Vernichtung erkorene junge Kunstfälscherin um die Ecke zu bringen.","locale":"de-DE"}],"longDescription":[{"value":"Der äußerlich unauffällige, stets gepflegte und konzentrierte Junggeselle Victor Maynard gilt als unerreichter Meister in der Zunft der Auftragsmörder. Nun jedoch könnte er zum ersten Mal in seiner langen Karriere versagen, bringt er es doch nicht übers Herz, eine zur Vernichtung erkorene junge Kunstfälscherin um die Ecke zu bringen.","locale":"de-DE"}],"genres":[{"genreName":{"value":"Komödie u003e Sonstiges","locale":"en-US"}},{"genreName":{"value":"Kriminalfilm u003e Komödie","locale":"en-US"}},{"genreName":{"value":"Kriminalfilm","locale":"en-US"}},{"genreName":{"value":"Komödie","locale":"en-US"}},{"genreName":{"value":"Comédie","locale":"fr-FR"}},{"genreName":{"value":"コメディー","locale":"ja-JP"}},{"genreName":{"value":"Suspense","locale":"fr-FR"}},{"genreName":{"value":"サスペンス","locale":"ja-JP"}},{"genreName":{"value":"Suspense - Policier","locale":"fr-FR"}},{"genreName":{"value":"サスペンス - 犯罪","locale":"ja-JP"}}]},"videoMediaDetails":{},"metadataInfo":{"providerName":"avc_vending_de","providerId":"amzn1.dv.gti.fea9f6b6-9094-91af-13da-fb39d63d029d"},"externalIds":[{"value":"amzn1.dv.gti.fea9f6b6-9094-91af-13da-fb39d63d029d","scheme":"gti"},{"value":"tt1235189","scheme":"imdb"},{"value":"B00FCLXSVK","scheme":"asin_de"}],"alternateIds":[{"value":"B00FCLXSVK","scheme":"aiv_de"}]}],"type":"video","itemType":"MOVIE"}

// "externalIds":[
//     {
//        "value":"amzn1.dv.gti.fea9f6b6-9094-91af-13da-fb39d63d029d",
//        "scheme":"gti"
//     },
//     {
//        "value":"tt1235189",
//        "scheme":"imdb"
//     },
//     {
//        "value":"B00FCLXSVK",
//        "scheme":"asin_de"
//     }
//  ],
//  "alternateIds":[
//     {
//        "value":"B00FCLXSVK",
//        "scheme":"aiv_de"
//     }
//  ]


// {"mergedItemId":"amzn1.p11cat.merged-video.013145eb-d29f-58e4-82cb-d1bcb8592135","structuralInfo":{"type":"expression","parent":{"type":"work","id":"amzn1.p11cat.merged-video.2060ae8d-7d48-5147-befd-bcb947194e02"}},"videoItems":[{"itemTypeId":"amzn1.p11cat.item-type.movie","contentDetails":{"title":[{"value":"Wild Target","locale":"en-US"}],"shortDescription":[{"value":"A middle-aged, solitary assassin, finds his professional routine being interrupted when he finds himself drawn to one of his intended victims.","locale":"en-US"}],"longDescription":[{"value":"Victor Maynard (Bill Nighy), a middle-aged, solitary assassin, finds his professional routine being interrupted when he finds himself drawn to one of his intended victims, Rose (Emily Blunt). He spares her life, unexpectedly acquiring in the process a young apprentice, Tony (Rupert Grint). Believing Victor to be a private detective, his two new companions tag along, while he attempts to thwart the murderous attentions of his unhappy client.","locale":"en-US"}],"genres":[{"genreName":{"value":"Action","locale":"en-US"}},{"genreName":{"value":"Crime","locale":"en-US"}},{"genreName":{"value":"Comedy","locale":"en-US"}},{"genreName":{"value":"Comédie","locale":"fr-FR"}},{"genreName":{"value":"コメディー","locale":"ja-JP"}},{"genreName":{"value":"Action","locale":"fr-FR"}},{"genreName":{"value":"アクション","locale":"ja-JP"}},{"genreName":{"value":"Suspense","locale":"fr-FR"}},{"genreName":{"value":"サスペンス","locale":"ja-JP"}},{"genreName":{"value":"Suspense - Policier","locale":"fr-FR"}},{"genreName":{"value":"サスペンス - 犯罪","locale":"ja-JP"}}]},"videoMediaDetails":{},"metadataInfo":{"providerName":"avc_vending_us","providerId":"amzn1.dv.gti.86a9f713-ca53-3b9a-ceae-0e49069cd338"},"externalIds":[{"value":"amzn1.dv.gti.86a9f713-ca53-3b9a-ceae-0e49069cd338","scheme":"gti"},{"value":"tt1235189","scheme":"imdb"},{"value":"B00BRAFMG4","scheme":"asin_us"}],"alternateIds":[{"value":"B00BRAFMG4","scheme":"aiv_us"}]}],"type":"video","itemType":"MOVIE"}
// "externalIds":[
//     {
//        "value":"amzn1.dv.gti.86a9f713-ca53-3b9a-ceae-0e49069cd338",
//        "scheme":"gti"
//     },
//     {
//        "value":"tt1235189",
//        "scheme":"imdb"
//     },
//     {
//        "value":"B00BRAFMG4",
//        "scheme":"asin_us"
//     }
//  ],
//  "alternateIds":[
//     {
//        "value":"B00BRAFMG4",
//        "scheme":"aiv_us"
//     }
//  ]

// {"mergedItemId":"amzn1.p11cat.merged-video.73382898-b39c-5aaa-a369-b6722abc32b5","structuralInfo":{"type":"expression","parent":{"type":"work","id":"amzn1.p11cat.merged-video.2060ae8d-7d48-5147-befd-bcb947194e02"}},"videoItems":[{"itemTypeId":"amzn1.p11cat.item-type.movie","contentDetails":{"title":[{"value":"Wild Target","locale":"en"}],"shortDescription":[{"value":"Ein Profikiller verliebt sich und wechselt die Seiten.","locale":"de"}],"longDescription":[{"value":"Victor Maynard, routinierter Profikiller und mit 52 immer noch Junggeselle, hat ein Problem: Er hat sich ausgerechnet in sein letztes Opfer verliebt. Statt die bildhübsche Rose mit präziser Kaltblütigkeit aus der Welt zu schaffen, lässt er sich von ihr zu ihrem Beschützer anheuern. Damit hat Victor nicht nur seinen Auftraggeber auf den Fersen, sondern auch den zweitbesten Killer Englands, der Victors Patzer wieder in Ordnung bringen soll.","locale":"de"}],"genres":[{"genreName":{"value":"Action","locale":"en-US"}},{"genreName":{"value":"Comedy drama","locale":"en-US"}}]},"videoMediaDetails":{},"metadataInfo":{"providerName":"ontv_de","providerId":"MV006392830000"},"externalIds":[{"value":"MV006392830000","scheme":"tms"}]}],"type":"video","state":"DELETED","itemType":"MOVIE"}
// "externalIds":[
//     {
//        "value":"MV006392830000",
//        "scheme":"tms"
//     }
//  ]


// aiv_uk:B07DZW7X9Q
// Differences : aiv_us:B01M4JBLLZ,ontv:EP023153770006,aiv_de:B01MZ8X9CF,aiv_uk:B01NASD873,aiv_row_eu:B083CNRTDJ
// ZeldaMappings
// {\"expressions\":[{\"identifiers\":{\"aiv_uk\":[\"B07DZW7X9Q\"],\"aiv_us\":[\"B07DZLFQ3Z\"]}}],\"identifiers\":{\"imdb\":[\"tt3904832\"]}}
// Title References
// 
// Avis
// {\n  \"title\" : null,\n  \"identifiers\" : {\n    \"imdb\" : [ \"tt3904832\" ],\n    \"iridium\" : [ \"irid116079809\" ]\n  },\n  \"expressions\" : [ {\n    \"title\" : null,\n    \"identifiers\" : {\n      \"aiv_de\" : [ \"B01MZ8X9CF\" ],\n      \"aiv_row_eu\" : [ \"B083CNRTDJ\" ],\n      \"aiv_uk\" : [ \"B01NASD873\", \"B07DZW7X9Q\" ],\n      \"aiv_us\" : [ \"B01M4JBLLZ\", \"B07DZLFQ3Z\" ],\n      \"iridium\" : [ \"irid116252723\" ],\n      \"ontv\" : [ \"EP023153770006\" ]\n    }\n  } ]\n}


// mdb:tt6666218, NOT in Zelda

// aiv_us:B07NBQGWHZ
// Differences : webedia:256868,aiv_row_eu:B08NCHHLPM
// ZeldaMappings
// {\"expressions\":[{\"identifiers\":{\"aiv_row_eu\":[\"B07RCXYM45\"],\"aiv_us\":[\"B08NCP89DM\",\"B07NBQGWHZ\"]}}],\"identifiers\":{\"imdb\":[\"tt6314766\"]}}
// Avis
// {\n  \"title\" : null,\n  \"identifiers\" : {\n    \"imdb\" : [ \"tt6314766\" ],\n    \"iridium\" : [ \"ee2ac192-7f39-4afc-a65f-a0a45b50f6bc\" ]\n  },\n  \"expressions\" : [ {\n    \"title\" : null,\n    \"identifiers\" : {\n      \"aiv_row_eu\" : [ \"B07RCXYM45\", \"B08NCHHLPM\" ],\n      \"aiv_us\" : [ \"B07NBQGWHZ\", \"B08NCP89DM\" ],\n      \"iridium\" : [ \"06643027-2d4b-482c-a744-199fa57883d6\" ],\n      \"webedia\" : [ \"256868\" ]\n    }\n  } ]\n}

// tt3904832, tt6666218, tt6314766