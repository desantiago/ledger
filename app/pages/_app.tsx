// import App from "next/app";
// import type { AppProps /*, AppContext */ } from 'next/app'

// function MyApp({ Component, pageProps }: AppProps) {
//     return <Component {...pageProps} />
// }

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
//
// MyApp.getInitialProps = async (appContext: AppContext) => {
//   // calls page's `getInitialProps` and fills `appProps.pageProps`
//   const appProps = await App.getInitialProps(appContext);

//   return { ...appProps }
// }

// export default MyApp

// import React from 'react';
// import {wrapper} from '../store';
// import { Provider } from 'react-redux';

// const MyApp = ({Component, pageProps, store}) => (
//     <Provider store={store}>
//         <Component {...pageProps} />
//     </Provider>
// );

// export default wrapper.withRedux(MyApp);

import React from "react";
import App, { AppInitialProps, AppContext } from "next/app";
import { wrapper } from "../store";
import {connect} from 'react-redux';

import { IntlProvider, FormattedMessage } from "react-intl";
import translations from "../translations/index";
const locale = "sp";

class WrappedApp extends App<AppInitialProps> {
  public static getInitialProps = async ({ Component, ctx }: AppContext) => {
    // Keep in mind that this will be called twice on server, one for page and second for error page
    ctx.store.dispatch({ type: "APP", payload: "was set in _app" });
    // console.log('Sorrr', ctx.store.getState());

    return {
      pageProps: {
        // Call page-level getInitialProps
        ...(Component.getInitialProps
          ? await Component.getInitialProps(ctx)
          : {}),
        // Some custom thing for all pages
        appProp: ctx.pathname
      }
    };
  };

  public render() {
    const { Component, pageProps, language } = this.props;
    console.log("reder", language);
    return (
      <IntlProvider locale={language} messages={translations[language]}>
        <Component {...pageProps} />
      </IntlProvider>
    )
  }
}

const mapStateToProps = state => state;
const connectedApp = connect(mapStateToProps)(WrappedApp);
// export default wrapper.withRedux(WrappedApp);
export default wrapper.withRedux(connectedApp);