import React from "react";
import { useEffect, useState, useCallback } from 'react'
import "../styles/antd.less";
import {connect} from 'react-redux';

import { NextPageContext } from "next";
import Head from 'next/head'

import styled, { css } from 'styled-components'
import moment from 'moment';
// import axios from 'axios'
// import {useDropzone} from 'react-dropzone'

// import PeriodSelector from '../components/PeriodSelector';

import { State, ADD_LEDGER, CHANGE_LEDGER, UPDATE_LEDGER, ADD_RECORD, ADD_CATEGORY, MERGE_LEDGER, CLOSE_LEDGER, CHANGE_LANGUAGE } from '../store/reducer';

import { message, Layout, Menu, Divider, Tag, Button, Breadcrumb, AutoComplete, Dropdown, Modal, Alert } from 'antd';
import { Row, Col } from 'antd';
import { DownOutlined, UserOutlined } from '@ant-design/icons';

import { FormattedMessage } from "react-intl";

// import InlineEdit from 'react-edit-inplace';

import {
  FileAddOutlined
} from '@ant-design/icons';

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

import EntryForm from '../components/HorizonalEntryForm';
import EntryForm2 from '../components/HorizonalEntryForm2';
import EntryList from '../components/EntryList';
import EntryList2 from '../components/EntryList2';
import DailyChart from '../components/DailyChart';
import MonthlyChart from '../components/MonthlyChart';
import PieChart from '../components/PieChart';
import Stats from '../components/Stats';
import Last7DaysChart from "../components/Last7DaysChart";
import CurrentPeriod from '../components/CurrentPeriod';
import CurrentPeriod2 from '../components/CurrentPeriod2';
import ModalAddLedger from '../components/ModalAddLedger';
import RelatedLedger from '../components/RelatedLedger';
import LinkedLedgersList from '../components/LinkedLedgersList';
import ModalAddCategory from '../components/modals/ModalAddCategory';
import ModalAddNewLedger from '../components/modals/ModalAddNewLedger';
import ModalMerge from '../components/modals/ModalMerge';

import Data from '../data/Data';
import { Ledger, LinkedRecord, LinkType } from "../data/Types";

export interface PageProps extends State {
  pageProp: string;
  appProp: string;
}

const Index = ({
  tagColors,
  ledgers, 
  selectedLedger, 
  changeLedger, 
  updateLedger, 
  addLedger, 
  categories, 
  addRecord,
  addCategory, 
  viewRelatedLedger,
  mergeLedgers,
  closeLedger,
  language,
changeLanguage}) => {

  const data = Data.getInstance();
  // console.log("ledgers", ledgers);

  const [ visibleModalAdd, setVisibleModalAdd ] = useState(false);
  const [ visibleModalAddCategory, setVisibleModalAddCategory ] = useState(false);
  const [ visibleModalAddNewLedger, setVisibleModalAddNewLedger ] = useState(false);
  const [ visibleModalMerge, setVisibleModalMerge ] = useState(false);

  const [ ledgersToRelate, setLedgersToRelate ] = useState([]);
  const [ ledgerData, setLedgerdata ] = useState<Ledger>({
    key: '',
    name: '',
    timeStamp: moment(),
    closed: false
  });
  const [ linkedRecords, setLinkedRecords ] = useState<LinkedRecord[]>(null);
  const [ heightLinkedRecords, setHeightLinkedRecords ] = useState(0);
  const [ mergedLedgerName, setMergedLedgerName ] = useState('');
  // const [ heights, setHeights ] = useState({});

  useEffect(() => {
    const dataledger = ledgers.find(ledger => ledger.key === selectedLedger);
    setLedgerdata(dataledger);
    console.log("useEffect", dataledger);
    // When a ledger is linked to another
    // if (dataledger.linkedLedgers) {
    if (dataledger.linkType && dataledger.linkType === LinkType.LINK) {
      let recordsLinked = dataledger.linkedLedgers.map(linkedLedger => {
        const ledger = ledgers.find(ledger => ledger.key === linkedLedger.ledgerKey);
        const record = data.getRecord(linkedLedger.recordKey);
        return {
          ledgerName: ledger.name,
          description: record.description,
          category: record.category,
        }
      });
      // recordsLinked = recordsLinked.concat(recordsLinked);
      setLinkedRecords(recordsLinked);
      setHeightLinkedRecords(recordsLinked.length*2);
    }
    else if (dataledger.linkType && dataledger.linkType === LinkType.MERGE) {
      // dataledger.linkedLedgers[0].ledgerKey;
      console.log("Data MERGE", dataledger.linkedLedgers);
      const ledger = ledgers.find(ledger => ledger.key === dataledger.linkedLedgers[0].ledgerKey);
      setMergedLedgerName(ledger.name);
    }
    else {
      setLinkedRecords(null);
    }
  }, [ledgers, selectedLedger]);

  const onChangeLedger = ({key}) => {
    changeLedger(key);
  }

  const onAddLedger = () => {
    //addLedger();
    setVisibleModalAddNewLedger(true);
  }

  // const ledgerName = ():string => {
  //   const res = ledgers.filter(ledger => ledger.key === selectedLedger);
  //   return res[0].name;
  // }

  const ledgerNameChanged = ({message}) => {
    // data = { description: "New validated text comes here" }
    // Update your model from here
    //console.log(message);
    //this.setState({...data})
    updateLedger(message, '');
  }

  const customValidateText = (text) => {
    return (text.length > 0 && text.length < 64);
  }

  function handleMenuClick(e) {
    //message.info('Click on left button.');
    if (e.key === 'add_ledger') {
      // const relatedLedgers = data.getRelatedLedgers(selectedLedger);
      // const toRelate = ledgers.filter(ledger => ledger.key !== selectedLedger && !relatedLedgers.includes(ledger.key));
      const toRelate = data.getLedgersToRelate(selectedLedger);
      // console.log("related ledgers", relatedLedgers);
      console.log("toRelate", toRelate);

      if (toRelate.length === 0) {
        message.error('There are no ledger to relate');
      }
      else {
        setLedgersToRelate(toRelate);
        setVisibleModalAdd(true);
      }
    }
    else if (e.key === 'merge_ledger') {
      const toRelate = data.getLedgersToRelate(selectedLedger);
      if (toRelate.length === 0) {
        message.error('There are no ledger to relate');
      }
      else {
        setLedgersToRelate(toRelate);
        setVisibleModalMerge(true);
      }
    }
    else if (e.key === 'add_category') {
      setVisibleModalAddCategory(true);
    }
    else if (e.key === 'close_ledger') {
      // console.log('close_ledger', selectedLedger);
      closeLedger(selectedLedger);
      message.info('Ledger is closed');
    }
    else if (e.key === 'change_language') {
      if (language === 'en') changeLanguage('sp');
      if (language === 'sp') changeLanguage('en');
    }
  }
  const menu = (
    <Menu onClick={handleMenuClick}>
      <Menu.Item key="close_ledger">
        Close Ledger
      </Menu.Item>
      <Menu.Item key="add_ledger">
        Add Ledger
      </Menu.Item>
      <Menu.Item key="merge_ledger">
        Merge Ledger
      </Menu.Item>
      <Menu.Item key="add_category">
        Add Category
      </Menu.Item>
      <Menu.Item key="change_language">
        Change Language
      </Menu.Item>
    </Menu>
  );

  const modalAddOk = (values) => {
    // console.log("Modal Add Ok", values);
    const record = {
      key: selectedLedger,
      description: values.description,
      category : values.category,
      type: 'Credit',
      amount: values.total * -1,
      date: values.close ? values.date : data.getLastRecord(values.ledgerKey).date,
      timeStamp: moment(),
      temp: false,
      ledgerRelated: values.ledgerKey,
      closeLedgerRelated: values.close
    }
    addRecord(record);
    setVisibleModalAdd(false);    
  }

  const modalAddCancel = () => {
    // console.log("Modal Add Cancel");
    setVisibleModalAdd(false);
  }

  const modalAddCategoryOk = (values) => {
    // console.log('modalAddCategoryOk', values);
    addCategory(values.name, values.color);
    setVisibleModalAddCategory(false);
  }

  const modalAddCategoryCancel = () => { 
    setVisibleModalAddCategory(false);
  }

  const modalAddNewLedgerOk = (values) => {
    // console.log('modalAddCategoryOk', values);
    addLedger(values);
    setVisibleModalAddNewLedger(false);
  }

  const modalAddNewLedgerCancel = () => { 
    setVisibleModalAddNewLedger(false);
  }

  const modalMergeOk = (values) => {
    console.log('modalMergeOk', values);
    // addLedger(values);
    mergeLedgers(selectedLedger, values.ledgerKey);
    setVisibleModalMerge(false);
  }

  const modalMergeCancel = () => { 
    setVisibleModalMerge(false);
  }

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <ModalAddLedger 
        visibleModalAdd={visibleModalAdd}
        modalAddOk={modalAddOk}
        modalAddCancel={modalAddCancel}
        ledgers={ledgers}
        categories={categories}
        selectedLedger={selectedLedger}
        ledgersToRelate={ledgersToRelate}
      />
      <ModalAddCategory
        visibleModalAdd={visibleModalAddCategory}
        modalAddOk={modalAddCategoryOk}
        modalAddCancel={modalAddCategoryCancel}
      />
      <ModalAddNewLedger
        visibleModalAdd={visibleModalAddNewLedger}
        modalAddOk={modalAddNewLedgerOk}
        modalAddCancel={modalAddNewLedgerCancel}
      />
      <ModalMerge
        visibleModalAdd={visibleModalMerge}
        modalAddOk={modalMergeOk}
        modalAddCancel={modalMergeCancel}
        ledgers={ledgers}
        // categories={categories}
        selectedLedger={selectedLedger}
        ledgersToRelate={ledgersToRelate}
      />

      <Sider 
        style={{
          overflow: 'auto',
          height: '100vh',
          position: 'fixed',
          left: 0,
        }}
      >
        <div className="logo" />
        <Menu theme="dark" selectedKeys={[selectedLedger]} mode="inline">
          <Menu.Item key="add-ledger" icon={<FileAddOutlined />} onClick={onAddLedger}>
            Add Ledger
          </Menu.Item>
          {
            ledgers.map(
              ledger => <Menu.Item key={ledger.key} onClick={onChangeLedger}> 
                          {ledger.name}
                        </Menu.Item>
            )
          }
        </Menu>
      </Sider>
      <Layout className="site-layout" style={{ marginLeft: 200 }}>
        <Content style={{ backgroundColor: '#fff', margin: '0 0px', padding: '0 16px' }}>
          <Row>
            <ContentCol span={13}>
              <Row>
                <Col span={12}>
                    <LedgerTitle>
                      {ledgerData.name}
                      { ledgerData.closed && <small> closed</small>}
                    </LedgerTitle>
                    {/* <InlineEdit
                        validate={customValidateText}
                        activeClassName="editing"
                        text={ledgerName()}
                        paramName="message"
                        change={ledgerNameChanged}
                        style={{
                            minWidth: 150,
                            display: 'inline-block',
                            margin: 0,
                            padding: 0,
                            fontSize: 20,
                            outline: 0,
                            border: 0
                        }}/> */}
                </Col>
                <ColRight span={12}>
                  {/* <Dropdown overlay={menu}>
                    <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                      Ledger Options <DownOutlined />
                    </a>
                  </Dropdown> */}
                  { !ledgerData.closed &&  <div style={{paddingTop:'0.3rem'}}>
                    <Dropdown overlay={menu}>
                      <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                        <FormattedMessage id="ledgerOptions" defaultMessage="Ledger Options" /><DownOutlined />
                      </a>
                    </Dropdown>
                  </div> }
                </ColRight>
              </Row>

              {
                linkedRecords &&
                <>
                  <div style={ ledgerData.closed ? { height: '4vh' } : { height:`${32+heightLinkedRecords}vh` }}>
                    { !ledgerData.closed && 
                      <Divider orientation="left"><FormattedMessage id="newRecord" defaultMessage="New Record" /></Divider>
                    }
                    { !ledgerData.closed && <EntryForm2 /> }

                    <Divider orientation="left">Related Ledgers <small>These ledgers will be updated</small></Divider>
                    <LinkedLedgersList tagColors={tagColors} linkedRecords={linkedRecords} />
                    <Divider orientation="left"><CurrentPeriod2/></Divider>
                  </div>
                  {/* 35 y 59 */}
                  <div style={ ledgerData.closed ? {height: '90vh', overflow: 'auto'} : {height: `${62-heightLinkedRecords}vh`, overflow: 'auto'}}>
                    <EntryList2 />
                  </div>
                </>
              }
              {
                !linkedRecords &&
                <>
                  <div style={ ledgerData.closed ? !(ledgerData.linkType === LinkType.MERGE) ? { height: '4vh' } : { height: '8vh' }  : { height: '25vh' }}>
                    { !ledgerData.closed && 
                      <Divider orientation="left"><FormattedMessage id="newRecord" defaultMessage="New Record" /></Divider> 
                    }
                    { !ledgerData.closed && <EntryForm2 /> }
                    { ledgerData.linkType === LinkType.MERGE && <Alert message={`All the records had been copied to ${mergedLedgerName}`} type="info" /> }
                    <Divider orientation="left"><CurrentPeriod2/></Divider>
                  </div>

                  <div style={ ledgerData.closed ? !(ledgerData.linkType === LinkType.MERGE) ? {height: '90vh', overflow: 'auto'} : {height: '86vh', overflow: 'auto'} : {height: '69vh', overflow: 'auto'}}>
                    <EntryList2 />
                  </div>
                </>
              }
            </ContentCol>
            <Col span={11}>
              { viewRelatedLedger && <RelatedLedger /> }
              {!viewRelatedLedger && <>
                <Stats/>
                <div style={{height:250}}>
                  <Last7DaysChart />
                </div>
                {/* <div style={{height:250}}>
                <MonthlyChart />
                </div> */}
                <div style={{height:350}}>
                  <PieChart />
                </div>
              </>}
            </Col>
          </Row>
        </Content>
        {/* <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer> */}
      </Layout>
    </Layout>
  )
}

Index.getInitialProps = async ({ store, query, req, pathname }: NextPageContext<State>) => {
  console.log("2. Page.getInitialProps uses the store to dispatch things", {
    pathname,
    query
  });
  //console.log(req);

  if (req && store) {
    // All async actions must be await'ed
    await store.dispatch({ type: "PAGE", payload: "server" });

    // Some custom thing for this particular page
    return { pageProp: "server" };
  }

  // await is not needed if action is synchronous
  store.dispatch({ type: "PAGE", payload: "client" });

  // Some custom thing for this particular page
  return { pageProp: "client" };
};

const mapStateToProps = state => state;

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    changeLedger: (ledgerKey) => dispatch({type: CHANGE_LEDGER, payload: ledgerKey}),
    addLedger: ({name, description}) => dispatch({type:ADD_LEDGER, payload: {name, description}}),
    updateLedger: (name, description) => dispatch({type: UPDATE_LEDGER, payload: {name, description}}),
    addRecord: (record) => dispatch({type: ADD_RECORD, payload: record}),
    addCategory: (category, color) => dispatch({type: ADD_CATEGORY, payload: { category, color }}),
    mergeLedgers: (destiny, source) => dispatch({type: MERGE_LEDGER, payload: {destiny, source}}),
    closeLedger: (ledgerKey) => dispatch({type: CLOSE_LEDGER, payload: {ledgerKey}}),
    changeLanguage: (language) => dispatch({type: CHANGE_LANGUAGE, payload: {language}})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Index);

const ColRight = styled(Col)`
    text-align: right;
`

// overflow: auto;
// height: 100vh;

const ContentCol = styled(Col)`
    padding-left: 0.5rem;
    padding-right: 0.5rem;
`

const LedgerTitle = styled.h2`
    margin-bottom: 0;
`