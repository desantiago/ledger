import Head from 'next/head'
import { useEffect, useState, useCallback } from 'react'
// import useSWR from 'swr'
import axios from 'axios'
import {useDropzone} from 'react-dropzone'
// import { Button } from 'antd';
import "../styles/antd.less";
// import {decrementCounter, incrementCounter} from '../store/actions';
import {connect} from 'react-redux';
import { NextPageContext } from "next";
import { ADD_LEDGER, State } from "../store/reducer";
import { INCREMENT_COUNTER } from '../store/reducer';
// import { Layout, Menu, Breadcrumb } from 'antd';
// import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';

import PeriodSelector from '../components/PeriodSelector';
import CurrentPeriod from '../components/CurrentPeriod';

import { CHANGE_PERIOD, CHANGE_LEDGER, UPDATE_LEDGER } from '../store/reducer';

// const { SubMenu } = Menu;
// const { Header, Content, Sider } = Layout;
import React from "react";

import { Layout, Menu, Breadcrumb } from 'antd';
import { Form, Input, InputNumber, Button, Checkbox, Select, DatePicker } from 'antd';
import { Row, Col } from 'antd';

import InlineEdit from 'react-edit-inplace';

// import { ResponsiveContainer, LineChart, Line, XAxis, YAxis, ReferenceLine, ReferenceArea,
//   ReferenceDot, Tooltip, CartesianGrid, Legend, Brush, ErrorBar, AreaChart, Area,
//   Label, LabelList } from 'recharts';

import {
  DesktopOutlined,
  PieChartOutlined,
  FileOutlined,
  TeamOutlined,
  UserOutlined,
  FileAddOutlined
} from '@ant-design/icons';

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

import EntryForm from '../components/EntryForm';
import EntryList from '../components/EntryList';
import DailyChart from '../components/DailyChart';
import MonthlyChart from '../components/MonthlyChart';
import PieChart from '../components/PieChart';
import Stats from '../components/Stats';

// const formItemLayout = {
//   labelCol: {
//     xs: { span: 24 },
//     sm: { span: 8 },
//   },
//   wrapperCol: {
//     xs: { span: 24 },
//     sm: { span: 16 },
//   },
// };

// const renderSpecialDot = (props) => {
//   const { cx, cy, stroke, key } = props;

//   if (cx === +cx && cy === +cy) {
//     return <path d={`M${cx - 2},${cy - 2}h4v4h-4Z`} fill={stroke} key={key} />;
//   }

//   return null;
// };

// function Hello() {
//   return <div>Hello</div>;
// }


// const MyResponsiveLine = ({ data /* see data tab */ }) => (
// )
// export default function Home() {


//   const [ userProfiles, setUserProfiles ] = useState<any[]>([]);
//   const [ collapsed, setCollapsed ] = useState(false);
//   // const [ opacity, setOpacity ] = useState(0);
//   // const [ chartData, setChartData] = useState(data);

//   // useEffect(() => {
//   //   // // Update the document title using the browser API
//   //   // document.title = `You clicked ${count} times`;
//   //   const linePlot = new Line(document.getElementById('chart'), {
//   //     data,
//   //     xField: 'year',
//   //     yField: 'value',
//   //   });
    
//   //   linePlot.render();   
//   // });

//   // const data = [
//   //   { year: '1991', value: 3 },
//   //   { year: '1992', value: 4 },
//   //   { year: '1993', value: 3.5 },
//   //   { year: '1994', value: 5 },
//   //   { year: '1995', value: 4.9 },
//   //   { year: '1996', value: 6 },
//   //   { year: '1997', value: 7 },
//   //   { year: '1998', value: 9 },
//   //   { year: '1999', value: 13 },
//   // ];

//   // function onCollapse (collapsed) {
//   //   console.log(collapsed);
//   //   // this.setState({ collapsed });
//   //   setCollapsed(collapsed);
//   // };

//   // const data02 = [
//   //   { name: 'Page A', uv: 300, pv: 2600, amt: 3400 },
//   //   { name: 'Page B', uv: 400, pv: 4367, amt: 6400 },
//   //   { name: 'Page C', uv: 300, pv: 1398, amt: 2400 },
//   //   { name: 'Page D', uv: 200, pv: 9800, amt: 2400 },
//   //   { name: 'Page E', uv: 278, pv: 3908, amt: 2400 },
//   //   { name: 'Page F', uv: 189, pv: 4800, amt: 2400 },
//   //   { name: 'Page G', uv: 189, pv: 4800, amt: 2400 },
//   // ];

//   // onMouseEnter={this.handleLegendMouseEnter}
//   // onMouseLeave={this.handleLegendMouseLeave}

//   const options = ['food', 'entertaiment', 'gas', 'house expenses'];
  
//   return (

//     <Layout style={{ minHeight: '100vh' }}>
//     <Sider 
//       style={{
//         overflow: 'auto',
//         height: '100vh',
//         position: 'fixed',
//         left: 0,
//       }}
//     >
//       <div className="logo" />
//       <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
//         <Menu.Item key="1" icon={<PieChartOutlined />}>
//           Option 1
//         </Menu.Item>
//         <Menu.Item key="2" icon={<DesktopOutlined />}>
//           Option 2
//         </Menu.Item>
//         <SubMenu key="sub1" icon={<UserOutlined />} title="User">
//           <Menu.Item key="3">Tom</Menu.Item>
//           <Menu.Item key="4">Bill</Menu.Item>
//           <Menu.Item key="5">Alex</Menu.Item>
//         </SubMenu>
//         <SubMenu key="sub2" icon={<TeamOutlined />} title="Team">
//           <Menu.Item key="6">Team 1</Menu.Item>
//           <Menu.Item key="8">Team 2</Menu.Item>
//         </SubMenu>
//         <Menu.Item key="9" icon={<FileOutlined />} />
//       </Menu>
//     </Sider>
//     <Layout className="site-layout" style={{ marginLeft: 200 }}>
//       <Header className="site-layout-background" style={{ padding: 0, backgroundColor: '#f0f2f5', paddingLeft: '16px' }}>
//         <h1>User's Dashboard</h1>
//       </Header>
//       <Content style={{ margin: '0 16px' }}>
//         {/* <Breadcrumb style={{ margin: '16px 0' }}>
//           <Breadcrumb.Item>User</Breadcrumb.Item>
//           <Breadcrumb.Item>Bill</Breadcrumb.Item>
//         </Breadcrumb> */}

//         <Row>
//           <Col span={8}>
//             <h2>Intro new movement</h2>
//             <EntryForm />
//           </Col>
//           <Col span={16}>
//             <EntryList/>
//             Charting
//             <div style={{height:500}}>
//               <DailyChart />
//             </div>
//           </Col>
//         </Row>
//       </Content>
//       <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
//     </Layout>
//   </Layout>

//     // <Layout>
//     //   <Header className="header">
//     //     <div className="logo" />
//     //     <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
//     //       <Menu.Item key="1">nav 1</Menu.Item>
//     //       <Menu.Item key="2">nav 2</Menu.Item>
//     //       <Menu.Item key="3">nav 3</Menu.Item>
//     //     </Menu>
//     //   </Header>
//     //   <Layout>
//     //     <Sider width={200} className="site-layout-background">
//     //       <Menu
//     //         mode="inline"
//     //         defaultSelectedKeys={['1']}
//     //         defaultOpenKeys={['sub1']}
//     //         style={{ height: '100%', borderRight: 0 }}
//     //       >
//     //         <SubMenu key="sub1" icon={<UserOutlined />} title="subnav 1">
//     //           <Menu.Item key="1">option1</Menu.Item>
//     //           <Menu.Item key="2">option2</Menu.Item>
//     //           <Menu.Item key="3">option3</Menu.Item>
//     //           <Menu.Item key="4">option4</Menu.Item>
//     //         </SubMenu>
//     //         <SubMenu key="sub2" icon={<LaptopOutlined />} title="subnav 2">
//     //           <Menu.Item key="5">option5</Menu.Item>
//     //           <Menu.Item key="6">option6</Menu.Item>
//     //           <Menu.Item key="7">option7</Menu.Item>
//     //           <Menu.Item key="8">option8</Menu.Item>
//     //         </SubMenu>
//     //         <SubMenu key="sub3" icon={<NotificationOutlined />} title="subnav 3">
//     //           <Menu.Item key="9">option9</Menu.Item>
//     //           <Menu.Item key="10">option10</Menu.Item>
//     //           <Menu.Item key="11">option11</Menu.Item>
//     //           <Menu.Item key="12">option12</Menu.Item>
//     //         </SubMenu>
//     //       </Menu>
//     //     </Sider>
//     //     <Layout style={{ padding: '0 24px 24px' }}>
//     //       <Breadcrumb style={{ margin: '16px 0' }}>
//     //         <Breadcrumb.Item>Home</Breadcrumb.Item>
//     //         <Breadcrumb.Item>List</Breadcrumb.Item>
//     //         <Breadcrumb.Item>App</Breadcrumb.Item>
//     //       </Breadcrumb>
//     //       <Content
//     //         className="site-layout-background"
//     //         style={{
//     //           padding: 24,
//     //           margin: 0,
//     //           minHeight: 280,
//     //         }}
//     //       >
//     //         Content
//     //       </Content>
//     //     </Layout>
//     //   </Layout>
//     // </Layout>
//   )

//   // const fetchUserProfiles = () => {
//   //   axios.get('http://localhost:8080/api/v1/user-profile').then(res => {
//   //     const data: any[] = res.data;
//   //     console.log(data)
//   //     setUserProfiles(data)
//   //   });
//   // }

//   // useEffect( () => {
//   //   console.log('always executes');
//   //   fetchUserProfiles();
//   // }, [])


//   // {userProfiles.map( userProfile => {
//   //   return (
//   //   <div className="card" key={userProfile.userProfileId}>
//   //     <h3>{userProfile.username}</h3>
//   //     <p>{userProfile.userProfileId}</p>
//   //     <PrimaryButton>Test Button</PrimaryButton>
//   //   </div>
//   //   )
//   // })}
  
//   // return (
//   //   <div className="main-container">
//   //       <header>
//   //           Some Header
//   //       </header>
//   //       <main >
//   //           <aside className="aside-left">
//   //           </aside>

//   //           <article>
//   //               Center
//   //               <Button type="primary">Test Button</Button>
//   //               <Button type="dashed">Dashed Button</Button>

//   //           </article>

//   //           <nav className="nav-right">...</nav>
//   //       </main>
//   //       <footer>
//   //           ...
//   //       </footer>

//   //     <style jsx>{`

//   //       .main-container {
//   //         display: flex;
//   //         flex-direction: column;
//   //       }

//   //       main {
//   //         /* Take the remaining height */
//   //         flex-grow: 1;

//   //         /* Layout the left sidebar, main content and right sidebar */
//   //         display: flex;
//   //         flex-direction: row;
//   //       }

//   //       .aside-left {
//   //         width: 15%;
//   //       }

//   //       article {
//   //         flex-grow: 1;
//   //         background-color: #eaeaea;
//   //       }

//   //       .nav-right {
//   //         width: 20%;
//   //         background-color: #fafafa;
//   //       }      
//   //     `}</style>
      

//   //     <style jsx global>{`
//   //       html,
//   //       body {
//   //         padding: 0;
//   //         margin: 0;
//   //         font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
//   //           Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
//   //           sans-serif;
//   //       }

//   //       * {
//   //         box-sizing: border-box;
//   //       }
//   //     `}</style>
//   //   </div>
//   // )
// }

export interface PageProps extends State {
  pageProp: string;
  appProp: string;
}

// class Index extends React.Component<PageProps> {
//   // note that since _app is wrapped no need to wrap page
//   public static async getInitialProps({
//     store,
//     pathname,
//     query,
//     req
//   }: NextPageContext<State>) {
//     console.log("2. Page.getInitialProps uses the store to dispatch things", {
//       pathname,
//       query
//     });
//     //console.log(req);

//     if (req && store) {
//       // All async actions must be await'ed
//       await store.dispatch({ type: "PAGE", payload: "server" });

//       // Some custom thing for this particular page
//       return { pageProp: "server" };
//     }

//     // await is not needed if action is synchronous
//     store.dispatch({ type: "PAGE", payload: "client" });

//     // Some custom thing for this particular page
//     return { pageProp: "client" };
//   }

//   constructor(props) {
//     super(props);
//     // Este enlace es necesario para hacer que `this` funcione en el callback
//     this.change = this.change.bind(this);
//   }

//   private change() {
//     console.log("chabnge");
//     // store
//     //this.props.dispatch({type: 'INCREMENT_COUNTER', payload: {}});
//     this.props.incrementCounter();
//   }

//   public render() {
//     console.log('5. Page.render');
//     console.log(this.props);
//     const { pageProp, appProp, app, page, counter } = this.props;

//     // const [ userProfiles, setUserProfiles ] = useState<any[]>([]);
//     // const [ collapsed, setCollapsed ] = useState(false);
//     // const options = ['food', 'entertaiment', 'gas', 'house expenses'];
    
//     return (
//       <Layout style={{ minHeight: '100vh' }}>
//         <Sider 
//           style={{
//             overflow: 'auto',
//             height: '100vh',
//             position: 'fixed',
//             left: 0,
//           }}
//         >
//           <div className="logo" />
//           <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
//             <Menu.Item key="1" icon={<PieChartOutlined />}>
//               Option 1
//             </Menu.Item>
//             <Menu.Item key="2" icon={<DesktopOutlined />}>
//               Option 2
//             </Menu.Item>
//             <SubMenu key="sub1" icon={<UserOutlined />} title="User">
//               <Menu.Item key="3">Tom</Menu.Item>
//               <Menu.Item key="4">Bill</Menu.Item>
//               <Menu.Item key="5">Alex</Menu.Item>
//             </SubMenu>
//             <SubMenu key="sub2" icon={<TeamOutlined />} title="Team">
//               <Menu.Item key="6">Team 1</Menu.Item>
//               <Menu.Item key="8">Team 2</Menu.Item>
//             </SubMenu>
//             <Menu.Item key="9" icon={<FileOutlined />} />
//           </Menu>      
//         </Sider>
//         <Layout className="site-layout" style={{ marginLeft: 200 }}>
//           <Header className="site-layout-background" style={{ padding: 0, backgroundColor: '#f0f2f5', paddingLeft: '16px' }}>
//             <h1>User's Dashboard</h1>
//           </Header>
//           <Content style={{ margin: '0 16px' }}>
//             <Row>
//               <Col span={8}>
//                 <h2>Intro new movement</h2>

//                 counter : {counter} <br/>

//           <Button type="primary" onClick={this.change}>
//             Change couner
//           </Button>

//                 <EntryForm />
//               </Col>
//               <Col span={16}>
//                 <EntryList/>
//                   Charting
//                   <div style={{height:500}}>
//                     <DailyChart />
//                   </div>
//               </Col>
//             </Row>
//           </Content>
//           <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
//         </Layout>
//       </Layout>
//     )
//   }
// }

// const mapStateToProps = state => state;

// const mapDispatchToProps = (dispatch, ownProps) => {
//   return {
//     incrementCounter: () => dispatch({type: 'INCREMENT_COUNTER'})
//   }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(Index);
// // export default connect(state => state)(Index);


const Index = ({counter, incrementCounter, selectedPeriod, changePeriod, selectedPeriodType, ledgers, selectedLedger, changeLedger, updateLedger, addLedger}) => {
  const [ userProfiles, setUserProfiles ] = useState<any[]>([]);
  const [ collapsed, setCollapsed ] = useState(false);
  // const options = ['food', 'entertaiment', 'gas', 'house expenses'];
  
  // const change = () => {
  //   console.log("chabnge");
  //   // store
  //   //this.props.dispatch({type: 'INCREMENT_COUNTER', payload: {}});
  //   incrementCounter();
  // }

  const onChangePeriod = (type: string, value: any) => {
    console.log("Period Selected", type);
    // console.log(Period.last30Days().start.format('LL'));
    // console.log(Period.last30Days().end.format('LL'));
    // console.log(Period.previousWeek());
    changePeriod(type, value);
  }

  const onChangeLedger = ({key}) => {
    console.log('onChangeLedger ', key);
    changeLedger(key);
  }

  const onAddLedger = () => {
    addLedger();
  }

  const ledgerName = ():string => {
    const res = ledgers.filter(ledger => ledger.key === selectedLedger);
    return res[0].name;
  }

  const ledgerNameChanged = ({message}) => {
    // data = { description: "New validated text comes here" }
    // Update your model from here
    //console.log(message);
    //this.setState({...data})
    updateLedger(message, '');
  }

  const customValidateText = (text) => {
    return (text.length > 0 && text.length < 64);
  }

  /*
            Counter : {counter} <br/>
            <Button type="primary" onClick={change}>
              Change couner
            </Button>
  */
  // console.log('Ledgers ', ledgers);

  return (
    <Layout style={{ minHeight: '100vh' }}>
    <Sider 
      style={{
        overflow: 'auto',
        height: '100vh',
        position: 'fixed',
        left: 0,
      }}
    >
      <div className="logo" />
      <Menu theme="dark" selectedKeys={[selectedLedger]} mode="inline">
        <Menu.Item key="add-ledger" icon={<FileAddOutlined />} onClick={onAddLedger}>
          Add Ledger
        </Menu.Item>
        {
          ledgers.map(
            ledger => <Menu.Item key={ledger.key} onClick={onChangeLedger}> 
                        {ledger.name}
                      </Menu.Item>
          )
        }
        {/* <SubMenu key="sub1" icon={<UserOutlined />} title="User">
          <Menu.Item key="3">Tom</Menu.Item>
          <Menu.Item key="4">Bill</Menu.Item>
          <Menu.Item key="5">Alex</Menu.Item>
        </SubMenu>
        <SubMenu key="sub2" icon={<TeamOutlined />} title="Team">
          <Menu.Item key="6">Team 1</Menu.Item>
          <Menu.Item key="8">Team 2</Menu.Item>
        </SubMenu>
        <Menu.Item key="9" icon={<FileOutlined />} /> */}
      </Menu>
    </Sider>
    <Layout className="site-layout" style={{ marginLeft: 200 }}>
      <Header className="site-layout-background" style={{ padding: 0, backgroundColor: '#f0f2f5', paddingLeft: '16px' }}>
        {/* <h1>{ledgerName()}</h1> */}

        <InlineEdit
          validate={customValidateText}
          activeClassName="editing"
          text={ledgerName()}
          paramName="message"
          change={ledgerNameChanged}
          style={{
            // backgroundColor: 'yellow',
            minWidth: 150,
            display: 'inline-block',
            margin: 0,
            padding: 0,
            // fontSize: 15,
            fontSize: 20,
            outline: 0,
            border: 0
          }}
        />        
      </Header>
      <Content style={{ margin: '0 16px' }}>
        <Row>
          <Col span={6}>
            <h2>Intro new movement</h2>
          </Col>
          <Col span={18}>
              <CurrentPeriod />
          </Col>
        </Row>
        <Row>
          <Col span={6}>
            <EntryForm />
          </Col>
          <Col span={12}>
            <EntryList/>
          </Col>
          <Col span={6}>
            <Stats/>
            <div style={{height:250}}>
              <DailyChart />
            </div>
            {/* <div style={{height:250}}>
              <MonthlyChart />
            </div> */}
            <div style={{height:350}}>
              <PieChart />
            </div>
          </Col>
        </Row>

      </Content>
      <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
    </Layout>
    </Layout>
  )
}

Index.getInitialProps = async ({ store, query, req, pathname }: NextPageContext<State>) => {
    console.log("2. Page.getInitialProps uses the store to dispatch things", {
      pathname,
      query
    });
    //console.log(req);

    if (req && store) {
      // All async actions must be await'ed
      await store.dispatch({ type: "PAGE", payload: "server" });

      // Some custom thing for this particular page
      return { pageProp: "server" };
    }

    // await is not needed if action is synchronous
    store.dispatch({ type: "PAGE", payload: "client" });

    // Some custom thing for this particular page
    return { pageProp: "client" };
};

const mapStateToProps = state => state;

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    incrementCounter: () => dispatch({type: INCREMENT_COUNTER}),
    changePeriod: (type, value) => dispatch({type: CHANGE_PERIOD, payload: { type, value }}),
    changeLedger: (ledgerKey) => dispatch({type: CHANGE_LEDGER, payload: ledgerKey}),
    addLedger: () => dispatch({type:ADD_LEDGER, payload: {}}),
    updateLedger: (name, description) => dispatch({type: UPDATE_LEDGER, payload: {name, description}})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Index);

// export default Index;