import { Record, ChartData, PieChartData, TotalData, Ledger } from './Types';
import LocalData from './LocalData';

/**
 * The Singleton class defines the `getInstance` method that lets clients access
 * the unique singleton instance.
 */
export default class Data {
    private static instance: Data;
    private d: LocalData;
    /**
     * The Singleton's constructor should always be private to prevent direct
     * construction calls with the `new` operator.
     */
    private constructor() {
        console.log("Data constructor");
        this.d = LocalData.getInstance();
    }

    /**
     * The static method that controls the access to the singleton instance.
     *
     * This implementation let you subclass the Singleton class while keeping
     * just one instance of each subclass around.
     */
    public static getInstance(): Data {
        if (!Data.instance) {
            Data.instance = new Data();
        }

        return Data.instance;
    }

    public getLedgers(): Ledger[] {
        return this.d.getLedgers();
    }

    public addLedger(ledger: Ledger): string {
        return this.d.addLedger(ledger);
    }

    public closeLedger(ledgerKey: string): boolean {
        return this.d.closeLedger(ledgerKey);
    }

    public updateLedger(ledgerKey: string, name: string, description: string) {
        this.d.updateLedger(ledgerKey, name, description);
    }

    public getLastRecords(ledgerKey: string): Record[] {
        return this.d.getLastRecords(ledgerKey);
    }

    public addRecord(record: Record, ledgerKey: string) {
        return this.d.addRecord(record, ledgerKey);
    }

    public removeRecord(recordKey: string) {
        return this.d.removeRecord(recordKey);
    }

    public cancelRecord(recordKey: string) {
        this.d.cancelRecord(recordKey);
    }

    public getLastRecord(ledgerKey: string): Record {
        return this.d.getLastRecord(ledgerKey);
    }
    // public getRecordsByPeriod(start:any, end:any): Record[] {
    //     return this.d.getRecordsByPeriod(start, end);
    // }

    public getRecord(recordKey: string): Record {
        return this.d.getRecord(recordKey);
    }

    public saveTemporalRecords(ledgerKey: string) {
        this.d.saveTemporalRecords(ledgerKey);
    }

    public mergeLedgers(destinyLedgerKey: string, sourceLedgerKey: string): boolean {
        return this.d.mergeLedgers(destinyLedgerKey, sourceLedgerKey);
    }

    public getRecordsByPeriod(ledgerKey: string, type: string, value: any): Record[] {
        return this.d.getRecordsByPeriod(ledgerKey, type, value);
    }

    // public getRecordsMonth(month: number): Record[] {
    //     return this.d.getRecordsMonth(month);
    // }

    public getRecordDailyChartData(ledgerKey: string, type: string, value: any): ChartData[] {
        return this.d.getRecordDailyChartData(ledgerKey, type, value);
    }

    public getMonthlyChartData(ledgerKey: string): ChartData[] {
        return this.d.getMonthlyChartData(ledgerKey);
    }

    public getCategoryPieData(ledgerKey: string, type: string, value: any): PieChartData[] {
        return this.d.getCategoryPieData(ledgerKey, type, value);
    }

    public getTotal(ledgerKey: string, type: string, value: any): TotalData {
        return this.d.getTotal(ledgerKey, type, value);
    }

    public getLast7DaysChartData(ledgerKey: string, type: string, value: any): any[] {
        return this.d.getLast7DaysChartData(ledgerKey, type, value);
    }

    public getTotalAllRecords(ledgerKey: string): TotalData {
        return this.d.getTotalAllRecords(ledgerKey);
    }

    public addCategory(category: string, color: string) {
        this.d.addCategory(category, color);
    }

    public getCategories(): string[] {
        return this.d.getCategories();
    }

    public getTagColors() {
        return this.d.getTagColors();
    }
    public getAvailableColorsName() {
        return this.d.getAvailableColorsName();
    }
    public getAvailableColorsHex() {
        return this.d.getAvailableColorsHex();
    }

    public getRelatedLedgers(ledgerKey: string): string[] {
        return this.d.getRelatedLedgers(ledgerKey);
    }

    public getLedgersToRelate(ledgerKey: string): Ledger[] {
        return this.d.getLedgersToRelate(ledgerKey);
    }
}
