import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';

import { STATUS_ACTIVE, STATUS_CANCELED } from '../data/Contants';

const categories = ['Home','Food','Gas','Entertainment'];

const ledgerKeyCurrentExpenses = uuidv4();
const ledgerKeyHouse = uuidv4();
const ledgerGamming = uuidv4();

export const SampleRecords = [
    {
        key: uuidv4(),
        ledgerKey: ledgerKeyCurrentExpenses,
        description: 'Christmas Decoration',
        category : categories[0],
        type: 'Credit',
        amount: 250,
        date: moment('2020-12-05'),
        timeStamp: moment('2020-12-01').add(1, 'second'),
        temp: false,
        status: STATUS_ACTIVE,
    },
    {
        key: uuidv4(),
        ledgerKey: ledgerKeyCurrentExpenses,
        description: 'Grocery store',
        category : categories[1],
        type: 'Credit',
        amount: 458,
        date: moment('2020-12-14'),
        timeStamp: moment('2020-12-01').add(2, 'second'),
        temp: false,
        status: STATUS_ACTIVE,
    },
    {
        key: uuidv4(),
        ledgerKey: ledgerKeyCurrentExpenses,
        description: 'Gas',
        category : categories[2],
        type: 'Credit',
        amount: 30,
        date: moment('2020-12-15'),
        timeStamp: moment('2020-12-01').add(3, 'second'),
        temp: false,
        status: STATUS_ACTIVE,
    },
    {
        key: uuidv4(),
        ledgerKey: ledgerKeyCurrentExpenses,
        description: 'PS5',
        category : categories[3],
        type: 'Credit',
        amount: 538,
        date: moment('2020-12-18'),
        timeStamp: moment('2020-12-01').add(4, 'second'),
        temp: false,
        status: STATUS_ACTIVE,
    },
    {
        key: uuidv4(),
        ledgerKey: ledgerKeyCurrentExpenses,
        description: 'Christmas Dinner',
        category : categories[1],
        type: 'Credit',
        amount: 750,
        date: moment('2020-12-22'),
        timeStamp: moment('2020-12-01').add(5, 'second'),
        temp: false,
        status: STATUS_ACTIVE,
    },
    {
        key: uuidv4(),
        ledgerKey: ledgerKeyCurrentExpenses,
        description: 'New Yeaers Dinner',
        category : categories[1],
        type: 'Credit',
        amount: 180,
        date: moment('2020-12-30'),
        timeStamp: moment('2020-12-01').add(6, 'second'),
        temp: false,
        status: STATUS_ACTIVE,
    },
    {
        key: uuidv4(),
        ledgerKey: ledgerKeyCurrentExpenses,
        description: 'Grovery Store',
        category : categories[1],
        type: 'Credit',
        amount: 269,
        date: moment('2021-01-02'),
        timeStamp: moment('2020-12-01').add(7, 'second'),
        temp: false,
        status: STATUS_ACTIVE,
    },
    {
        key: uuidv4(),
        ledgerKey: ledgerKeyCurrentExpenses,
        description: 'Gas',
        category : categories[2],
        type: 'Credit',
        amount: 35,
        date: moment('2021-01-02'),
        timeStamp: moment('2020-12-01').add(8, 'second'),
        temp: false,
        status: STATUS_ACTIVE,
    },
    {
        key: uuidv4(),
        ledgerKey: ledgerKeyCurrentExpenses,
        description: 'Furniture',
        category : categories[0],
        type: 'Credit',
        amount: 500,
        date: moment('2021-01-05'),
        timeStamp: moment('2020-12-01').add(9, 'second'),
        temp: false,
        status: STATUS_ACTIVE,
    },
    {
        key: uuidv4(),
        ledgerKey: ledgerKeyCurrentExpenses,
        description: 'PS5 Videogames',
        category : categories[3],
        type: 'Credit',
        amount: 77,
        date: moment('2021-01-06'),
        timeStamp: moment('2020-12-01').add(10, 'second'),
        temp: false,
        status: STATUS_ACTIVE,
    },
    {
        key: uuidv4(),
        ledgerKey: ledgerKeyCurrentExpenses,
        description: 'Power and Gas',
        category : categories[0],
        type: 'Credit',
        amount: 450,
        date: moment(),
        timeStamp: moment().add(1, 'second'),
        temp: false,
        status: STATUS_ACTIVE,
    },
    {
        key: uuidv4(),
        ledgerKey: ledgerKeyCurrentExpenses,
        description: 'Fix garage door',
        category : categories[0],
        type: 'Credit',
        amount: 350,
        date: moment(),
        timeStamp: moment().add(2, 'second'),
        temp: false,
        status: STATUS_ACTIVE,
    },
    {
        key: uuidv4(),
        ledgerKey: ledgerKeyHouse,
        description: 'New table for the kitchen',
        category : categories[0],
        type: 'Credit',
        amount: 650,
        date: moment(),
        timeStamp: moment().add(2, 'second'),
        temp: false,
        status: STATUS_ACTIVE,
    },
    {
        key: uuidv4(),
        ledgerKey: ledgerKeyHouse,
        description: 'New TV for the living room',
        category : categories[0],
        type: 'Credit',
        amount: 550,
        date: moment(),
        timeStamp: moment().add(3, 'second'),
        temp: false,
        status: STATUS_ACTIVE,
    },
    {
        key: uuidv4(),
        ledgerKey: ledgerKeyHouse,
        description: 'New TV for the living room',
        category : categories[3],
        type: 'Credit',
        amount: 550,
        date: moment(),
        timeStamp: moment().add(3, 'second'),
        temp: false,
        status: STATUS_CANCELED,
    },

    {
        key: uuidv4(),
        ledgerKey: ledgerKeyCurrentExpenses,
        description: 'Gas',
        category : categories[2],
        type: 'Credit',
        amount: 35,
        date: moment('2021-01-02'),
        timeStamp: moment().add(4, 'second'),
        temp: true,
        status: STATUS_ACTIVE,
    },
    {
        key: uuidv4(),
        ledgerKey: ledgerKeyCurrentExpenses,
        description: 'Gas',
        category : categories[2],
        type: 'Credit',
        amount: 35,
        date: moment('2021-01-02'),
        timeStamp: moment().add(5, 'second'),
        temp: true,
        status: STATUS_ACTIVE,
    },

    {
        key: uuidv4(),
        ledgerKey: ledgerGamming,
        description: 'Xbox Series X',
        category : categories[3],
        type: 'Credit',
        amount: 550,
        date: moment(),
        timeStamp: moment().add(4, 'second'),
        temp: true,
        status: STATUS_ACTIVE,
    },
    {
        key: uuidv4(),
        ledgerKey: ledgerGamming,
        description: 'PS5',
        category : categories[3],
        type: 'Credit',
        amount: 550,
        date: moment(),
        timeStamp: moment().add(5, 'second'),
        temp: true,
        status: STATUS_ACTIVE,
    },
];

export const SampleLedgers = [
    {
        key: ledgerKeyCurrentExpenses,
        name: 'Current Expenses',
        description: '',
        timeStamp: moment(),
        closed: false,
    },
    {
        key: ledgerKeyHouse,
        name: 'House Remodelation',
        description: 'Project for remodelate the house',
        timeStamp: moment(),
        closed: false,
    },
    {
        key: ledgerGamming,
        name: 'New game console',
        description: 'Bought several games and consoles',
        timeStamp: moment(),
        closed: false,
    }
]
