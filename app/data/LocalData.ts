import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';

import * as Period from './Periods';
import { PERIOD, RANGE, STATUS_ACTIVE, STATUS_TEMP, STATUS_CANCELED, STATUS_RELATED, STATUS_RESULT, STATUS_MERGED } from '../data/Contants';

import { Record, DataPoint, ChartData, PieChartData, TotalData, Ledger, LinkType } from './Types';
import { SampleRecords, SampleLedgers } from './TestData';
/**
 * The Singleton class defines the `getInstance` method that lets clients access
 * the unique singleton instance.
 */
export default class LocalData {
    private static instance: LocalData;
    private ledgers: Ledger[];
    private records: Record[];
    private categories: string[];
    private colors: string[];
    private categoriesColors: {};
    private tagColors: {};
    private availableColorsName: {};
    private availableColorsHex: {};

    /**
     * The Singleton's constructor should always be private to prevent direct
     * construction calls with the `new` operator.
     */
    private constructor() { 
        this.records = [...SampleRecords];
        this.ledgers = [...SampleLedgers];

        this.availableColorsName = {
            blue: '#91d5ff',
            green: '#b7eb8f',
            orange: '#ffd591',
            magenta: '#ffadd2',
            red: '#ffa39e',
            volcano: '#ffbb96',
            gold: '#ffe58f',
            lime: '#eaff8f',
            cyan: '#87e8de',
            geekblue: '#adc6ff',
            purple: '#d3adf7'
        };

        this.availableColorsHex = {
            '#91d5ff': 'blue',
            '#b7eb8f': 'green',
            '#ffd591': 'orange',
            '#ffadd2': 'magenta',
            '#ffa39e': 'red',
            '#ffbb96': 'volcano',
            '#ffe58f': 'gold',
            '#eaff8f': 'lime',
            '#87e8de': 'cyan',
            '#adc6ff': 'geekblue',
            '#d3adf7': 'purple',
        };

        this.categories = ['Home','Food','Gas','Entertainment'];        
        this.categoriesColors = {};
        this.colors = ['#91d5ff', '#b7eb8f', '#ffd591', '#ffadd2'];
        //this.colors = ['#e6f7ff', '#f6ffed', '#fff7e6', '#fff0f6'];
        //this.colors = ['#1890ff', '#52c41a', '#fa8c16', '#eb2f96'];
        for(let i = 0; i < this.categories.length; i++) {
            this.categoriesColors[this.categories[i]] = this.colors[i];
        }

        this.tagColors = {
            'Home': 'blue',
            'Food': 'green',
            'Gas' : 'orange',
            'Entertainment' : 'magenta'
        };
    }

    /**
     * The static method that controls the access to the singleton instance.
     *
     * This implementation let you subclass the Singleton class while keeping
     * just one instance of each subclass around.
     */
    public static getInstance(): LocalData {
        if (!LocalData.instance) {
            LocalData.instance = new LocalData();
        }

        return LocalData.instance;
    }

    public getLedgers(): Ledger[] {
        return this.ledgers;
    }

    public addLedger(ledger: Ledger): string {
        ledger.key = uuidv4();
        ledger.timeStamp = moment();
        // ledger.name = 'Untitled';
        // ledger.description = '';

        this.ledgers.push(ledger);
        return ledger.key;
    }

    public closeLedger(ledgerKey: string): boolean {
        const index = this.ledgers.findIndex(ledger => ledger.key === ledgerKey);
        this.ledgers[index].closed = true;
        return true;
    }

    public updateLedger(ledgerKey: string, name: string, description: string) {
        const index = this.ledgers.findIndex(ledger => ledger.key === ledgerKey);
        this.ledgers[index].name = name;
        this.ledgers[index].description = description;
    }

    private recordsLedger(ledgerKey: string): Record[] {
        return this.records.filter(record => record.ledgerKey == ledgerKey);
    }

    public getLastRecords(ledgerKey: string): Record[] {
        return this.recordsLedger(ledgerKey).sort((a,b) => {
            if (a.timeStamp.unix() < b.timeStamp.unix()) {
                return 1;
            }
            if (a.timeStamp.unix() > b.timeStamp.unix()) {
                return -1;
            }
            return 0;
        });
    }

    private updateLinkedRecords(ledgerKey: string) {
        // update record with new date and new total, that is linked to this ledger
        const ledger = this.ledgers.find(ledger => ledger.key === ledgerKey);
        if (ledger.linkedLedgers && ledger.linkedLedgers.length > 0) {
            ledger.linkedLedgers.forEach(linkedLedger => {
                const index = this.records.findIndex(record => record.key === linkedLedger.recordKey);
                // console.log('record to update', this.records[index] );
                // console.log("new total", this.getTotalAllRecords(ledgerKey));
                this.records[index].date = this.getLastRecord(ledgerKey).date;
                this.records[index].amount = this.getTotalAllRecords(ledgerKey).total * -1;
            });
        }
    }

    private insertRecord(recordData: Record, ledgerKey: string) {
        const newKey = uuidv4();
        if (recordData.ledgerRelated) {
            recordData.status = STATUS_RESULT;

            const index = this.ledgers.findIndex(ledger => ledger.key === recordData.ledgerRelated);
            recordData.ledgerNameRelated = this.ledgers[index].name;

            if (!this.ledgers[index].linkedLedgers) {
                this.ledgers[index].linkedLedgers = [];
            }
            // save the ledger and the record that create a relation with the ledger
            // a way to know what record to update if something changed in the linked ledger
            this.ledgers[index].linkedLedgers.push({
                ledgerKey: ledgerKey,
                recordKey: newKey,
            });

            this.ledgers[index].linkType = LinkType.LINK;
            if (recordData.closeLedgerRelated) {
                this.ledgers[index].closed = true;
            }
        }
        else if (recordData.temp) {
            recordData.status = STATUS_TEMP;
        }
        else {
            recordData.status = STATUS_ACTIVE;
        }

        recordData.key = newKey;
        recordData.ledgerKey = ledgerKey;        
        console.log("Record to add", recordData);
        this.records.push(recordData);
    }

    public addRecord(recordData: Record, ledgerKey: string) {
        if (recordData.temp) {
            const index = this.records.findIndex(record => record.key === recordData.key);
            if (index >= 0) {
                recordData.status = STATUS_TEMP;
                recordData.ledgerKey = ledgerKey;
                this.records[index] = recordData;
            }
            else {
                this.insertRecord(recordData, ledgerKey);
            }
        }
        else {
            this.insertRecord(recordData, ledgerKey);
        }

        this.updateLinkedRecords(ledgerKey);
    }

    public removeRecord(recordKey: string) {
        const index = this.records.findIndex(record => record.key === recordKey);
        if (index >= 0) {
            const record = this.records[index];
            this.records.splice(index,1);
            this.updateLinkedRecords(record.ledgerKey);
        }
    }

    public getRecord(recordKey: string): Record {
        return this.records.find(record => record.key === recordKey);
    }

    public cancelRecord(recordKey: string) {
        // const record = this.records.find(record => record.key === recordKey);
        const index = this.records.findIndex(record => record.key === recordKey);
        if (index >= 0) {
            const record = this.records[index];
            record.status = STATUS_CANCELED;
            this.records[index] = record;

            // console.log("Record Canceled", record);
            const newRecord = {
                ...record,
                key: uuidv4(),
                type: record.type === 'Credit' ? 'Debit' : 'Credit',
                date: moment(),
                timeStamp: moment(),
                status: STATUS_RELATED
            };
            // console.log("Record Related ", newRecord, STATUS_RELATED);
            this.records.push(newRecord);
            this.updateLinkedRecords(record.ledgerKey);
        }
    }

    public saveTemporalRecords(ledgerKey: string) {
        const records = this.records.filter(record => record.temp);
        records.forEach(record => {
            const newRecord = {
                ...record,
                temp: false,
            };
            this.removeRecord(newRecord.key);
            this.addRecord(newRecord, ledgerKey);
        });

        this.updateLinkedRecords(ledgerKey);
    }

    public addCategory(category: string, color: string) {
        this.categories.push(category);
        this.setColorCategory(category, color);
    }

    public getCategories(): string[] {
        return this.categories;
    }

    public setColorCategory(category: string, color:string) {
        this.categoriesColors[category] = color;
        this.tagColors[category] = this.availableColorsHex[color];
    }

    public getTagColors() {
        return this.tagColors;
    }
    public getAvailableColorsName() {
        return this.availableColorsName;
    }
    public getAvailableColorsHex() {
        return this.availableColorsHex;
    }

    public getLastRecord(ledgerKey: string): Record {
        const records = this.recordsLedger(ledgerKey).sort((a,b) => {
            if (a.date.unix() < b.date.unix()) {
                return 1;
            }
            if (a.date.unix() > b.date.unix()) {
                return -1;
            }
            return 0;
        });

        return records[0];
    }

    public mergeLedgers(destinyLedgerKey: string, sourceLedgerKey: string): boolean {
        const records = this.recordsLedger(sourceLedgerKey);
        const index = this.ledgers.findIndex(ledger => ledger.key === sourceLedgerKey);
        const nameSource = this.ledgers[index].name;
        this.ledgers[index].closed = true;
        this.ledgers[index].linkType = LinkType.MERGE;

        if (!this.ledgers[index].linkedLedgers) {
            this.ledgers[index].linkedLedgers = [];
        }
        // save the ledger and the record that create a relation with the ledger
        // a way to know what record to update if something changed in the linked ledger
        this.ledgers[index].linkedLedgers.push({
            ledgerKey: destinyLedgerKey,
        });

        records.forEach(record => {
            this.records.push({
                ...record,
                key: uuidv4(),
                temp: false,
                timeStamp: moment(),
                ledgerKey: destinyLedgerKey,
                ledgerNameRelated: nameSource,
                ledgerRelated: sourceLedgerKey,
                status: STATUS_MERGED
            });
        });

        this.updateLinkedRecords(destinyLedgerKey);

        return true;
    }

    // agregations and chart
    public getRecordsMonth(ledgerKey: string, month: number): Record[] {
        return this.recordsLedger(ledgerKey).filter(record => record.date.month() === month);
    }

    public getRecordsYear(ledgerKey: string, year: number): Record[] {
        return this.recordsLedger(ledgerKey).filter(record => record.date.year() === year);
    }

    private getRange(periodId: string): Period.PeriodRange {
        switch (periodId) {
            case 'CURRENT_WEEK': return Period.currentWeek();
            case 'LAST_WEEK': return Period.lastWeek();
            case 'LAST_TWO_WEEKS': return Period.lastTwoWeeks();
            case 'CURRENT_MONTH': return Period.currentMonth();
            case 'LAST_MONTH': return Period.lastMonth();
            case 'LAST_TWO_MONTHS': return Period.lastTwoMonths();
            case 'LAST_THREE_MONTHS': return Period.lastThreeMonths();
            case 'CURRENT_QUARTER': return Period.currentQuarter();
            case 'LAST_QUARTER': return Period.lastQuarter();
            case 'CURRENT_YEAR': return Period.currentYear();
            case 'LAST_YEAR': return Period.lastYear();
            case 'LAST_7_DAYS': return Period.last7Days();
            case 'LAST_14_DAYS': return Period.last14Days();
            case 'LAST_30_DAYS': return Period.last30Days();
        }
    }

    public sortByTimeStamp(records: Record[]): Record[] {
        return records.sort((a,b) => {
            if (a.timeStamp.unix() < b.timeStamp.unix()) {
                return 1;
            }
            if (a.timeStamp.unix() > b.timeStamp.unix()) {
                return -1;
            }
            return 0;
        });
    }

    public getRecordsByPeriodRange(ledgerKey: string, start:any, end:any): Record[] {
        return this.recordsLedger(ledgerKey).filter(record => 
            // console.log(record);
            record.date.isBetween(start, end, undefined, '[]')
        );
    }

    public getRecordsByPeriodId(ledgerKey: string, periodId: string): Record[] {
        const range:Period.PeriodRange = this.getRange(periodId);
        return this.sortByTimeStamp(this.getRecordsByPeriodRange(ledgerKey, range.start, range.end));
    }

    private getRecords(ledgerKey: string, type: string, value: any) {
        //console.log("VALORES ", type, value);
        switch (type) {
            case PERIOD: return this.getRecordsByPeriodId(ledgerKey, value);
            case RANGE: return this.getRecordsByPeriodRange(ledgerKey, value.start, value.end);
            default: return this.getRecordsByPeriodId(ledgerKey, value);
        }
    }

    public getRecordsByPeriod(ledgerKey: string, type: string, value: any): Record[] {
        return this.getRecords(ledgerKey, type, value);
    }

    public getRecordDailyChartData(ledgerKey: string, type: string, value: any): ChartData[] {
        return this.getChartData(this.getRecords(ledgerKey, type, value));
    }

    public getCategoryPieData(ledgerKey: string, type: string, value: any): PieChartData[] {
        return this.getPieChartData(this.getRecords(ledgerKey, type, value));
    }

    public getTotal(ledgerKey: string, type: string, value: any): TotalData {
        return this.sumCreditDebit(this.getRecords(ledgerKey, type, value));
    }

    public getTotalAllRecords(ledgerKey: string): TotalData {
        return this.sumCreditDebit(this.recordsLedger(ledgerKey));
    }

    public getLast7DaysChartData(ledgerKey: string, type: string, value: any): any[] {
        return this.getDataLast7DaysChartData(this.getRecords(ledgerKey, type, value));
    }

    public getRelatedLedgers(ledgerKey: string): string[] {
        const records = this.recordsLedger(ledgerKey);
        return records.filter(record => record.status === STATUS_RESULT).map(record => record.ledgerRelated);
    }

    public getLedgersToRelate(ledgerKey: string): Ledger[] {
        const relatedLedgers = this.getRelatedLedgers(ledgerKey);
        const toRelate = this.ledgers.filter(ledger => ledger.key !== ledgerKey && !relatedLedgers.includes(ledger.key));  
        return toRelate;
    }

    public getChartData(records: Record[]): ChartData[] {
        const daysInMonth:number = moment().daysInMonth();
        const categories = this.getCategoriesInRecords(records);
      
        let data:ChartData[] = Object.keys(categories).reduce(
            (data, key) => {
                data.push({
                    id: key,
                    data: this.getDaysMonthList(daysInMonth)
                });
                return data;
            },
            []
        );

        records.forEach(record => {
            const pos = data.findIndex(elm => elm.id === record.category);
            data[pos].data[record.date.date()-1].y += Number(record.amount);
        });
      
        return data;
    }

    private getDaysMonthList(daysInMonth: number): DataPoint[] {
        let days: DataPoint[] = [];
        for (let i=1; i <= daysInMonth; i++) {
            days.push({
                x: `${i}`,
                y: 0
            });
        }
        return days;
    }

    private getCategoriesInRecords(records: Record[]) {
        return records.reduce(
            (group, record) => {
                if (!group.hasOwnProperty(record.category)) {
                    group[record.category] = 0;
                }
                group[record.category] = group[record.category] + Number(record.amount);
                return group;
            },
            {}
        );
    }

    public getMonthlyChartData(ledgerKey: string): ChartData[] {
        return this.getMonthlyChartData2(
                this.getRecordsYear(ledgerKey, moment().year())
            );
    }

    public getMonthlyChartData2(records: Record[]): ChartData[] {
        const categories = this.getCategoriesInRecords(records);
      
        let data: ChartData[] = Object.keys(categories).reduce(
            (data, key) => {
                data.push({
                    id: key,
                    data: this.getMonthsYearList()
                });
                return data;
            },
            []
        );

        records.forEach(record => {
            const pos = data.findIndex(elm => elm.id === record.category);
            data[pos].data[record.date.month()].y += Number(record.amount);
        });
      
        return data;
    }

    private getMonthsYearList(): DataPoint[] {
        let days: DataPoint[] = [];
        for (let i=1; i <= 12; i++) {
            days.push({
                x: `${i}`,
                y: 0
            });
        }
        return days;
    }
    
    private getPieChartData(records: Record[]): PieChartData[] {
        return records.reduce(
            (list, record) => {
                const pos = list.findIndex(elm => elm.id === record.category);
                if (pos === -1) {
                    list.push({
                        id: record.category,
                        label: record.category,
                        value: Number(record.amount),
                        color: this.categoriesColors[record.category],
                    });
                }
                else {
                    list[pos].value += Number(record.amount);
                }
                return list;
            },
            []
        );
    }

    private sumCreditDebit(records: Record[]): TotalData {
        const recordToSum = records.filter(record => record.status !== STATUS_CANCELED)
        console.log('Calculate sumCredit', recordToSum);
        const totalCredit = recordToSum.reduce((total, record) => {
            if (record.type === 'Credit') total += Number(record.amount);
            return total
        }, 0);
        const totalDebit = recordToSum.reduce((total, record) => {
            if (record.type === 'Debit') total += Number(record.amount);
            return total
        }, 0);

        return {
            totalCredit,
            totalDebit,
            total: totalDebit - totalCredit
        }
    }

    private getDataLast7DaysChartData(records: Record[]):any[] {
        if (!records) return [];
        const start = moment().subtract(7, 'day').startOf('day');

        return records
            .filter(record => (moment(record.date).isAfter(start)))
            .reduce((rows, record) => {
                const day = record.date.date();
                const index = rows.findIndex(row => row.day === day);
                if (index === -1) {
                    rows.push({
                        day,
                        ... {[record.category] : record.amount},
                        ... {[record.category+'-color']: this.categoriesColors[record.category]}
                    });
                }
                else {
                    if (!rows[index][record.category]) {
                        rows[index][record.category] = 0;
                        rows[index][record.category+'-color'] = this.categoriesColors[record.category];
                    }
                    rows[index][record.category] += record.amount;
                }
                return rows;
            }, [])
            .sort((a, b) => a.day - b.day);
        // console.log(records);
        // return records;
        // [
        //     {
        //       "country": "AD",
        //       "hot dog": 149,
        //       "hot dogColor": "hsl(152, 70%, 50%)",
        //       "burger": 117,
        //       "burgerColor": "hsl(40, 70%, 50%)",
        //       "sandwich": 9,
        //       "sandwichColor": "hsl(47, 70%, 50%)",
        //       "kebab": 43,
        //       "kebabColor": "hsl(102, 70%, 50%)",
        //       "fries": 140,
        //       "friesColor": "hsl(275, 70%, 50%)",
        //       "donut": 189,
        //       "donutColor": "hsl(343, 70%, 50%)"
        //     },
        // ]
    }
    
    //   const getData = (records: Record[]) => {
    //     // const month = moment().month()  // jan=0, dec=11
    //     // const year = moment().year()
    //     const daysInMonth = moment().daysInMonth();
    //     const categories = getCategories(records);
      
    //     let data = Object.keys(categories).reduce(
    //       (data, key) => {
    //         data.push({
    //           id: key,
    //           data: getDaysMonthList(daysInMonth)
    //         });
    //         return data;
    //       },
    //       []
    //     );
      
    //     // console.log('records', getCategories(records));
    //     console.log(data);
      
    //     records.forEach(record => {
    //       // days[record.date.date()-1].y = Number(record.amount);
    //       const pos = data.findIndex(elm => elm.id === record.category);
    //       // console.log(pos, record.category);
    //       data[pos].data[record.date.date()-1].y = Number(record.amount);
    //     });
      
    //     console.log("data", data);
    //     return data;
    //   }     
}