import moment from 'moment';

export interface PeriodRange {
    start: any,
    end: any
}

export const currentWeek = (): PeriodRange => {
    const currentDate = moment();
    const weekStart = currentDate.clone().startOf('isoWeek');
    const weekEnd = currentDate.clone().endOf('isoWeek');

    return {
        start: weekStart,
        end: weekEnd
    }
}

export const lastWeek = (): PeriodRange => {
    const weekStart = moment().subtract(1, 'weeks').startOf('isoWeek');
    const weekEnd = moment().subtract(1, 'weeks').endOf('isoWeek');

    return {
        start: weekStart,
        end: weekEnd
    }
}

export const lastTwoWeeks = (): PeriodRange => {
    const weekStart = moment().subtract(2, 'weeks').startOf('isoWeek');
    const weekEnd = moment().subtract(2, 'weeks').endOf('isoWeek');

    return {
        start: weekStart,
        end: weekEnd
    }
}

export const currentMonth = (): PeriodRange => {
    const monthStart = moment().startOf('month');
    const monthEnd = moment().endOf('month');

    return {
        start: monthStart,
        end: monthEnd
    }
}

export const lastMonth = (): PeriodRange => {
    const monthStart = moment().subtract(1, 'month').startOf('month');
    const monthEnd = moment().subtract(1, 'month').endOf('month');

    return {
        start: monthStart,
        end: monthEnd
    }
}

export const lastTwoMonths = (): PeriodRange => {
    const monthStart = moment().subtract(2, 'month').startOf('month');
    const monthEnd = moment().subtract(2, 'month').endOf('month');

    return {
        start: monthStart,
        end: monthEnd
    }
}

export const lastThreeMonths = (): PeriodRange => {
    const monthStart = moment().subtract(3, 'month').startOf('month');
    const monthEnd = moment().subtract(3, 'month').endOf('month');

    return {
        start: monthStart,
        end: monthEnd
    }
}

export const currentQuarter = (): PeriodRange => {
    const quarterStart = moment().quarter(moment().quarter()).startOf('quarter');
    const quarterEnd = moment().quarter(moment().quarter()).endOf('quarter');

    return {
        start: quarterStart,
        end: quarterEnd
    }
}

export const lastQuarter = (): PeriodRange => {
    const quarterStart = moment()
        .quarter(moment().subtract(3, 'month').quarter())
        .startOf('quarter');
    const quarterEnd = moment()
        .quarter(moment().subtract(3, 'month').quarter())
        .endOf('quarter');

    return {
        start: quarterStart,
        end: quarterEnd
    }
}

export const currentYear = (): PeriodRange => {
    const yearStart = moment().startOf('year');
    const yearEnd = moment().endOf('year');

    return {
        start: yearStart,
        end: yearEnd
    }
}

export const lastYear = (): PeriodRange => {
    const yearStart = moment().subtract(1, 'year').startOf('year');
    const yearEnd = moment().subtract(1, 'year').endOf('year');

    return {
        start: yearStart,
        end: yearEnd
    }
}

export const last7Days = (): PeriodRange => {
    const end = moment().startOf('day');
    const start = moment().subtract(7, 'day').endOf('day');

    return {
        start,
        end
    }
}

export const last14Days = (): PeriodRange => {
    const end = moment().startOf('day');
    const start = moment().subtract(14, 'day').endOf('day');

    return {
        start,
        end
    }
}

export const last30Days = (): PeriodRange => {
    const end = moment().startOf('day');
    const start = moment().subtract(30, 'day').endOf('day');

    return {
        start,
        end
    }
}
