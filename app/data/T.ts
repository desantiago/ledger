// interface Message {
//     id: string;
//     integer: number;
// }

// interface StoredMessage {
//     id: string;
//     integer: number;
//     time: number;
// }

// class WindowM {
//     private messages:StoredMessage[];
//     private numOfMiliseconds;

//     WindowM(numOfMilisecond:number) {
//         this.numOfMiliseconds = numOfMilisecond;
//     }

//     public addMessage(message: Message) {
//         const storedMessage = {
//             id: message.id,
//             integer: message.integer,
//             time: new Date().getMilliseconds()
//         }
//         this.messages.push({...message, time: new Date().getMilliseconds()});
//     }

//     public getMessage(key: string):Message {
//         const storedMessage = this.messages.find(message => message.id === key);
//         if (!this.timeHasPassed(storedMessage)) {
//             return {
//                 id: storedMessage.id,
//                 integer: storedMessage.integer
//             }
//         }
//         else {
//             return null;
//         }
//     }

//     private timeHasPassed(storedMessage: StoredMessage): boolean {
//         const actualTime = new Date().getMilliseconds()
//         const timePassed = actualTime - storedMessage.time;
//         return (timePassed < this.numOfMiliseconds);
//     }

//     public getAverage():number {
//         const messages = this.messages.filter((message) => !this.timeHasPassed(message));
//         const total = messages.reduce( (total, message) => total += message.integer, 0);
//         return total / messages.length;
//     }
// }
