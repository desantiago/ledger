
// export const Options = [
//     { id: 'CURRENT_WEEK', text: 'Current Week' },
//     { id: 'LAST_WEEK', text: 'Previous Week' },
//     { id: 'LAST_TWO_WEEKS', text: 'Last Two Weeks' },
//     { id: 'CURRENT_MONTH', text: 'Current Month' },
//     { id: 'LAST_MONTH', text: 'Last Months' },
//     { id: 'LAST_TWO_MONTHS', text: 'Last Two Months' },
//     { id: 'LAST_THREE_MONTHS', text: 'Last Three Months' },
//     { id: 'CURRENT_QUARTER', text: 'Current Quarter' },
//     { id: 'LAST_QUARTER', text: 'Last Quarter' },
//     { id: 'CURRENT_YEAR', text: 'Current Year' },
//     { id: 'LAST_YEAR', text: 'Last Year' },
//     { id: 'LAST_7_DAYS', text: 'Last 7 Days' },
//     { id: 'LAST_14_DAYS', text: 'Last 14 Days' },
//     { id: 'LAST_30_DAYS', text: 'Last 30 Days' },
// ];

export const Options = [
    { id: 'CURRENT_WEEK'},
    { id: 'LAST_WEEK'},
    { id: 'LAST_TWO_WEEKS'},
    { id: 'CURRENT_MONTH'},
    { id: 'LAST_MONTH'},
    { id: 'LAST_TWO_MONTHS'},
    { id: 'LAST_THREE_MONTHS'},
    { id: 'CURRENT_QUARTER'},
    { id: 'LAST_QUARTER'},
    { id: 'CURRENT_YEAR'},
    { id: 'LAST_YEAR'},
    { id: 'LAST_7_DAYS'},
    { id: 'LAST_14_DAYS'},
    { id: 'LAST_30_DAYS'},
];

export const PERIOD = 'PERIOD';
export const RANGE = 'RANGE';

export const NEW_RECORD = 'NEW_RECORD';
export const EDIT_RECORD = 'EDIT_RECORD';

export const STATUS_CANCELED = 'CANCELED';
export const STATUS_RELATED = 'RELATED';
export const STATUS_ACTIVE = 'ACTIVE';
export const STATUS_TEMP = 'TEMP';
export const STATUS_RESULT = 'RESULT';
export const STATUS_MERGED = 'MERGED';