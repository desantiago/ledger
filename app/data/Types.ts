export interface Record {
    key: string;
    ledgerKey: string;
    description: string;
    category: string;
    type: string;
    amount: number;
    date: any;
    timeStamp: any;
    temp: boolean;
    status: string;
    keyRelated?: string;

    ledgerRelated?: string;
    ledgerNameRelated?: string;
    closeLedgerRelated?: boolean;
}

export enum LinkType {
    LINK,
    MERGE
}

export interface LinkedLedger {
    ledgerKey: string;
    recordKey?: string;
}

export interface Ledger {
    key: string;
    name: string;
    description?: string;
    timeStamp: any;
    closed: boolean;

    linkedLedgers?: LinkedLedger[];
    linkType?: LinkType;
}

export interface LinkedRecord {
    ledgerName: string,
    description: string,
    category: string,
}

export interface DataPoint {
    x: string;
    y: number;
}

export interface ChartData {
    id: string;
    color?: string;
    data: DataPoint[];
}

export interface PieChartData {
    id: string;
    color?: string;
    label: string;
    value: string;
}

export interface TotalData {
    totalCredit: number;
    totalDebit: number;
    total: number;
}
