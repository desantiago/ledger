import { combineReducers, createStore } from "redux";
import {MakeStore, createWrapper, Context, HYDRATE} from 'next-redux-wrapper';

import { reducer, State } from './reducer';
// import reducerB from './reducerB';
// export default (preloadState, options) => {
//   return createStore(
//     combineReducers({
//       reducer,
//     //   reducerB
//     }),
//     preloadState
//   )
// }

// create a makeStore function
const makeStore: MakeStore<State> = (context: Context) => createStore(reducer);

// export an assembled wrapper
export const wrapper = createWrapper<State>(makeStore, {debug: true});