import {AnyAction} from 'redux';
import {HYDRATE} from 'next-redux-wrapper';

import moment from 'moment';

import { Record, ChartData, PieChartData, TotalData, Ledger } from '../data/Types';
import Data from '../data/Data';
import { Options } from '../data/Contants';
import { PERIOD, RANGE, NEW_RECORD, EDIT_RECORD, STATUS_ACTIVE } from '../data/Contants';
import { PeriodRange } from '../data/Periods';
import { timeStamp } from 'console';

export const INCREMENT_COUNTER = "INCREMENT_COUNTER";
export const DECREMENT_COUNTER = "DECREMENT_COUNTER";
export const ADD_RECORD = "ADD_RECORD";
export const ADD_CATEGORY = "ADD_CATEGORY";
export const CHANGE_PERIOD = "CHANGE_PERIOD";
export const EDIT_TEMP_RECORD = "EDIT_TEMP_RECORD";
export const DELETE_TEMP_RECORD = "DELETE_TEMP_RECORD";
export const CLONE_RECORD = "CLONE_RECORD";
export const CANCEL_RECORD = "CANCEL_RECORD";
export const NEW_RECORD_MODE = "NEW_RECORD_MODE";
export const SAVE_TEMP_RECORDS = "SAVE_TEMP_RECORDS";
export const ADD_LEDGER = "ADD_LEDGER";
export const CHANGE_LEDGER = "CHANGE_LEDGER";
export const UPDATE_LEDGER = "UPDATE_LEDGER";
export const VIEW_RELATED_LEDGER = "VIEW_RELATED_LEDGER";
export const HIDE_RELATED_LEDGER = "HIDE_RELATED_LEDGER";
export const MERGE_LEDGER = "MERGE_LEDGER";
export const CLOSE_LEDGER = "CLOSE_LEDGER";
export const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE';

export interface State {
    tick: string;
    counter: number;
    records: Record[];
    dailyChartData: ChartData[];
    monthlyChartData: ChartData[];
    categoryChartData: PieChartData[];
    totalData: TotalData;
    categories: string[];
    app: string;
    page: string;
    selectedPeriodType: string;
    selectedPeriod: string;
    selectedRange: PeriodRange;
    editMode: string;
    editingRecord: Record;
    ledgers: Ledger[];
    selectedLedger: string;
    last7DaysChartData: any[];
    recordsRelatedLedger: Record[];
    viewRelatedLedger: boolean;
    selectedRelatedLedger: string;
    tagColors: {};
    language: string;    
}

const data = Data.getInstance();

const getValueSelectedPeriod = (state): any => {
    switch (state.selectedPeriodType) {
        case PERIOD: return state.selectedPeriod;
        case RANGE: return state.selectedRange;
        default: return state.selectedPeriod;
    }
}

const emptyRecord = (categories) => {
    return {
        key: '',
        ledgerKey: '',
        description: '',
        category: categories[0],
        type: 'Credit',
        amount: 0,
        date: moment(),
        timeStamp: moment(),
        temp: false,
        status: STATUS_ACTIVE
    }
}

const defaultLedger = data.getLedgers()[0].key;

const loadRecords = (state) => {
    const { selectedLedger, selectedPeriodType } = state;
    const valueSelectedPeriod = getValueSelectedPeriod(state);

    return {
        records: [...data.getRecordsByPeriod(selectedLedger, selectedPeriodType, valueSelectedPeriod)],
        dailyChartData: [...data.getRecordDailyChartData(selectedLedger, selectedPeriodType, valueSelectedPeriod)],
        monthlyChartData: [...data.getMonthlyChartData(selectedLedger)],
        categoryChartData: [...data.getCategoryPieData(selectedLedger, selectedPeriodType, valueSelectedPeriod)],
        totalData: {...data.getTotal(selectedLedger, selectedPeriodType, valueSelectedPeriod)},
        last7DaysChartData: [...data.getLast7DaysChartData(selectedLedger, selectedPeriodType, valueSelectedPeriod)],
    }
}

export const reducer = (state: State = {
    tick: 'init',
    counter: 0,
    records: data.getRecordsByPeriod(defaultLedger, PERIOD, Options[0].id),
    dailyChartData: data.getRecordDailyChartData(defaultLedger, PERIOD, Options[0].id),
    monthlyChartData: [],
    categoryChartData: data.getCategoryPieData(defaultLedger, PERIOD, Options[0].id),
    totalData: data.getTotal(defaultLedger, PERIOD, Options[0].id),
    categories: data.getCategories(),
    selectedPeriod: Options[0].id,
    selectedRange: { start: moment(), end: moment() },
    selectedPeriodType: PERIOD,
    app: 'testng-react',
    page: '',
    editingRecord: emptyRecord(data.getCategories()),
    editMode: NEW_RECORD,
    ledgers: data.getLedgers(),
    selectedLedger: defaultLedger,
    last7DaysChartData: data.getLast7DaysChartData(defaultLedger, PERIOD, Options[0].id),
    recordsRelatedLedger: [],
    viewRelatedLedger: false,
    selectedRelatedLedger: '',
    tagColors: data.getTagColors(),
    language: 'sp'
}, action: AnyAction) => {

    switch (action.type) {
        case HYDRATE:
            const nextState = {
                ...state, // use previous state
                ...action.payload, // apply delta from hydration
            }
            if (state.counter) nextState.count = state.counter // preserve count value on client side navigation
            if (state.records) nextState.records = state.records;
            if (state.dailyChartData) nextState.dailyChartData = state.dailyChartData;
            if (state.categoryChartData) nextState.categoryChartData = state.categoryChartData;
            if (state.totalData) nextState.totalData = state.totalData;
            if (state.categories) nextState.categories = state.categories;
            if (state.selectedPeriod) nextState.selectedPeriod = state.selectedPeriod;
            if (state.selectedRange) nextState.selectedRange = state.selectedRange;
            if (state.selectedPeriodType) nextState.selectedPeriodType = state.selectedPeriodType;
            if (state.ledgers) nextState.ledgers = state.ledgers;
            if (state.selectedLedger) nextState.selectedLedger = state.selectedLedger;
            if (state.last7DaysChartData) nextState.last7DaysChartData = state.last7DaysChartData;
            if (state.recordsRelatedLedger) nextState.recordsRelatedLedger = state.recordsRelatedLedger;
            if (state.viewRelatedLedger) nextState.viewRelatedLedger = state.viewRelatedLedger;
            if (state.selectedRelatedLedger) nextState.selectedRelatedLedger = state.selectedRelatedLedger;
            if (state.tagColors) nextState.tagColors = state.tagColors;
            return nextState     
        case 'reducerA/setCounter':
            return {
                ...state,
                counter: action.payload
            }
        case "APP":
                return { ...state, app: action.payload };
        case "PAGE":
                return { ...state, page: action.payload };            
        case INCREMENT_COUNTER:
            return {...state, counter: state.counter + 1};
        case DECREMENT_COUNTER:
            return {...state, counter: state.counter - 1};
        case ADD_RECORD:
            data.addRecord(action.payload, state.selectedLedger);
            // console.log(data.getLastRecords());
            return {
                ...state, 
                ...loadRecords(state),
                // records: [...data.getRecordsByPeriod(state.selectedPeriodType, getValueSelectedPeriod(state))], 
                // dailyChartData: [...data.getRecordDailyChartData(state.selectedPeriodType, getValueSelectedPeriod(state))],
                // monthlyChartData: [...data.getMonthlyChartData()],
                // categoryChartData: [...data.getCategoryPieData(state.selectedPeriodType, getValueSelectedPeriod(state))],
                // totalData: {...data.getTotal(state.selectedPeriodType, getValueSelectedPeriod(state))},
                editMode: NEW_RECORD,
                editingRecord: {...emptyRecord(data.getCategories())},
                ledgers: [...data.getLedgers()],
            };
        case ADD_CATEGORY:
            console.log("ADD_CATEGORY ", action.payload);
            data.addCategory(action.payload.category, action.payload.color);
            return {
                ...state,
                categories: [...data.getCategories()],
                tagColors: {...data.getTagColors()}
            }
        case CHANGE_PERIOD:
            //console.log(action);
            const { type, value } = action.payload;

            return {
                ...state,
                selectedPeriod: type === PERIOD ? value : state.selectedPeriod,
                selectedRange: type === RANGE ? value : state.selectedRange,
                selectedPeriodType: type,
                records: data.getRecordsByPeriod(state.selectedLedger, type, value),
                dailyChartData: [...data.getRecordDailyChartData(state.selectedLedger, type, value)],
                categoryChartData: [...data.getCategoryPieData(state.selectedLedger, type, value)],
                totalData: {...data.getTotal(state.selectedLedger, type, value)}, 
                last7DaysChartData: [...data.getLast7DaysChartData(state.selectedLedger, type, value)]
            }
        case EDIT_TEMP_RECORD:
            return {
                ...state,
                editMode: EDIT_RECORD,
                editingRecord: {...data.getRecord(action.payload)}
            }
        case DELETE_TEMP_RECORD:
            // payload contains the record key
            data.removeRecord(action.payload);
            // const alldata = loadRecords(state);
            return {
                ...state,
                ...loadRecords(state),
                // monthlyChartData: [...data.getMonthlyChartData()],
                // records: [...data.getRecordsByPeriod(state.selectedPeriodType, getValueSelectedPeriod(state))], 
                // dailyChartData: [...data.getRecordDailyChartData(state.selectedPeriodType, getValueSelectedPeriod(state))],
                // categoryChartData: [...data.getCategoryPieData(state.selectedPeriodType, getValueSelectedPeriod(state))],
                // totalData: {...data.getTotal(state.selectedPeriodType, getValueSelectedPeriod(state))},
            }
        case CLONE_RECORD:
            //const recordKey = action.payload;
            const cloneRecord: Record = data.getRecord(action.payload);
            return {
                ...state,
                editMode: NEW_RECORD,
                editingRecord: {...cloneRecord}
            }
        case CANCEL_RECORD:
            data.cancelRecord(action.payload);
            return {
                ...state,
                ...loadRecords(state),
                // records: [...data.getRecordsByPeriod(state.selectedPeriodType, getValueSelectedPeriod(state))], 
                // dailyChartData: [...data.getRecordDailyChartData(state.selectedPeriodType, getValueSelectedPeriod(state))],
                // categoryChartData: [...data.getCategoryPieData(state.selectedPeriodType, getValueSelectedPeriod(state))],
                // totalData: {...data.getTotal(state.selectedPeriodType, getValueSelectedPeriod(state))},
            }
        case NEW_RECORD_MODE:
            return {
                ...state,
                editMode: NEW_RECORD,
                editingRecord: {...emptyRecord(data.getCategories())}
            }
        case SAVE_TEMP_RECORDS:
            data.saveTemporalRecords(state.selectedLedger);
            return {
                ...state,
                ...loadRecords(state),
                // records: [...data.getRecordsByPeriod(state.selectedPeriodType, getValueSelectedPeriod(state))], 
                // dailyChartData: [...data.getRecordDailyChartData(state.selectedPeriodType, getValueSelectedPeriod(state))],
                // categoryChartData: [...data.getCategoryPieData(state.selectedPeriodType, getValueSelectedPeriod(state))],
                // totalData: {...data.getTotal(state.selectedPeriodType, getValueSelectedPeriod(state))},
            }
        case ADD_LEDGER:
            const ledgerKey = data.addLedger(action.payload);
            return {
                ...state,
                ...loadRecords({...state, selectedLedger: ledgerKey}),
                selectedLedger: ledgerKey,
                ledgers: [...data.getLedgers()],
            }
        case CLOSE_LEDGER:
            // const { ledgerKey } = action.payload;
            data.closeLedger(action.payload.ledgerKey);
            return {
                ...state,
                ledgers: [...data.getLedgers()],
            }
        case CHANGE_LEDGER:
            return {
                ...state,
                ...loadRecords({...state, selectedLedger: action.payload}),
                selectedLedger: action.payload,
            }
        case UPDATE_LEDGER:
            const {name, description} = action.payload;
            data.updateLedger(state.selectedLedger, name, description);
            return {
                ...state,
                ledgers: [...data.getLedgers()]
            }
        case VIEW_RELATED_LEDGER:
            // console.log("VIEW_RELATED_LEDGER", data.getLastRecords(action.payload));
            return {
                ...state,
                recordsRelatedLedger: [...data.getLastRecords(action.payload)],
                viewRelatedLedger: true,
                selectedRelatedLedger: action.payload,
            }
        case HIDE_RELATED_LEDGER:
            return {
                ...state,
                viewRelatedLedger: false
            }
        case MERGE_LEDGER:            
            const {destiny, source} = action.payload;
            data.mergeLedgers(destiny, source);
            return {
                ...state,
                ...loadRecords(state),
            }
        case CHANGE_LANGUAGE:
            console.log('CHANGE_LANGUAGE', action.payload);
            return {
                ...state,
                language: action.payload.language
            }
        default:
            return state
    }
}
