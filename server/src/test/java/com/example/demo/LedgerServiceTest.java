package com.example.demo;

import com.example.demo.datastore.FakeLedgerDataStore;
import com.example.demo.jwt.JWTAuthenticationEntryPoint;
import com.example.demo.jwt.JWTUserDetailsService;
import com.example.demo.ledger.Journal;
import com.example.demo.ledger.LedgerController;
import com.example.demo.ledger.LedgerService;
import com.example.demo.ledger.Record;
import com.example.demo.utils.JWTTokenUtil;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@SpringBootTest
@RunWith(SpringRunner.class)
@WebMvcTest(LedgerController.class)
public class LedgerServiceTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private JWTUserDetailsService userDetailsService;

    @MockBean
    private JWTTokenUtil jwtTokenUtil;

    @MockBean
    private JWTAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @MockBean
    private LedgerService ledgerService;

//    @TestConfiguration
//    static class EmployeeServiceImplTestContextConfiguration {
//
//        @Bean
//        public LedgerService employeeService() {
//            return new LedgerService();
//        }
//    }
//
//    @Autowired
//    private final LedgerService ledgerService;
//
//    @Test
//    public void whenValidName_thenEmployeeShouldBeFound() {
//        List<Journal> journals = ledgerService.getAllJournals(FakeLedgerDataStore.USER_ID);
//        assertEquals(journals.size(), 1);
//    }

    static FakeLedgerDataStore fakeLedgerDataStore;

    @BeforeClass
    public static void setup() {
        fakeLedgerDataStore = new FakeLedgerDataStore();
    }

    @Test
    public void givenUserId_whenGetAllJournals_thenReturnJsonArray() throws Exception {

//        FakeLedgerDataStore fakeLedgerDataStore = new FakeLedgerDataStore();
        List<Journal> expectedJournals = fakeLedgerDataStore.getAllJournals(FakeLedgerDataStore.USER_ID);
        given(ledgerService.getAllJournals(FakeLedgerDataStore.USER_ID)).willReturn(fakeLedgerDataStore.getAllJournals(FakeLedgerDataStore.USER_ID));

        mvc.perform(get("/api/v1/ledger/journals")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].description", is(expectedJournals.get(0).getDescription())));
    }

    @Test
    public void givenJournalId_whenGetAllRecords_thenReturnJsonArray() throws Exception {

//        FakeLedgerDataStore fakeLedgerDataStore = new FakeLedgerDataStore();
        List<Journal> expectedJournals = fakeLedgerDataStore.getAllJournals(FakeLedgerDataStore.USER_ID);
        UUID journalId = expectedJournals.get(0).getId();

        given(ledgerService.getJournalRecords(journalId)).willReturn(fakeLedgerDataStore.getJournalRecords(journalId));

        List<Record> records = fakeLedgerDataStore.getJournalRecords(journalId).get();
//        System.out.println("================================ records");
//        System.out.println(records.size());
//        records.forEach(record -> System.out.println(record.getId()));
//        System.out.println("================================ end records");

        mvc.perform(get("/api/v1/ledger/"+journalId+"/records")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(6)))
                .andExpect(jsonPath("$[0].id", is(records.get(0).getId().toString())))
                .andExpect(jsonPath("$[1].id", is(records.get(1).getId().toString())))
                .andExpect(jsonPath("$[2].id", is(records.get(2).getId().toString())))
                .andExpect(jsonPath("$[3].id", is(records.get(3).getId().toString())))
                .andExpect(jsonPath("$[4].id", is(records.get(4).getId().toString())))
                .andExpect(jsonPath("$[5].id", is(records.get(5).getId().toString())));
    }

}
