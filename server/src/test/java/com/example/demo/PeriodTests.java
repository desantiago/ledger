package com.example.demo;

import com.example.demo.ledger.LocalDateFactory;
import com.example.demo.ledger.Period;
import com.example.demo.ledger.PeriodRange;
//import org.junit.jupiter.api.Test;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
//import org.powermock.modules.junit4.legacy.PowerMockRunner;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({LocalDateFactory.class})
//@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class PeriodTests {

//    @Mock
//    private TimeFactory timeFactory = new TimeFactory();
//    TimeFactory timeFactory = mock(TimeFactory.class);

//    LocalDate date;

    @Before
    public void setup() {
        mockStatic(LocalDateFactory.class);
        LocalDate date = LocalDate.of(2020, 7, 27);
        when(LocalDateFactory.now()).thenReturn(date);
    }

    @Test
    public void return_currentWeek() {
//        LocalDate date = LocalDate.of(2020, 7, 27);
//        when(LocalDateFactory.now()).thenReturn(date);
//        mockStatic(LocalDateFactory.class);
//        LocalDate date = LocalDate.of(2020, 7, 27);
//        when(LocalDateFactory.now()).thenReturn(date);

        PeriodRange range = Period.CURRENT_WEEK.getRange();
        assertEquals(range.getStartDate(), 1595833200000L);
        assertEquals(range.getEndDate(), 1596437999000L);
    }

    @Test
    public void return_lastWeek() {
        PeriodRange range = Period.LAST_WEEK.getRange();
        assertEquals(range.getStartDate(), 1595142000000L);
        assertEquals(range.getEndDate(), 1595746799000L);
    }

    @Test
    public void return_lastTwoWeeks() {
        PeriodRange range = Period.LAST_TWO_WEEKS.getRange();
        assertEquals(range.getStartDate(), 1595142000000L);
        assertEquals(range.getEndDate(), 1596351599000L);
    }

    @Test
    public void return_currentMonth() {
        PeriodRange range = Period.CURRENT_MONTH.getRange();
        assertEquals(range.getStartDate(), 1593586800000L);
        assertEquals(range.getEndDate(), 1596265199000L);
    }

    @Test
    public void return_LastMonth() {
        PeriodRange range = Period.LAST_MONTH.getRange();
        assertEquals(range.getStartDate(), 1590994800000L);
        assertEquals(range.getEndDate(), 1593586799000L);
    }

    @Test
    public void return_LastTwoMonths() {
        PeriodRange range = Period.LAST_TWO_MONTHS.getRange();
        assertEquals(range.getStartDate(), 1590994800000L);
        assertEquals(range.getEndDate(), 1596265199000L);
    }

    @Test
    public void return_LastThreeMonths() {
        PeriodRange range = Period.LAST_THREE_MONTHS.getRange();
        assertEquals(range.getStartDate(), 1588316400000L);
        assertEquals(range.getEndDate(), 1596265199000L);
    }

    @Test
    public void return_currentQuarter() {
        PeriodRange range = Period.CURRENT_QUARTER.getRange();
        assertEquals(range.getStartDate(), 1593586800000L);
        assertEquals(range.getEndDate(), 1601535599000L);
    }

    @Test
    public void return_lastQuarter() {
        PeriodRange range = Period.LAST_QUARTER.getRange();
        assertEquals(range.getStartDate(), 1585724400000L);
        assertEquals(range.getEndDate(), 1593586799000L);
    }

    @Test
    public void return_currentYear() {
        PeriodRange range = Period.CURRENT_YEAR.getRange();
        assertEquals(range.getStartDate(), 1577865600000L);
        assertEquals(range.getEndDate(), 1609487999000L);
    }

    @Test
    public void return_lastYear() {
        PeriodRange range = Period.LAST_YEAR.getRange();
        assertEquals(range.getStartDate(), 1546329600000L);
        assertEquals(range.getEndDate(), 1577865599000L);
    }

    @Test
    public void return_lastSevenDays() {
        PeriodRange range = Period.LAST_7_DAYS.getRange();
        assertEquals(range.getStartDate(), 1595314800000L);
        assertEquals(range.getEndDate(), 1595919599000L);
    }

    @Test
    public void return_lastFourteenDays() {
        PeriodRange range = Period.LAST_14_DAYS.getRange();
        assertEquals(range.getStartDate(), 1594710000000L);
        assertEquals(range.getEndDate(), 1595919599000L);
    }

    @Test
    public void return_lastThirtyDays() {
        PeriodRange range = Period.LAST_30_DAYS.getRange();
        assertEquals(range.getStartDate(), 1593327600000L);
        assertEquals(range.getEndDate(), 1595919599000L);
    }

}
