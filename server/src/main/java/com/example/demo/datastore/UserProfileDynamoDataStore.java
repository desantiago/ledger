package com.example.demo.datastore;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.document.*;
//import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.model.*;
import com.example.demo.profile.UserProfile;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import imdb.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

class LogEntry {
    private String JobId;
    private String description;
    private long time;


}

@Repository
public class UserProfileDynamoDataStore {

    private final AmazonDynamoDB amazonDynamoDB;
    private final AmazonDynamoDB amazonDynamoDBPersonal;

    private static final List<UserProfile> USER_PROFILES = new ArrayList<>();

    static {
        USER_PROFILES.add(new UserProfile(UUID.fromString("43b64df4-2e45-4942-950e-e321f376349e"), "User 1", "Screen Shot 2019-10-09 at 5.46.29 PM.png-fa69ab66-0e66-4054-9054-802d8ac7661b"));
        USER_PROFILES.add(new UserProfile(UUID.fromString("b9dbd4ea-0d1e-4904-948e-2c578174c0be"), "User 2", "Screen Shot 2019-11-11 at 12.42.45 PM.png-e82ad8a1-76d3-4ed6-9b18-09fdee468d1f"));
    }

    @Autowired
    public UserProfileDynamoDataStore(AmazonDynamoDB dynamodb, AmazonDynamoDB dynamodbPersonal) {
        this.amazonDynamoDB = dynamodb;
        this.amazonDynamoDBPersonal = dynamodbPersonal;
    }

    public List<UserProfile> getUserProfiles() {
        return USER_PROFILES;
    }


    private static final Set<ZeldaProvider> NON_EXPRESSIONS_TYPES = ImmutableSet.of(ZeldaProvider.IMDB);
    private static final String IDENTIFIERS = "identifiers";
    private static final String EXPRESSIONS = "expressions";

    private int totalnum = 0;
    private int numdiffs = 0;
    private int numdiffsnozelda = 0;
    private int numequal = 0;

    public void addUser(UserProfile userProfile) {
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        Table table = dynamoDB.getTable("users");

        Item item = new Item()
                .withPrimaryKey("id", userProfile.getUserProfileId().toString())
                .withString("username", userProfile.getUsername())
                .withString("userProfileImageLink", userProfile.getUserProfileImageLink().get());

        try {
            System.out.println("Adding a new item...");
            PutItemOutcome outcome = table
                    .putItem(item);

            System.out.println("PutItem succeeded:\n" + outcome.getPutItemResult());

        } catch (Exception e) {
            System.err.println("Unable to add item: ");
            System.err.println(e.getMessage());
        }
    }

    public long checkRecordCount() throws InterruptedException, IOException {
//        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

//        Table table = dynamoDB.getTable("users");
//
//        Item item = new Item()
//                .withPrimaryKey("id", userProfile.getUserProfileId().toString())
//                .withString("username", userProfile.getUsername())
//                .withString("userProfileImageLink", userProfile.getUserProfileImageLink().get());
//
//        try {
//            System.out.println("Adding a new item...");
//            PutItemOutcome outcome = table
//                    .putItem(item);
//
//            System.out.println("PutItem succeeded:\n" + outcome.getPutItemResult());
//
//        } catch (Exception e) {
//            System.err.println("Unable to add item: ");
//            System.err.println(e.getMessage());
//        }

//        final String week = "whatever";
//        final Integer myPoint = 1337;
//        Condition weekCondition = new Condition()
//                .withComparisonOperator(ComparisonOperator.EQ)
//                .withAttributeValueList(new AttributeValue().withS(week));
//        Condition myPointCondition = new Condition()
//                .withComparisonOperator(ComparisonOperator.GE)
//                .withAttributeValueList(new AttributeValue().withN(myPoint.toString()));
//
//        Map<String, Condition> keyConditions = new HashMap<>();
//        keyConditions.put("week", weekCondition);
//        keyConditions.put("point", myPointCondition);
//
//
//        QueryRequest request = new QueryRequest("beta-metacritic-ingest-table");
////        request.setIndexName("week-point-index");
//        request.setSelect(Select.COUNT);
//        request.setKeyConditions(keyConditions);
//
////        dynamoDB.getTable("ss").
//        QueryResult result = amazonDynamoDB.query(request);
//        Integer count = result.getCount();

        final Object[] FILE_HEADER = {"id", "userId", "createdDate"};
        CSVFormat csvFormat = CSVFormat.DEFAULT.withRecordSeparator("\n");
        CSVPrinter csvPrinter = new CSVPrinter(new FileWriter("prod.csv"), csvFormat);
        csvPrinter.printRecord(FILE_HEADER);

//        List<String> titleIds = new ArrayList<>();
//        prod-metacritic-ingest-table


        ScanRequest scanRequest = new ScanRequest()
                .withTableName("prod-metacritic-ingest-table")
                .withConsistentRead(false)
                .withLimit(100)
                .withAttributesToGet("titleId");
        int counter = 0;
        do {
            ScanResult result = amazonDynamoDB.scan(scanRequest);
            Map<String, AttributeValue> lastEvaluatedKey = result.getLastEvaluatedKey();
            for (Map<String, AttributeValue> item : result.getItems()) {
                List record = new ArrayList();
//                System.out.println(item);
                AttributeValue titleId = item.getOrDefault("titleId", new AttributeValue());
//                AttributeValue idAttribute = item.getOrDefault("id", new AttributeValue());
//                AttributeValue createdDateAttribute = item.getOrDefault("createdDate", new AttributeValue());
//                record.add(idAttribute.getS());
//                record.add(userIdAttribute.getS());
                record.add(titleId.getS());
                csvPrinter.printRecord(record);
                TimeUnit.MILLISECONDS.sleep(10);
            }
            scanRequest.setExclusiveStartKey(lastEvaluatedKey);
        } while (scanRequest.getExclusiveStartKey() != null);
        csvPrinter.flush();
        csvPrinter.close();
        System.out.println("CSV file generated successfully.");

        DescribeTableResult description = amazonDynamoDB.describeTable("prod-metacritic-ingest-table");
        return description.getTable().getItemCount();
    }

    public void checkAvisTable() throws InterruptedException, IOException {
//        ScanRequest scanRequest = new ScanRequest()
//                .withTableName("avis")
//                .withConsistentRead(false)
//                .withLimit(100)
//                .withAttributesToGet("videoID", "mappings");
//
//        int counter = 0;
//        long startTime = System.currentTimeMillis();
//        do {
//            ScanResult result = amazonDynamoDB.scan(scanRequest);
//            Map<String, AttributeValue> lastEvaluatedKey = result.getLastEvaluatedKey();
//
//            List<Map<String, AttributeValue>> tableKeys = new ArrayList<>();
//
//            for (Map<String, AttributeValue> item : result.getItems()) {
//                AttributeValue videoId = item.getOrDefault("videoID", new AttributeValue());
//                AttributeValue mappings = item.getOrDefault("mappings", new AttributeValue());
//
////                System.out.println("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
////                System.out.println(videoId.getS());
////                System.out.println(mappings.getS());
//
//                tableKeys.add(Collections.singletonMap("videoID", new AttributeValue().withS(videoId.getS())));
//            }
//
//            System.out.println("Always ");
//            String tableName = "ZeldaMappings";
//
//            Map<String, KeysAndAttributes> requestItems = Collections.singletonMap(tableName, new KeysAndAttributes().withConsistentRead(true).withKeys(tableKeys));
//            BatchGetItemResult result2 = amazonDynamoDB.batchGetItem(new BatchGetItemRequest(requestItems));
//
//            List<Map<String, AttributeValue>> results = result2.getResponses().get(tableName);
//            for (Map<String, AttributeValue> item : results) {
//                final String id = item.get("videoID").getS();
//                final String m = item.get("mappings").getS();
//
////                System.out.println("/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/");
////                System.out.println(id);
////                System.out.println(m);
//    //            final T baseObject = createObject(item);
//    //            output.put(id, baseObject);
//    //            queriedIds.remove(id);
//            }
//
//            scanRequest.setExclusiveStartKey(lastEvaluatedKey);
//            counter += 100;
//
//            System.out.println(counter);
//            if (counter >= 20000) {
//                break;
//            }
//        } while (scanRequest.getExclusiveStartKey() != null);
//
//        long finishTime = System.currentTimeMillis() - startTime;
//        System.out.println(finishTime);
//        System.out.println(finishTime / 1000);


//        // total number of sample items
//        int scanItemCount = 20000;
//
//        // number of items each scan request should return
//        int scanItemLimit = 1000;
//
//        // number of logical segments for parallel scan
//        int parallelScanThreads = 10;
//
//        // table that will be used for scanning
//        String parallelScanTestTableName = "ZeldaMappings";
//
////        DynamoDB dynamoDB = new DynamoDB(client);
//        long startTime = System.currentTimeMillis();
//        parallelScan(parallelScanTestTableName, scanItemLimit, parallelScanThreads);
//        long finishTime = System.currentTimeMillis() - startTime;
//        System.out.println(finishTime);
//        System.out.println(finishTime / 1000);


//        final Object[] FILE_HEADER = {"id"};
//        CSVFormat csvFormat = CSVFormat.DEFAULT.withRecordSeparator("\n");
//        CSVPrinter csvPrinter = new CSVPrinter(new FileWriter("ZeldaMappings.csv"), csvFormat);
//        csvPrinter.printRecord(FILE_HEADER);
//
////        List<String> titleIds = new ArrayList<>();
////        prod-metacritic-ingest-table
//
//
//        ScanRequest scanRequest = new ScanRequest()
//                .withTableName("ZeldaMappings")
//                .withConsistentRead(false)
//                .withLimit(1000)
//                .withAttributesToGet("titleId");
//        int counter = 0;
//        do {
//            ScanResult result = amazonDynamoDB.scan(scanRequest);
//            Map<String, AttributeValue> lastEvaluatedKey = result.getLastEvaluatedKey();
//            for (Map<String, AttributeValue> item : result.getItems()) {
//                List record = new ArrayList();
////                System.out.println(item);
//                AttributeValue titleId = item.getOrDefault("videoID", new AttributeValue());
////                AttributeValue idAttribute = item.getOrDefault("id", new AttributeValue());
////                AttributeValue createdDateAttribute = item.getOrDefault("createdDate", new AttributeValue());
////                record.add(idAttribute.getS());
////                record.add(userIdAttribute.getS());
//                record.add(titleId.getS());
//                csvPrinter.printRecord(record);
//                TimeUnit.MILLISECONDS.sleep(10);
//            }
//            scanRequest.setExclusiveStartKey(lastEvaluatedKey);
//        } while (scanRequest.getExclusiveStartKey() != null);
//        csvPrinter.flush();
//        csvPrinter.close();
//        System.out.println("CSV file generated successfully.");

//        DescribeTableResult description = amazonDynamoDB.describeTable("ZeldaMappings");
//        System.out.println(description.getTable().getItemCount());

//        getOneSegment();
//        getWithDynamoDBMapper();


//        // number of items each scan request should return
//        int scanItemLimit = 100;
//
//        // number of logical segments for parallel scan
//        int parallelScanThreads = 140;
//
//        // table that will be used for scanning
//        String parallelScanTestTableName = "avis";
//
////        DynamoDB dynamoDB = new DynamoDB(client);
//        long startTime = System.currentTimeMillis();
//        parallelScan(parallelScanTestTableName, scanItemLimit, parallelScanThreads);
//        long finishTime = System.currentTimeMillis() - startTime;
//        System.out.println(finishTime);
//        System.out.println(finishTime / 1000);

//        getOneSegment(0,140);
////        testZeldaClient();
//
//        System.out.println("Total imdb records: "+totalnum);
//        System.out.println("Num Diffs : "+numdiffs);
//        System.out.println("Num Equal : "+numequal);
//        System.out.println("Num Diffs No Zelda : "+numdiffsnozelda);

//        copy();

        scanSegment();


    }



    private void parallelScan(String tableName, int itemLimit, int numberOfThreads) {
        System.out.println(
                "Scanning " + tableName + " using " + numberOfThreads + " threads " + itemLimit + " items at a time");
        ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);

        // Divide DynamoDB table into logical segments
        // Create one task for scanning each segment
        // Each thread will be scanning one segment
        int totalSegments = numberOfThreads;
        for (int segment = 5; segment < 6; segment++) {
            // Runnable task that will only scan one segment
            ScanSegmentTask2 task = new ScanSegmentTask2(tableName, itemLimit, totalSegments, segment);

            // Execute the task
            executor.execute(task);
        }

        shutDownExecutorService(executor);
    }

    // Runnable task for scanning a single segment of a DynamoDB table
    private class ScanSegmentTask implements Runnable {

        // DynamoDB table to scan
        private String tableName;

        // number of items each scan request should return
        private int itemLimit;

        // Total number of segments
        // Equals to total number of threads scanning the table in parallel
        private int totalSegments;

        // Segment that will be scanned with by this task
        private int segment;

        DynamoDB dynamoDB;

        public ScanSegmentTask(String tableName, int itemLimit, int totalSegments, int segment) {
            this.tableName = tableName;
            this.itemLimit = itemLimit;
            this.totalSegments = totalSegments;
            this.segment = segment;

            dynamoDB = new DynamoDB(amazonDynamoDB);
        }

        @Override
        public void run() {

            final Object[] FILE_HEADER = {"id"};
            CSVFormat csvFormat = CSVFormat.DEFAULT.withRecordSeparator("\n");
            CSVPrinter csvPrinter = null;
            try {
                csvPrinter = new CSVPrinter(new FileWriter(String.format("segment%d.csv", segment)), csvFormat);
                csvPrinter.printRecord(FILE_HEADER);
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("Scanning " + tableName + " segment " + segment + " out of " + totalSegments
                    + " segments " + itemLimit + " items at a time...");
            int totalScannedItemCount = 0;

            Table table = dynamoDB.getTable(tableName);

            try {
                ScanSpec spec;
                String lastId = "";
                int count = 0;
                do {

//                    if (itemLimit > 0) {
//                        spec = new ScanSpec().withMaxResultSize(itemLimit)
////                                .withTotalSegments(totalSegments)
//                                .withSegment(segment);
//                    } else {
//                        spec = new ScanSpec().withTotalSegments(totalSegments)
//                                .withSegment(segment);
//                    }

                    spec = new ScanSpec().withTotalSegments(totalSegments)
                            .withSegment(segment);

                    if (itemLimit > 0) {
                        spec.withMaxResultSize(itemLimit);
                    }

                    if (!lastId.equals("")) {
                        spec.withExclusiveStartKey(new PrimaryKey("videoID", lastId));
                    }

                    ItemCollection<ScanOutcome> items = table.scan(spec);

                    Iterator<Item> iterator = items.iterator();

                    Item currentItem = null;
                    lastId = "";
                    while (iterator.hasNext()) {
                        totalScannedItemCount++;
                        currentItem = iterator.next();
                        lastId = currentItem.getString("videoID");
//                        System.out.println(lastId);

                        List record = new ArrayList();
                        record.add(lastId);
                        csvPrinter.printRecord(record);
                    }

                    count += 1;

                    System.out.println("Termino *"+lastId+"*");
                    if (lastId.equals("")) {
                        System.out.println("Finished the lastId is empty");
                        break;
                    }

                } while (!lastId.equals(""));

//                System.out.println("Getting second batch");
//
//                if (itemLimit > 0) {
//                    spec = new ScanSpec().withMaxResultSize(itemLimit).withTotalSegments(totalSegments)
//                        .withExclusiveStartKey(new PrimaryKey("videoID", lastId))
//                            .withSegment(segment);
//                }
//                else {
//                    spec = new ScanSpec().withTotalSegments(totalSegments)
//                            .withExclusiveStartKey(new PrimaryKey("videoID", lastId))
//                            .withSegment(segment);
//                }
//
//                items = table.scan(spec);
//                iterator = items.iterator();
//
//                currentItem = null;
//                lastId = "";
//                while (iterator.hasNext()) {
//                    totalScannedItemCount++;
//                    currentItem = iterator.next();
//                    lastId = currentItem.getString("videoID");
//                    System.out.println(lastId);
//                }

                csvPrinter.flush();
                csvPrinter.close();
                System.out.println("CSV file generated successfully.");

            }
            catch (Exception e) {
//                System.out.println(e.getMessage());
            }
            finally {

                System.out.println("Scanned " + totalScannedItemCount + " items from segment " + segment + " out of "
                        + totalSegments + " of " + tableName);
            }
        }
    }

    private static void shutDownExecutorService(ExecutorService executor) {
        executor.shutdown();
        try {
            if (!executor.awaitTermination(10, TimeUnit.SECONDS)) {
                executor.shutdownNow();
            }
        }
        catch (InterruptedException e) {
            executor.shutdownNow();

            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }

    public void getOneSegment(int segment, int totalSegments) throws IOException {

        String tableName = "avis";
//        int segment = 5;
//        int totalSegments = 140;
        int itemLimit = 100;
        long startTime  = System.currentTimeMillis();

//        final Object[] FILE_HEADER = {"id"};
//        CSVFormat csvFormat = CSVFormat.DEFAULT.withRecordSeparator("\n");
//        CSVPrinter csvPrinter = null;
//        csvPrinter = new CSVPrinter(new FileWriter(String.format("onlysegment%d.csv", segment)), csvFormat);
//        csvPrinter.printRecord(FILE_HEADER);

//        for (int i =0; i < 20; i++) {
//            String lastId = scanOneSegment(tableName, itemLimit, totalSegments, segment, "", csvPrinter);
//            System.out.println("Finished");
//            System.out.println(lastId);
//
//            lastId = scanOneSegment(tableName, itemLimit, totalSegments, segment, lastId, csvPrinter);
//            System.out.println("Finished");
//            System.out.println(lastId);
//        }

        String lastId = scanOneSegment(tableName, itemLimit, totalSegments, segment, "");
//        lastId = scanOneSegment(tableName, itemLimit, totalSegments, segment, lastId);
//        lastId = scanOneSegment(tableName, itemLimit, totalSegments, segment, lastId);
//        lastId = scanOneSegment(tableName, itemLimit, totalSegments, segment, lastId);
//        lastId = scanOneSegment(tableName, itemLimit, totalSegments, segment, lastId);

//        String lastId = "";
//        int counter = 0;
//        do {
//            lastId = scanOneSegment(tableName, itemLimit, totalSegments, segment, lastId);
//            counter += 1;
//            System.out.println(lastId);
//            System.out.println(counter);
//        } while (!lastId.equals(""));

//        csvPrinter.flush();
//        csvPrinter.close();
//        System.out.println("CSV file generated successfully.");

        long finishTime = System.currentTimeMillis() - startTime;
        System.out.println(finishTime);
        System.out.println(finishTime / 1000);

    }

    public String scanOneSegment(String tableName, int itemLimit, int totalSegments, int segment, String lastId) {
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        System.out.println("Scanning " + tableName + " segment " + segment + " out of " + totalSegments
                + " segments " + itemLimit + " items at a time...");
        int totalScannedItemCount = 0;

        Table table = dynamoDB.getTable(tableName);

        ObjectMapper mapper = new ObjectMapper();
        TypeReference<HashMap<String, String>> typeRef
                = new TypeReference<HashMap<String, String>>() { };

        try {
            ScanSpec spec;
            int count = 0;

            spec = new ScanSpec().withTotalSegments(totalSegments)
//                    .withFilterExpression("contains(mappings, :imdb)")
//                    .withValueMap(ImmutableMap.of(":imdb", "imdb"))
                    .withSegment(segment);

            if (itemLimit > 0) {
                spec.withMaxResultSize(itemLimit);
            }

            if (!lastId.equals("")) {
                System.out.println("Empezando desde el previous key");
                spec.withExclusiveStartKey(new PrimaryKey("videoID", lastId));
            }

            ItemCollection<ScanOutcome> items = table.scan(spec);

            Iterator<Item> iterator = items.iterator();

            List<Map<String, AttributeValue>> tableKeys = new ArrayList<>();
            Item currentItem = null;
            String mappings;
            lastId = "";
            Map<String, Set<ZeldaIdentifier>> avisIdentifiers = new HashMap<>();
            Map<String, Set<ZeldaIdentifier>> zeldaMappingsIdentifiers = new HashMap<>();
            List<String> ids = new ArrayList<>();

            while (iterator.hasNext()) {
                totalScannedItemCount++;
                currentItem = iterator.next();
                lastId = currentItem.getString("videoID");
                mappings = currentItem.getString("mappings");

                if (mappings.contains("imdb")) {

//                    System.out.println("---------------------------------------------------------------------");
//                    System.out.println("Mappings");
//                    System.out.println(mappings);
                    totalnum+=1;

                    JsonNode itemRoot = mapper.readTree(mappings);
//                    System.out.println("Matches");
//                    getProviderMatches(ZeldaProvider.values()[0], itemRoot).forEach(z -> System.out.println(z));
//                    System.out.println("Matches");
//                    getProviderMatches(ZeldaProvider.values()[1], itemRoot).forEach(z -> System.out.println(z));

                    ImmutableSet<ZeldaProvider> providers = ImmutableSet.copyOf(ZeldaProvider.values());
                    Set<ZeldaIdentifier> matchedIdentifiers = providers.stream()
                            .flatMap(provider -> ZeldaParser.getProviderMatches(provider, itemRoot))
                            .collect(Collectors.toSet());

//                    matchedIdentifiers.forEach(ident -> System.out.println(ident));

//                    List record = new ArrayList();
//                    record.add(lastId);
//                    csvPrinter.printRecord(record);

                    avisIdentifiers.put(lastId, matchedIdentifiers);

                    tableKeys.add(Collections.singletonMap("videoID", new AttributeValue().withS(lastId)));
//                    System.out.println("---------------------------------------------------------------------");
                    ids.add(lastId);
                }
            }

            if (tableKeys.size() > 0) {
//                Map<String, KeysAndAttributes> requestItems = Collections.singletonMap("ZeldaMappings", new KeysAndAttributes().withConsistentRead(true).withKeys(tableKeys));
//                BatchGetItemResult result2 = amazonDynamoDB.batchGetItem(new BatchGetItemRequest(requestItems));
//
//
//                List<Map<String, AttributeValue>> results = result2.getResponses().get("ZeldaMappings");
//                for (Map<String, AttributeValue> item : results) {
//                    final String id = item.get("videoID").getS();
//                    final String m = item.get("mappings").getS();
//
//                    JsonNode itemRoot = mapper.readTree(m);
//
//                    ImmutableSet<ZeldaProvider> providers = ImmutableSet.copyOf(ZeldaProvider.values());
//                    Set<ZeldaIdentifier> matchedIdentifiers = providers.stream()
//                            .flatMap(provider -> ZeldaParser.getProviderMatches(provider, itemRoot))
//                            .collect(Collectors.toSet());
//
//                    zeldaMappingsIdentifiers.put(id, matchedIdentifiers);
//                }
/*
Total imdb records: 52
Num Diffs : 14
Num Equal : 0
Num Diffs No Zelda : 38
 */

                TableKeysAndAttributes tableKeyAndAttributes = new TableKeysAndAttributes("ZeldaMappings");
                ids.forEach(id -> tableKeyAndAttributes.addHashOnlyPrimaryKeys("videoID", id));

                BatchGetItemOutcome outcome = dynamoDB.batchGetItem(tableKeyAndAttributes);
                Map<String, KeysAndAttributes> unprocessed = null;
                List<Item> listItems = new ArrayList<Item>();
                do {
                    listItems.addAll(outcome.getTableItems().get("ZeldaMappings"));

                    unprocessed = outcome.getUnprocessedKeys();
                    if (!unprocessed.isEmpty()) {
                        outcome = dynamoDB.batchGetItemUnprocessed(unprocessed);
                    }

                } while (!unprocessed.isEmpty());

                for (Item item : listItems) {
                    final String id = item.getString("videoID");
                    final String m = item.getString("mappings");

                    JsonNode itemRoot = mapper.readTree(m);

                    ImmutableSet<ZeldaProvider> providers = ImmutableSet.copyOf(ZeldaProvider.values());
                    Set<ZeldaIdentifier> matchedIdentifiers = providers.stream()
                            .flatMap(provider -> ZeldaParser.getProviderMatches(provider, itemRoot))
                            .collect(Collectors.toSet());

                    zeldaMappingsIdentifiers.put(id, matchedIdentifiers);
                }
            }

//            System.out.println("Differences");
//            System.out.println("avis :"+avisIdentifiers.size());
//            System.out.println("zelmda :"+zeldaMappingsIdentifiers.size());

            avisIdentifiers.entrySet().forEach(e -> {
                String avisKey = e.getKey();
//                System.out.println("Getting differences for "+avisKey);
                Set<ZeldaIdentifier> avisId = e.getValue();
                Set<ZeldaIdentifier> zeldaId = zeldaMappingsIdentifiers.get(avisKey);

                if (!Objects.isNull(zeldaId)) {
                    Sets.SetView<ZeldaIdentifier> diffs = Sets.symmetricDifference(avisId, zeldaId);

//                    diffs.forEach(d -> System.out.println(d));
                    if (diffs.isEmpty()) {
//                        System.out.println("No differences");
                        numequal+=1;
                    } else {
//                        System.out.println("There are differences");
//                        System.out.println("avis:");
//                        avisId.forEach(a -> System.out.println(a));
//                        System.out.println("zeldaMappings:");
//                        zeldaId.forEach(z -> System.out.println(z));
//                        System.out.println("differences:");
//                        diffs.forEach(d -> System.out.println(d));
                        numdiffs+=1;

                        Optional<Sets.SetView<ZeldaIdentifier>> differences = Optional.of(diffs);

                        String strDifferences = differences.map(difference ->
                                difference.stream()
                                        .map(zeldaIdentifier -> zeldaIdentifier.getIdentifier())
                                        .collect(Collectors.joining(","))
                        ).orElse("");

                        System.out.println("Diffes: " + strDifferences);

                    }
                }
                else {
//                    System.out.println("There are no data in ZeldaMappings");
                    numdiffsnozelda+=1;
                }
            });

            return lastId;
        }
        catch (Exception e) {
            e.printStackTrace();
//            System.out.println(e.getMessage());
            return "";
        }
        finally {

            System.out.println("Scanned " + totalScannedItemCount + " items from segment " + segment + " out of "
                    + totalSegments + " of " + tableName);
        }
    }

//    public java.util.stream.Stream<ZeldaIdentifier> getProviderMatches(ZeldaProvider zeldaProvider, JsonNode itemRoot) {
//        return Optional.of(itemRoot)
//                // Go to the location of the identifiers we're looking for
//                .map(entry -> NON_EXPRESSIONS_TYPES.contains(zeldaProvider) ? entry : entry.get(EXPRESSIONS))
//                .map(this::arrayNodeToStream)
//                .orElse(java.util.stream.Stream.empty())
//                // Filter to only the identifiers we want
//                .flatMap(nodeRoot -> Optional.of(nodeRoot)
//                        .map(entry -> entry.get(IDENTIFIERS))
//                        .map(entry -> entry.get(zeldaProvider.getZeldaPrefix()))
//                        .map(this::arrayNodeToStream)
//                        .orElse(java.util.stream.Stream.empty()))
//                // Convert them into ZeldaIdentifiers
//                .flatMap(providerNode -> Stream.of(providerNode)
//                        .filter(JsonNode::isTextual)
//                        .map(JsonNode::textValue)
//                        .map(identifierValue -> ZeldaIdentifier.builder()
//                                .provider(zeldaProvider)
//                                .value(identifierValue).build()));
//    }
//
//    private  Stream<JsonNode> arrayNodeToStream(JsonNode node) {
//        return Optional.of(node)
//                .filter(JsonNode::isArray)
//                .map(arrayNode -> StreamSupport.stream(arrayNode.spliterator(), false))
//                .orElse(Stream.of(node));
//    }

    // Runnable task for scanning a single segment of a DynamoDB table
    private class ScanSegmentTask2 implements Runnable {

        // DynamoDB table to scan
        private String tableName;

        // number of items each scan request should return
        private int itemLimit;

        // Total number of segments
        // Equals to total number of threads scanning the table in parallel
        private int totalSegments;

        // Segment that will be scanned with by this task
        private int segment;

        DynamoDB dynamoDB;

        public ScanSegmentTask2(String tableName, int itemLimit, int totalSegments, int segment) {
            this.tableName = tableName;
            this.itemLimit = itemLimit;
            this.totalSegments = totalSegments;
            this.segment = segment;

            dynamoDB = new DynamoDB(amazonDynamoDB);
        }

        @Override
        public void run() {

//            String tableName = "avis";
//            int segment = 5;
//            int totalSegments = 140;
//            int itemLimit = 100;
//            long startTime = System.currentTimeMillis();

            final Object[] FILE_HEADER = {"id"};
            CSVFormat csvFormat = CSVFormat.DEFAULT.withRecordSeparator("\n");
            CSVPrinter csvPrinter = null;
            try {
                csvPrinter = new CSVPrinter(new FileWriter(String.format("avis%d.csv", segment)), csvFormat);
                csvPrinter.printRecord(FILE_HEADER);
            } catch (IOException e) {
                e.printStackTrace();
            }

//        for (int i =0; i < 20; i++) {
//            String lastId = scanOneSegment(tableName, itemLimit, totalSegments, segment, "", csvPrinter);
//            System.out.println("Finished");
//            System.out.println(lastId);
//
//            lastId = scanOneSegment(tableName, itemLimit, totalSegments, segment, lastId, csvPrinter);
//            System.out.println("Finished");
//            System.out.println(lastId);
//        }

            String lastId = "";
            int counter = 0;
            do {
                lastId = scanOneSegment(tableName, itemLimit, totalSegments, segment, lastId);
                counter += 1;
//                System.out.println(lastId);
//                System.out.println(counter);
            } while (!lastId.equals(""));

            try {
                csvPrinter.flush();
                csvPrinter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

//            System.out.println("CSV file generated successfully.");

//            long finishTime = System.currentTimeMillis() - startTime;
//            System.out.println(finishTime);
//            System.out.println(finishTime / 1000);
        }
    }


    public void getWithDynamoDBMapper() {
        int numberOfThreads = 4;

        DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDB);

        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression().withSegment(1).withTotalSegments(10);

        List<HashMap> scanResult = mapper.parallelScan(HashMap.class, scanExpression, numberOfThreads);
    }

    public void testZeldaClient() {
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        ZeldaClient zeldaClient = ZeldaClient.builder(dynamoDB)
                .stage(ZeldaStage.BETA)
                .callerId("test")
                .build();

        ZeldaIdentifier IMDB_IDENTIFIER = ZeldaIdentifier.create("imdb:tt5119260");

        Set<ZeldaIdentifier> test = zeldaClient.getMatches(IMDB_IDENTIFIER);

//        System.out.println("Test -------");
        test.forEach(t -> System.out.println(t));
    }

    public void copy() {
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);
        DynamoDB dynamoDBPersonal = new DynamoDB(amazonDynamoDBPersonal);

        Table tableProd = dynamoDB.getTable("ZeldaMappings");
        Table tablePersonal = dynamoDBPersonal.getTable("ZeldaMappings");

        ScanSpec spec = new ScanSpec().
            withMaxResultSize(10000);

        ItemCollection<ScanOutcome> items = tableProd.scan(spec);

        Iterator<Item> iterator = items.iterator();

        System.out.println("Avis prod ---------------------------------");
        Item currentItem = null;
        while (iterator.hasNext()) {
            currentItem = iterator.next();
            String videoId = currentItem.getString("videoID");
            System.out.println(videoId);

            PutItemOutcome outcome = tablePersonal
                    .putItem(currentItem);

            System.out.println("PutItem succeeded:\n" + outcome.getPutItemResult());
        }

        // Original personal test rows
//        aiv_de:B07BTXL45D
//        aiv_de:B077TBLXBS
//        aiv_de:B07KV7D8GM
//        aiv_de:B083Y146KM
//        aiv_row_eu:B01N8VTPZN

        System.out.println("Avis personal ---------------------------------");
        ItemCollection<ScanOutcome> itemsP = tablePersonal.scan(spec);

        Iterator<Item> iteratorP = itemsP.iterator();
        Item currentItemP = null;
        while (iteratorP.hasNext()) {
            currentItemP = iteratorP.next();
            String videoId = currentItemP.getString("videoID");
            System.out.println(videoId);
        }
    }

    public void scanSegment() {
        int totalSegments = 10;
        int segment = 1;
        int itemLimit = 100;
        String lastId = "";

        int totalScanned = 0;

        do {

            ScanSpec spec = new ScanSpec()
                    .withSegment(segment)
                    .withTotalSegments(totalSegments);

            if (itemLimit > 0) {
                spec.withMaxResultSize(itemLimit);
            }

            if (!lastId.equals("")) {
                spec.withExclusiveStartKey(new PrimaryKey("videoID", lastId));
            }

            DynamoDB dynamoDBPersonal = new DynamoDB(amazonDynamoDBPersonal);
            Table tablePersonal = dynamoDBPersonal.getTable("ZeldaMappings");

            ItemCollection<ScanOutcome> items = tablePersonal.scan(spec);
            Iterator<Item> iterator = items.iterator();

            System.out.println("-----------------");
            Item currentItem = null;
            lastId = "";

            while (iterator.hasNext()) {
    //            totalScannedItemCount++;
                currentItem = iterator.next();
                lastId = currentItem.getString("videoID");
                System.out.println(totalScanned+"-"+lastId);

                totalScanned++;
    //            List record = new ArrayList();
    //            record.add(lastId);
    //            csvPrinter.printRecord(record);
            }

        } while (!lastId.equals(""));


//        ScanSpec spec = new ScanSpec().withMaxResultSize(itemLimit).withTotalSegments(totalSegments)
//                .withSegment(segment);
//
//        ItemCollection<ScanOutcome> items = table.scan(spec);
//        Iterator<Item> iterator = items.iterator();
//
//        Item currentItem = null;
//        while (iterator.hasNext()) {
//            totalScannedItemCount++;
//            currentItem = iterator.next();
//            System.out.println(currentItem.toString());
//        }
    }
}
