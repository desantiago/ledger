package com.example.demo.datastore;

import com.example.demo.ledger.*;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class FakeJournalDataStore {
    private Journal journal;
    List<Record> records;

    public FakeJournalDataStore(UUID id, String description, String status, UUID userId) {
        Objects.requireNonNull(description);
        Objects.requireNonNull(status);
        Objects.requireNonNull(userId);

        //journalData = new JournalData(id, Instant.now(), description, status, userId);
        journal = new Journal.Builder(id)
                .withDescription(description)
                .withStatus(status)
                .withUserId(userId)
                .build();
        records = new ArrayList<>();
    }

    public UUID addRecordtoJournal(RecordWriter recordWriter) {
        UUID id = getValidId();
        Record record = new Record.Builder()
                .buildFromRecordWriter(recordWriter)
                .withId(id)
                .withBalance(newBalance(recordWriter.getAmountSum()))
                .withTimestamp(String.valueOf(Instant.now().toEpochMilli()))
                .build();
        records.add(record);
        return id;
    }

    private UUID getValidId() {
        UUID id = UUID.randomUUID();
        if (idExists(id)) {
            return getValidId();
        }
        else {
            return id;
        }
    }

    private boolean idExists(UUID id) {
        return records.stream().anyMatch(record -> record.getId().equals(id));
    }

    private BigDecimal currentBalance() {
        return records.stream()
                .map(record -> record.getAmountSum())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal newBalance( BigDecimal amount) {
        BigDecimal balance = currentBalance().add(amount);
        return balance;
    }

    public List<Record> getRecords() {
        return records;
    }

    public List<Record> getRecordsByDateRange(long startDate, long endDate) {
        return records.stream()
                .filter(record -> Long.parseLong(record.getUtcDate()) >= startDate && Long.parseLong(record.getUtcDate()) <= endDate)
                .collect(Collectors.toList());
    }

    public Map<RecordType, List<Record>> getRecordsGroupedType() {
        Map<RecordType, List<Record>> collect = records.stream().collect(groupingBy(Record::getType));
        return collect;
    }

    public Map<String, List<Record>> getRecordsGroupedCategory() {
        Map<String, List<Record>> collect = records.stream().collect(groupingBy(Record::getCategory));
        return collect;
    }

    public Map<String, BigDecimal> getSumByCategory() {
        Map<String, BigDecimal> collect = records.stream()
                .collect(groupingBy(Record::getCategory,
                        Collectors.mapping(Record::getAmountSum, Collectors.reducing(BigDecimal.ZERO, BigDecimal::add))));
        return collect;
    }

    public Map<String, BigDecimal> getSumByMonth() {
        Map<String, BigDecimal> collect = records.stream()
                .collect(groupingBy(Record::getMonthYear,
                        Collectors.mapping(Record::getAmountSum, Collectors.reducing(BigDecimal.ZERO, BigDecimal::add))));
        return collect;
    }

    public Map<String, BigDecimal> getSumByMonthByPeriod(Period period) {
        Map<String, BigDecimal> collect = records.stream()
                .filter(record ->
                        Long.parseLong(record.getUtcDate()) >= period.getRange().getStartDate() &&
                                Long.parseLong(record.getUtcDate()) <= period.getRange().getEndDate())
                .collect(groupingBy(Record::getMonthYear,
                        Collectors.mapping(Record::getAmountSum, Collectors.reducing(BigDecimal.ZERO, BigDecimal::add))));
        return collect;
    }

    public Journal getJournaldata() {
        return journal;
    }
}
