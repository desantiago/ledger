package com.example.demo.datastore;

import com.example.demo.profile.UserProfile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class FakeUserProfileDataStore {
    private static final List<UserProfile> USER_PROFILES = new ArrayList<>();

    static {
        USER_PROFILES.add(new UserProfile(UUID.randomUUID(), "User 1", "image1"));
        USER_PROFILES.add(new UserProfile(UUID.randomUUID(), "User 2", "image2"));
    }

    public List<UserProfile> getUserProfiles() {
        return USER_PROFILES;
    }
}
