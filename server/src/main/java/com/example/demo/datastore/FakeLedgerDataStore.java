package com.example.demo.datastore;

import com.example.demo.ledger.*;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class FakeLedgerDataStore {
    public final static UUID USER_ID = UUID.randomUUID();
    private Map<UUID, FakeJournalDataStore> journals;

    public FakeLedgerDataStore() {
        System.out.println("Initializing dataStore ==============================");
        this.journals = new HashMap<>();

        UUID id = addJournal("journal", "open", USER_ID);
        Optional<FakeJournalDataStore> optionalJournal = getJournal(id);
        FakeJournalDataStore journal = optionalJournal.get();

//        LocalDateTime.parse(                   // Parse as an indeterminate `LocalDate`, devoid of time zone or offset-from-UTC. NOT a moment, NOT a point on the timeline.
//                "04:30 PM, Sat 5/12/2018" ,        // This input uses a poor choice of format. Whenever possible, use standard ISO 8601 formats when exchanging date-time values as text. Conveniently, the java.time classes use the standard formats by default when parsing/generating strings.
//                DateTimeFormatter.ofPattern( "hh:mm a, EEE M/d/uuuu" , Locale.US )  // Use single-character `M` & `d` when the number lacks a leading padded zero for single-digit values.
//        )                                      // Returns a `LocalDateTime` object.
//                .atZone(                               // Apply a zone to that unzoned `LocalDateTime`, giving it meaning, determining a point on the timeline.
//                        ZoneId.of( "America/Toronto" )     // Always specify a proper time zone with `Contintent/Region` format, never a 3-4 letter pseudo-zone such as `PST`, `CST`, or `IST`.
//                )                                      // Returns a `ZonedDateTime`. `toString` → 2018-05-12T16:30-04:00[America/Toronto].
//                .toInstant()                           // Extract a `Instant` object, always in UTC by definition.
//                .toString();

        RecordWriter record = new RecordWriter.Builder()
                .withDescription("test")
                .withUtcDate(String.valueOf(Instant.now().toEpochMilli()))
                .withLocalDate(String.valueOf(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()))
                .withCategory("cat")
                .withSubCategory("subcat")
                .withType(RecordType.DEBIT)
                .withAmount(BigDecimal.valueOf(100))
                .withUserId(USER_ID)
                .build();

        journal.addRecordtoJournal(record);

        RecordWriter record2 = new RecordWriter.Builder()
                .withDescription("test")
                .withUtcDate(String.valueOf(Instant.now().toEpochMilli()))
                .withLocalDate(String.valueOf(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()))
                .withCategory("cat")
                .withSubCategory("subcat")
                .withType(RecordType.DEBIT)
                .withAmount(BigDecimal.valueOf(150))
                .withUserId(USER_ID)
                .build();

        journal.addRecordtoJournal(record2);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        Long milis20200610 = LocalDateTime.parse("2020-06-10 12:00:00", formatter)
                .atZone(ZoneId.systemDefault())
                .toInstant()
                .toEpochMilli();

        //1590998400
        RecordWriter record20200610 = new RecordWriter.Builder()
                .withDescription("record for 20200610")
                .withUtcDate(String.valueOf(milis20200610))
                .withLocalDate(String.valueOf(milis20200610))
                .withCategory("july")
                .withSubCategory("subcat")
                .withType(RecordType.DEBIT)
                .withAmount(BigDecimal.valueOf(100))
                .withUserId(USER_ID)
                .build();

        journal.addRecordtoJournal(record20200610);

        //1593590400
        Long milis20200710 = LocalDateTime.parse("2020-07-10 12:00:00", formatter)
            .atZone(ZoneId.systemDefault())
            .toInstant()
            .toEpochMilli();

        RecordWriter record20200710 = new RecordWriter.Builder()
                .withDescription("record for 20200710")
                .withUtcDate(String.valueOf(milis20200710))
                .withLocalDate(String.valueOf(milis20200710))
                .withCategory("july")
                .withSubCategory("subcat")
                .withType(RecordType.DEBIT)
                .withAmount(BigDecimal.valueOf(100))
                .withUserId(USER_ID)
                .build();

        journal.addRecordtoJournal(record20200710);

        Long milis20200720 = LocalDateTime.parse("2020-07-20 12:00:00", formatter)
                .atZone(ZoneId.systemDefault())
                .toInstant()
                .toEpochMilli();

        RecordWriter record20200720 = new RecordWriter.Builder()
                .withDescription("record for 20200720")
                .withUtcDate(String.valueOf(milis20200720))
                .withLocalDate(String.valueOf(milis20200720))
                .withCategory("july")
                .withSubCategory("subcat")
                .withType(RecordType.DEBIT)
                .withAmount(BigDecimal.valueOf(100))
                .withUserId(USER_ID)
                .build();

        journal.addRecordtoJournal(record20200720);

        //1596268800
        Long milis20200805 = LocalDateTime.parse("2020-08-05 12:00:00", formatter)
                .atZone(ZoneId.systemDefault())
                .toInstant()
                .toEpochMilli();

        RecordWriter record20200805 = new RecordWriter.Builder()
                .withDescription("record for 20200805")
                .withUtcDate(String.valueOf(milis20200805))
                .withLocalDate(String.valueOf(milis20200805))
                .withCategory("august")
                .withSubCategory("subcat")
                .withType(RecordType.DEBIT)
                .withAmount(BigDecimal.valueOf(100))
                .withUserId(USER_ID)
                .build();

        journal.addRecordtoJournal(record20200805);
    }

    public UUID addJournal(String description, String status, UUID userId) {
        UUID uuid = getValidId();
        FakeJournalDataStore journal = new FakeJournalDataStore(uuid, description, status, userId);

        journals.put(uuid, journal);
        return uuid;
    }

    public Optional<FakeJournalDataStore> getJournal(UUID id) {
        return Optional.of(journals.get(id));
    }

    public boolean idExists(UUID id) {
        return journals.containsKey(id);
    }

    public List<UUID> getAllJournalIds(UUID userId) {
        return journals.entrySet()
                .stream()
                .filter(e -> e.getValue().getJournaldata().getUserId().equals(userId))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public List<Journal> getAllJournals(UUID userId) {
        return journals.entrySet()
                .stream()
                .filter(e -> e.getValue().getJournaldata().getUserId().equals(userId))
                .map(e -> e.getValue().getJournaldata())
                .collect(Collectors.toList());
    }

    private UUID getValidId() {
        UUID id = UUID.randomUUID();
        if (idExists(id)) {
            return getValidId();
        }
        else {
            return id;
        }
    }

    public UUID addRecord(UUID journalId, String description, String category, String subCategory, RecordType type, BigDecimal amount, UUID userId) {
        FakeJournalDataStore journal = getJournal(journalId).get();
        RecordWriter record = new RecordWriter.Builder()
                .withDescription(description)
                .withUtcDate(String.valueOf(Instant.now().toEpochMilli()))
                .withLocalDate(String.valueOf(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()))
                .withCategory(category)
                .withSubCategory(subCategory)
                .withType(type)
                .withAmount(amount)
                .withUserId(userId)
                .build();

        return journal.addRecordtoJournal(record);
    }

    public UUID addRecord(UUID journalId, RecordWriter record) {
        FakeJournalDataStore journal = getJournal(journalId).get();
        return journal.addRecordtoJournal(record);
    }

    public Optional<List<Record>> getJournalRecords(UUID journalId) {
        FakeJournalDataStore journal = getJournal(journalId).get();
        return Optional.of(journal.getRecords());
    }

    public Optional<List<Record>> getJournalRecordsByDateRange(UUID journalId, long startDate, long endDate) {
        FakeJournalDataStore journal = getJournal(journalId).get();
        return Optional.of(journal.getRecordsByDateRange(startDate, endDate));
    }

    public Optional<Map<RecordType, List<Record>>> getJournalRecordsGroupedByType(UUID journalId) {
        FakeJournalDataStore journal = getJournal(journalId).get();
        return Optional.of(journal.getRecordsGroupedType());
    }

    public Optional<Map<String, List<Record>>> getJournalRecordsGroupedCategory(UUID journalId) {
        FakeJournalDataStore journal = getJournal(journalId).get();
        return Optional.of(journal.getRecordsGroupedCategory());
    }

    public Optional<Map<String, BigDecimal>> getSumByCategory(UUID journalId) {
        FakeJournalDataStore journal = getJournal(journalId).get();
        return Optional.of(journal.getSumByCategory());
    }

    public Optional<Map<String, BigDecimal>> getSumByMonth(UUID journalId) {
        FakeJournalDataStore journal = getJournal(journalId).get();
        return Optional.of(journal.getSumByMonth());
    }

    public Optional<Map<String, BigDecimal>> getSumByMonthByPeriod(UUID journalId, Period period) {
        FakeJournalDataStore journal = getJournal(journalId).get();
        return Optional.of(journal.getSumByMonthByPeriod(period));
    }

}
