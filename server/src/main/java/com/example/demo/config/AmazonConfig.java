package com.example.demo.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.example.demo.utils.JWTTokenUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//        AWSCredentials awsCredentials = new BasicAWSCredentials(
//                "AKIATU6UP7D5RFK4PNET",
//                "xgboGwHKqxJTtM4TqFimR7tnZt05Ermyisr4fazp"
//        );

@Configuration
public class AmazonConfig {

    // This is for my user, and my tests
//    private final String ACCESS_KEY = "AKIATU6UP7D5SHRNV2CF";
//    private final String SECRET_KEY = "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";

    // Beta Metacritic
//    private final String ACCESS_KEY = "AKIAWES2CPLO32IFKKL2";
//    private final String SECRET_KEY = "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";

    //Prod Metacritic
//    private final String ACCESS_KEY = "AKIARNVPBZCIY3J6P3UB";
    //                                   AKIARNVPBZCIY3J6P3UB
//    private final String SECRET_KEY = "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";

    //DynamoDB avis table access keys
    private final String ACCESS_KEY = "AKIARBIYO5GMUZVK4E6K";
    private final String SECRET_KEY = "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";

//    private final String ACCESS_KEY = "AKIATU6UP7D53S2AV54S";
//    private final String SECRET_KEY = "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";

    private AWSCredentials awsCredentials = new BasicAWSCredentials(
            ACCESS_KEY,
            SECRET_KEY
    );

    @Bean
    public AmazonS3 s3() {
        return AmazonS3ClientBuilder
                .standard()
                .withRegion(Regions.US_WEST_2)

                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();
    }

    @Bean
    public AWSLambda lambda() {
        return AWSLambdaClientBuilder
                .standard()
                .withRegion(Regions.US_WEST_2)
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();
    }

    @Bean
    public AmazonDynamoDB dynamodb() {
        return AmazonDynamoDBClientBuilder
                .standard()
//                .withRegion(Regions.US_WEST_2)
                .withRegion(Regions.US_EAST_1)
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();
    }

    @Bean
    public AmazonDynamoDB dynamodbPersonal() {

        String ACCESS_KEY_PERSONAL = "AKIATU6UP7D53S2AV54S";
        String SECRET_KEY_PERSONAL = "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";

        AWSCredentials awsCredentials = new BasicAWSCredentials(
                ACCESS_KEY_PERSONAL,
                SECRET_KEY_PERSONAL
        );

        return AmazonDynamoDBClientBuilder
                .standard()
                .withRegion(Regions.US_WEST_2)
//                .withRegion(Regions.US_EAST_1)
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();
    }

    @Bean
    public JWTTokenUtil jwtTokenUtil() {
        return new JWTTokenUtil();
    }

}
