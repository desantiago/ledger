package com.example.demo.ledger;

import org.apache.tomcat.jni.Local;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.WeekFields;
import java.util.Locale;

import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;

public enum Period {
    CURRENT_WEEK {public PeriodRange getRange() {
        DayOfWeek firstDayOfWeek;
        DayOfWeek lastDayOfWeek;
        Locale spanishLocale=new Locale("es", "ES");
        firstDayOfWeek = WeekFields.of(spanishLocale).getFirstDayOfWeek();
        lastDayOfWeek = DayOfWeek.of(((firstDayOfWeek.getValue() + 5) % DayOfWeek.values().length) + 1);

//        LocalDate localDate = LocalDate.now(ZoneId.systemDefault());
        LocalDate localDate = LocalDateFactory.now();
        LocalDate firstDay = localDate.with(TemporalAdjusters.previousOrSame(firstDayOfWeek));
        LocalDate lastDay = localDate.with(TemporalAdjusters.nextOrSame(lastDayOfWeek));
        Instant startDate = Period.localDateToInstantAtStartOfDay(firstDay);
        Instant endDate = Period.localDateToInstantAtEndOfDay(lastDay);

//        Instant startDate = LocalDate.now(ZoneId.systemDefault())
//                .with(TemporalAdjusters.previousOrSame(firstDayOfWeek))
//                .atStartOfDay(ZoneId.systemDefault())
//                .toInstant();
//        Instant endDate = LocalDate.now(ZoneId.systemDefault())
//                .with(TemporalAdjusters.nextOrSame(lastDayOfWeek))
//                .atTime(LocalTime.MAX)
//                .atZone(ZoneId.systemDefault())
//                .toInstant();
        System.out.println(formatter.format(startDate));
        System.out.println(formatter.format(endDate));
        return new PeriodRange(startDate.toEpochMilli(), endDate.toEpochMilli());
    } },
    LAST_WEEK {public PeriodRange getRange() {
        DayOfWeek firstDayOfWeek;
        DayOfWeek lastDayOfWeek;
        Locale spanishLocale=new Locale("en", "US");
        firstDayOfWeek = WeekFields.of(spanishLocale).getFirstDayOfWeek();
        lastDayOfWeek = DayOfWeek.of(((firstDayOfWeek.getValue() + 5) % DayOfWeek.values().length) + 1);

//        LocalDate localDate = LocalDate.now(ZoneId.systemDefault());
        LocalDate localDate = LocalDateFactory.now();
        LocalDate firstDay = localDate.minusWeeks(1).with(TemporalAdjusters.previousOrSame(firstDayOfWeek));
        LocalDate lastDay = localDate.minusWeeks(1).with(TemporalAdjusters.nextOrSame(lastDayOfWeek));
        Instant startDate = Period.localDateToInstantAtStartOfDay(firstDay);
        Instant endDate = Period.localDateToInstantAtEndOfDay(lastDay);

//        Instant startDate = LocalDate.now(ZoneId.systemDefault())
//                .minusWeeks(1)
//                .with(TemporalAdjusters.previousOrSame(firstDayOfWeek))
//                .atStartOfDay(ZoneId.systemDefault())
//                .toInstant();
//        Instant endDate = LocalDate.now(ZoneId.systemDefault())
//                .minusWeeks(1)
//                .with(TemporalAdjusters.nextOrSame(lastDayOfWeek))
//                .atTime(LocalTime.MAX)
//                .atZone(ZoneId.systemDefault())
//                .toInstant();
        System.out.println(formatter.format(startDate));
        System.out.println(formatter.format(endDate));
        return new PeriodRange(startDate.toEpochMilli(), endDate.toEpochMilli());
    } },
    LAST_TWO_WEEKS {public PeriodRange getRange() {
        DayOfWeek firstDayOfWeek;
        DayOfWeek lastDayOfWeek;
        Locale spanishLocale=new Locale("en", "US");
        firstDayOfWeek = WeekFields.of(spanishLocale).getFirstDayOfWeek();
        lastDayOfWeek = DayOfWeek.of(((firstDayOfWeek.getValue() + 5) % DayOfWeek.values().length) + 1);

//        LocalDate localDate = LocalDate.now(ZoneId.systemDefault());
        LocalDate localDate = LocalDateFactory.now();
        LocalDate firstDay = localDate.minusWeeks(1).with(TemporalAdjusters.previousOrSame(firstDayOfWeek));
        LocalDate lastDay = localDate.with(TemporalAdjusters.nextOrSame(lastDayOfWeek));
        Instant startDate = Period.localDateToInstantAtStartOfDay(firstDay);
        Instant endDate = Period.localDateToInstantAtEndOfDay(lastDay);

//        Instant startDate = LocalDate.now(ZoneId.systemDefault())
//                .minusWeeks(1)
//                .with(TemporalAdjusters.previousOrSame(firstDayOfWeek))
//                .atStartOfDay(ZoneId.systemDefault())
//                .toInstant();
//        Instant endDate = LocalDate.now(ZoneId.systemDefault())
//                .with(TemporalAdjusters.nextOrSame(lastDayOfWeek))
//                .atTime(LocalTime.MAX)
//                .atZone(ZoneId.systemDefault())
//                .toInstant();
        System.out.println(formatter.format(startDate));
        System.out.println(formatter.format(endDate));
        return new PeriodRange(startDate.toEpochMilli(), endDate.toEpochMilli());
    } },
    CURRENT_MONTH {public PeriodRange getRange() {
//        LocalDate currentMonth = LocalDate.now(ZoneId.systemDefault());
        LocalDate currentMonth = LocalDateFactory.now();

//        Instant firstDayOfMonth = currentMonth
//                .withDayOfMonth(1)
//                .atStartOfDay(ZoneId.systemDefault())
//                .toInstant();
//        Instant lastDayOfMonth = currentMonth
//                .withDayOfMonth(currentMonth.getMonth().length(currentMonth.isLeapYear()))
//                .atTime(LocalTime.MAX)
//                .atZone(ZoneId.systemDefault())
//                .toInstant();

        LocalDate firstDay = currentMonth.withDayOfMonth(1);
        LocalDate lastDay = currentMonth.withDayOfMonth(currentMonth.getMonth().length(currentMonth.isLeapYear()));
        Instant firstDayOfMonth = Period.localDateToInstantAtStartOfDay(firstDay);
        Instant lastDayOfMonth = Period.localDateToInstantAtEndOfDay(lastDay);

        System.out.println(formatter.format(firstDayOfMonth));
        System.out.println(formatter.format(lastDayOfMonth));
        return new PeriodRange(firstDayOfMonth.toEpochMilli(), lastDayOfMonth.toEpochMilli());
    } },
    LAST_MONTH {public PeriodRange getRange() {
//        LocalDate currentMonth = LocalDate.now(ZoneId.systemDefault());
//        Instant firstDayOfMonth = currentMonth
//                .minusMonths(1)
//                .withDayOfMonth(1)
//                .atStartOfDay(ZoneId.systemDefault())
//                .toInstant();
//        Instant lastDayOfMonth = currentMonth
//                .minusMonths(1)
//                .withDayOfMonth(currentMonth.getMonth().length(currentMonth.isLeapYear()))
//                .atTime(LocalTime.MAX)
//                .atZone(ZoneId.systemDefault())
//                .toInstant();

        LocalDate currentMonth = LocalDateFactory.now();
        LocalDate firstDay = currentMonth.minusMonths(1).withDayOfMonth(1);
        LocalDate lastDay = currentMonth.minusMonths(1).with(TemporalAdjusters.lastDayOfMonth());
        Instant firstDayOfMonth = Period.localDateToInstantAtStartOfDay(firstDay);
        Instant lastDayOfMonth = Period.localDateToInstantAtEndOfDay(lastDay);

        System.out.println(formatter.format(firstDayOfMonth));
        System.out.println(formatter.format(lastDayOfMonth));
        return new PeriodRange(firstDayOfMonth.toEpochMilli(), lastDayOfMonth.toEpochMilli());
    } },
    LAST_TWO_MONTHS {public PeriodRange getRange() {
//        LocalDate currentMonth = LocalDate.now(ZoneId.systemDefault());
//        Instant firstDayOfMonth = currentMonth
//                .minusMonths(1)
//                .withDayOfMonth(1)
//                .atStartOfDay(ZoneId.systemDefault())
//                .toInstant();
//        Instant lastDayOfMonth = currentMonth
//                .withDayOfMonth(currentMonth.getMonth().length(currentMonth.isLeapYear()))
//                .atTime(LocalTime.MAX)
//                .atZone(ZoneId.systemDefault())
//                .toInstant();

        LocalDate currentMonth = LocalDateFactory.now();
        LocalDate firstDay = currentMonth.minusMonths(1).withDayOfMonth(1);
        LocalDate lastDay = currentMonth.with(TemporalAdjusters.lastDayOfMonth());;
        Instant firstDayOfMonth = Period.localDateToInstantAtStartOfDay(firstDay);
        Instant lastDayOfMonth = Period.localDateToInstantAtEndOfDay(lastDay);

        System.out.println(formatter.format(firstDayOfMonth));
        System.out.println(formatter.format(lastDayOfMonth));
        return new PeriodRange(firstDayOfMonth.toEpochMilli(), lastDayOfMonth.toEpochMilli());
    } },
    LAST_THREE_MONTHS {public PeriodRange getRange() {
//        LocalDate currentMonth = LocalDate.now(ZoneId.systemDefault());

        LocalDate currentMonth = LocalDateFactory.now();
        LocalDate firstDay = currentMonth.minusMonths(2).withDayOfMonth(1);
        LocalDate lastDay = currentMonth.with(TemporalAdjusters.lastDayOfMonth());
        Instant firstDayOfMonth = Period.localDateToInstantAtStartOfDay(firstDay);
        Instant lastDayOfMonth = Period.localDateToInstantAtEndOfDay(lastDay);

//        Instant firstDayOfMonth = currentMonth
//                .minusMonths(2)
//                .withDayOfMonth(1)
//                .atStartOfDay(ZoneId.systemDefault())
//                .toInstant();
//        Instant lastDayOfMonth = currentMonth
//                .withDayOfMonth(currentMonth.getMonth().length(currentMonth.isLeapYear()))
//                .atTime(LocalTime.MAX)
//                .atZone(ZoneId.systemDefault())
//                .toInstant();

        System.out.println(formatter.format(firstDayOfMonth));
        System.out.println(formatter.format(lastDayOfMonth));
        return new PeriodRange(firstDayOfMonth.toEpochMilli(), lastDayOfMonth.toEpochMilli());
    } },
    CURRENT_QUARTER {public PeriodRange getRange() {
//        LocalDate localDate = LocalDate.now(ZoneId.systemDefault());
        LocalDate localDate = LocalDateFactory.now();
        LocalDate firstDay = localDate.with(localDate.getMonth().firstMonthOfQuarter())
                .with(TemporalAdjusters.firstDayOfMonth());
        LocalDate lastDay = firstDay.plusMonths(2)
                .with(TemporalAdjusters.lastDayOfMonth());

        Instant firstDayOfQuarter = Period.localDateToInstantAtStartOfDay(firstDay);
        Instant lastDayOfQuarter = Period.localDateToInstantAtEndOfDay(lastDay);

        System.out.println(formatter.format(firstDayOfQuarter));
        System.out.println(formatter.format(lastDayOfQuarter));
        return new PeriodRange(firstDayOfQuarter.toEpochMilli(), lastDayOfQuarter.toEpochMilli());
    } },
    LAST_QUARTER {public PeriodRange getRange() {
//        LocalDate localDate = LocalDate.now(ZoneId.systemDefault()).minusMonths(3);
        LocalDate localDate = LocalDateFactory.now().minusMonths(3);
        LocalDate firstDay = localDate.with(localDate.getMonth().firstMonthOfQuarter())
                .with(TemporalAdjusters.firstDayOfMonth());
        LocalDate lastDay = firstDay.plusMonths(2)
                .with(TemporalAdjusters.lastDayOfMonth());

        Instant firstDayOfQuarter = Period.localDateToInstantAtStartOfDay(firstDay);
        Instant lastDayOfQuarter = Period.localDateToInstantAtEndOfDay(lastDay);

        System.out.println(formatter.format(firstDayOfQuarter));
        System.out.println(formatter.format(lastDayOfQuarter));
        return new PeriodRange(firstDayOfQuarter.toEpochMilli(), lastDayOfQuarter.toEpochMilli());
    } },
    CURRENT_YEAR {public PeriodRange getRange() {
//        LocalDate now = LocalDate.now();
        LocalDate localDate = LocalDateFactory.now();

        LocalDate firstDay = localDate.with(firstDayOfYear());
        LocalDate lastDay = localDate.with(lastDayOfYear());
        Instant firstDayOfYear = Period.localDateToInstantAtStartOfDay(firstDay);
        Instant lastDayOfYear = Period.localDateToInstantAtEndOfDay(lastDay);

        System.out.println(formatter.format(firstDayOfYear));
        System.out.println(formatter.format(lastDayOfYear));
        return new PeriodRange(firstDayOfYear.toEpochMilli(), lastDayOfYear.toEpochMilli());
    } },
    LAST_YEAR {public PeriodRange getRange() {
//        LocalDate now = LocalDate.now().minusYears(1);
        LocalDate now = LocalDateFactory.now().minusYears(1);

        LocalDate firstDay = now.with(firstDayOfYear());
        LocalDate lastDay = now.with(lastDayOfYear());
        Instant firstDayOfYear = Period.localDateToInstantAtStartOfDay(firstDay);
        Instant lastDayOfYear = Period.localDateToInstantAtEndOfDay(lastDay);

        System.out.println(formatter.format(firstDayOfYear));
        System.out.println(formatter.format(lastDayOfYear));
        return new PeriodRange(firstDayOfYear.toEpochMilli(), lastDayOfYear.toEpochMilli());
    } },
    LAST_7_DAYS {public PeriodRange getRange() {
//        LocalDate now = LocalDate.now();
        LocalDate now = LocalDateFactory.now();
        LocalDate firstDay = now.minusDays(6);
        LocalDate lastDay = now;
        Instant firstDayOfYear = Period.localDateToInstantAtStartOfDay(firstDay);
        Instant lastDayOfYear = Period.localDateToInstantAtEndOfDay(lastDay);

        System.out.println(formatter.format(firstDayOfYear));
        System.out.println(formatter.format(lastDayOfYear));
        return new PeriodRange(firstDayOfYear.toEpochMilli(), lastDayOfYear.toEpochMilli());
    } },
    LAST_14_DAYS {public PeriodRange getRange() {
//        LocalDate now = LocalDate.now();
        LocalDate now = LocalDateFactory.now();
        LocalDate firstDay = now.minusDays(13);
        LocalDate lastDay = now;
        Instant firstDayOfYear = Period.localDateToInstantAtStartOfDay(firstDay);
        Instant lastDayOfYear = Period.localDateToInstantAtEndOfDay(lastDay);

        System.out.println(formatter.format(firstDayOfYear));
        System.out.println(formatter.format(lastDayOfYear));
        return new PeriodRange(firstDayOfYear.toEpochMilli(), lastDayOfYear.toEpochMilli());
    } },
    LAST_30_DAYS {public PeriodRange getRange() {
//        LocalDate now = LocalDate.now();
        LocalDate now = LocalDateFactory.now();
        LocalDate firstDay = now.minusDays(29);
        LocalDate lastDay = now;
        Instant firstDayOfYear = Period.localDateToInstantAtStartOfDay(firstDay);
        Instant lastDayOfYear = Period.localDateToInstantAtEndOfDay(lastDay);

        System.out.println(formatter.format(firstDayOfYear));
        System.out.println(formatter.format(lastDayOfYear));
        return new PeriodRange(firstDayOfYear.toEpochMilli(), lastDayOfYear.toEpochMilli());
    } };

    private static DateTimeFormatter formatter =
            DateTimeFormatter.ofLocalizedDateTime( FormatStyle.SHORT )
                    .withLocale( Locale.getDefault() )
                    .withZone( ZoneId.systemDefault() );
    public abstract  PeriodRange getRange();

    private static Instant localDateToInstantAtStartOfDay(LocalDate localDate) {
        return localDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
    }

    private static Instant localDateToInstantAtEndOfDay(LocalDate localDate) {
        return localDate.atTime(23, 59, 59)
                .atZone(ZoneId.systemDefault())
                .toInstant();
    }
}
