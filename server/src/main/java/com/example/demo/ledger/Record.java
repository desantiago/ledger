package com.example.demo.ledger;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

public class Record extends RecordBase {
    private UUID id;
    private String timeStamp;
    private BigDecimal balance;

    public static class Builder extends RecordBase.Builder<Builder> {
        private String timeStamp;
        private BigDecimal balance;
        private UUID id;

        public Builder() {}

        public Builder withId(UUID id) {
            this.id = id; return this;
        }

        public Builder withBalance(BigDecimal balance) {
            this.balance = balance; return this;
        }

        public Builder withTimestamp(String timeStamp) {
            this.timeStamp = timeStamp; return this;
        }

        @Override
        public Record build() {
            return new Record(this);
        }

        @Override
        protected Builder self() { return this; }

        public Builder buildFromRecordWriter(RecordWriter recordWriter) {
            withDescription(recordWriter.getDescription());
            withLocalDate(recordWriter.getLocalDate());
            withUtcDate(recordWriter.getUtcDate());
            withType(recordWriter.getType());
            withAmount(recordWriter.getAmount());
            withCategory(recordWriter.getCategory());
            withSubCategory(recordWriter.getSubCategory());
            withUserId(recordWriter.getUserId());
            return this;
        }
    }

    private Record(Builder builder) {
        super(builder);
        Objects.requireNonNull(builder.id);
        Objects.requireNonNull(builder.balance);

        id = UUID.fromString(builder.id.toString());
        timeStamp = String.valueOf(builder.timeStamp);
        balance = new BigDecimal(builder.balance.toString());
    }

    public UUID getId() {
        return id;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Record record = (Record) o;
        return id.equals(record.id) &&
                timeStamp.equals(record.timeStamp) &&
                balance.equals(record.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, timeStamp, balance);
    }

    @Override
    public String toString() {
        return "Record{" +
                "id=" + id +
                ", timeStamp=" + timeStamp +
                ", balance=" + balance +
                '}';
    }
}
