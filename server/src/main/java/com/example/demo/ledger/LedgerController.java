package com.example.demo.ledger;

import com.example.demo.datastore.FakeLedgerDataStore;
import com.example.demo.utils.Utils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;

@RestController
@RequestMapping("api/v1/ledger")
@CrossOrigin("*")
public class LedgerController {
    private final LedgerService ledgerService;

    public LedgerController(LedgerService ledgerService) {
        this.ledgerService = ledgerService;
    }

    @GetMapping(path = "/all")
    public List<UUID> getAllJournalIds() {
        return ledgerService.getAllJournalIds(FakeLedgerDataStore.USER_ID);
    }

    @GetMapping(path = "/journals")
    public List<Journal> getAllJournals() {
        return ledgerService.getAllJournals(FakeLedgerDataStore.USER_ID);
    }

//        Journal journal = ledger.getJournal(journalId)
//                .map(Journal::getJournal).orElseGet(() -> null);

    @GetMapping(path = "/{journalId}/records")
    public List<Record> getJournalRecords(@PathVariable("journalId")UUID journalId) {
        if (ledgerService.getJournalRecords(journalId).isPresent()) {
            return ledgerService.getJournalRecords(journalId).get();
        }
        return Collections.EMPTY_LIST;
    }

//    @GetMapping(path = "/{journalId}/records/{period}")
//    public List<Record> getJournalRecordsByPeriod(@PathVariable("journalId")UUID journalId, @PathVariable("period") Period period) {
//        if (ledgerService.getJournalRecordsByPeriod(journalId, period).isPresent()) {
//            return ledgerService.getJournalRecordsByPeriod(journalId, period).get();
//        }
//        return Collections.EMPTY_LIST;
//    }

    @GetMapping(path = "/{journalId}/records-by-category")
    public  Map<String, List<Record>> getJournalRecordsGroupedCategory(@PathVariable("journalId")UUID journalId) {
        Optional<Map<String, List<Record>>> records = ledgerService.getJournalRecordsGroupedCategory(journalId);
        if (records.isPresent()) {
            return records.get();
        }
        return Collections.EMPTY_MAP;
    }

    @GetMapping(path = "/{journalId}/agg-by-category")
    public Map<String, BigDecimal> getSumByCategory(@PathVariable("journalId")UUID journalId) {
        Optional<Map<String, BigDecimal>> records = ledgerService.getSumByCategory(journalId);
        if (records.isPresent()) {
            return records.get();
        }
        return Collections.EMPTY_MAP;
    }

//    @GetMapping(path = "/{journalId}/agg-by-category/{period}")
//    public Map<String, BigDecimal> getSumByCategoryByPeriod(@PathVariable("journalId")UUID journalId, @PathVariable("period") Period period) {
//        Optional<Map<String, BigDecimal>> records = ledgerService.getSumByCategoryByPeriod(journalId, period);
//        if (records.isPresent()) {
//            return records.get();
//        }
//        return Collections.EMPTY_MAP;
//    }

    @GetMapping(path = "/{journalId}/records-by-daterange/{startDate}/{endDate}")
    public List<Record> getRecordsByDateRange(
            @PathVariable("journalId")UUID journalId,
            @PathVariable("startDate")String startDate,
            @PathVariable("endDate")String endDate) {

        Optional<Long> startDateLong = Utils.parseLong(startDate);
        Optional<Long> endDateLong = Utils.parseLong(endDate);

        if (!startDateLong.isPresent()) return Collections.EMPTY_LIST;
        if (!endDateLong.isPresent()) return Collections.EMPTY_LIST;

        if (ledgerService.getJournalRecordsByDateRange(journalId, startDateLong.get(), endDateLong.get()).isPresent()) {
            return ledgerService.getJournalRecordsByDateRange(journalId, Long.parseLong(startDate), Long.parseLong(endDate)).get();
        }
        return Collections.EMPTY_LIST;
    }

    @GetMapping(path = "/{journalId}/agg-by-month")
    public Map<String, BigDecimal> getSumByMonth(@PathVariable("journalId")UUID journalId) {
        Optional<Map<String, BigDecimal>> records = ledgerService.getSumByMonth(journalId);
        if (records.isPresent()) {
            return records.get();
        }
        return Collections.EMPTY_MAP;
    }

    @GetMapping(path = "/{journalId}/agg-by-month/{period}")
    public Map<String, BigDecimal> getSumByMonthByPeriod(@PathVariable("journalId")UUID journalId, @PathVariable("period") Period period) {
        Optional<Map<String, BigDecimal>> records = ledgerService.getSumByMonthByPeriod(journalId, period);
        if (records.isPresent()) {
            return records.get();
        }
        return Collections.EMPTY_MAP;
    }

    @GetMapping(path = "/period/{period}")
    public PeriodRange getPeriod(@PathVariable("period") Period period) {
//        period.
//        return Period.CURRENT_WEEK.getRange();

        return period.getRange();
    }

//    @GetMapping(path = "/bycategory")
//    public Map<String, List<Record>> getRecordsGroupedCategory() {
//        return journal.getRecordsGroupedCategory();
//    }

//    @GetMapping
//    public List<Record> getLedger() {
//        Ledger ledger = new Ledger.Builder().withDescription("main ledger").withStatus("open").build();
//        UUID id = ledger.createJournal("journal", "open");
//        Optional<Journal> optionalJournal = ledger.getJournal(id);
//
//        if (!optionalJournal.isPresent()) return new ArrayList();
//
//        Journal journal = optionalJournal.get();
//
//        RecordWriter record = new RecordWriter.Builder()
//                .withDescription("test")
//                .withDate(Instant.now())
//                .withCategory("cat")
//                .withSubCategory("subcat")
//                .withType(RecordType.DEBIT)
//                .withAmount(BigDecimal.valueOf(100))
//                .build();
//
//        journal.addToJournal(record);
//
//        RecordWriter record2 = new RecordWriter.Builder()
//                .withDescription("test")
//                .withDate(Instant.now())
//                .withCategory("cat")
//                .withSubCategory("subcat")
//                .withType(RecordType.DEBIT)
//                .withAmount(BigDecimal.valueOf(150))
//                .build();
//        journal.addToJournal(record2);
//
//        RecordWriter record3 = new RecordWriter.Builder()
//                .withDescription("test")
//                .withDate(Instant.now())
//                .withCategory("cat2")
//                .withSubCategory("subcat")
//                .withType(RecordType.DEBIT)
//                .withAmount(BigDecimal.valueOf(350))
//                .build();
//        journal.addToJournal(record3);
//
//        RecordWriter record4 = new RecordWriter.Builder()
//                .withDescription("test")
//                .withDate(Instant.now())
//                .withCategory("cat")
//                .withSubCategory("subcat")
//                .withType(RecordType.CREDIT)
//                .withAmount(BigDecimal.valueOf(50))
//                .build();
//        journal.addToJournal(record4);
//
//        RecordWriter record5 = new RecordWriter.Builder()
//                .withDescription("test")
//                .withDate(Instant.now())
//                .withCategory("cat2")
//                .withSubCategory("subcat")
//                .withType(RecordType.CREDIT)
//                .withAmount(BigDecimal.valueOf(50))
//                .build();
//        journal.addToJournal(record5);
//
//        System.out.println("here");
//
//        return journal.getRecords();
//    }

//    @GetMapping(path = "/bytype")
//    public Map<RecordType, List<Record>> getRecordsGroupedType() {
//        return journal.getRecordsGroupedType();
//    }
//
//    @GetMapping(path = "/bycategory")
//    public Map<String, List<Record>> getRecordsGroupedCategory() {
//        return journal.getRecordsGroupedCategory();
//    }
//
//    @GetMapping(path = "/sumcategory")
//    public Map<String, BigDecimal> getSumByCategory() {
//        return journal.getSumByCategory();
//    }
}