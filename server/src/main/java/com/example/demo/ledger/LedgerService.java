package com.example.demo.ledger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class LedgerService {
    private final LedgerDataAccessService ledgerDataAccessService;

    @Autowired
    public LedgerService(LedgerDataAccessService ledgerDataAccessService) {
        this.ledgerDataAccessService = ledgerDataAccessService;
    }

    public UUID addJournal(String description, String status, UUID userId) {
        return ledgerDataAccessService.addJournal(description, status, userId);
    }

    public UUID addRecord(UUID journalId, String description, String category, String subCategory, RecordType type, BigDecimal amount, UUID userId) {
        return ledgerDataAccessService.addRecord(journalId, description, category, subCategory, type, amount, userId);
    }

    public List<UUID> getAllJournalIds(UUID userId) {
        return ledgerDataAccessService.getAllJournalIds(userId);
    }

    public List<Journal> getAllJournals(UUID userId) {
        return ledgerDataAccessService.getAllJournals(userId);
    }

    public Optional<List<Record>> getJournalRecords(UUID journalId) {
        return ledgerDataAccessService.getJournalRecords(journalId);
    }

    public Optional<List<Record>> getJournalRecordsByDateRange(UUID journalId, long startDate, long endDate) {
        return ledgerDataAccessService.getJournalRecordsByDateRange(journalId, startDate, endDate);
    }

    public Optional<Map<RecordType, List<Record>>> getJournalRecordsGroupedByType(UUID journalId) {
        return ledgerDataAccessService.getJournalRecordsGroupedByType(journalId);
    }

    public Optional<Map<String, List<Record>>> getJournalRecordsGroupedCategory(UUID journalId) {
        return ledgerDataAccessService.getJournalRecordsGroupedCategory(journalId);
    }

    public Optional<Map<String, BigDecimal>> getSumByCategory(UUID journalId) {
        return ledgerDataAccessService.getSumByCategory(journalId);
    }

    public Optional<Map<String, BigDecimal>> getSumByMonth(UUID journalId) {
        return ledgerDataAccessService.getSumByMonth(journalId);
    }

    public Optional<Map<String, BigDecimal>> getSumByMonthByPeriod(UUID journalId, Period period) {
        return ledgerDataAccessService.getSumByMonthByPeriod(journalId, period);
    }

}
