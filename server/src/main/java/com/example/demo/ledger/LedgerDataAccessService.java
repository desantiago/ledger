package com.example.demo.ledger;

import com.example.demo.datastore.FakeLedgerDataStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Repository
public class LedgerDataAccessService {
    private final FakeLedgerDataStore fakeLedgerDataStore;

    @Autowired
    public LedgerDataAccessService(FakeLedgerDataStore fakeLedgerDataStore) {
        this.fakeLedgerDataStore = fakeLedgerDataStore;
    }

    public UUID addJournal(String description, String status, UUID userId) {
        return fakeLedgerDataStore.addJournal(description, status, userId);
    }

    public List<UUID> getAllJournalIds(UUID userId) {
        return fakeLedgerDataStore.getAllJournalIds(userId);
    }

    public List<Journal> getAllJournals(UUID userId) {
        return fakeLedgerDataStore.getAllJournals(userId);
    }

    public UUID addRecord(UUID journalId, String description, String category, String subCategory, RecordType type, BigDecimal amount, UUID userId) {
        return fakeLedgerDataStore.addRecord(journalId, description, category, subCategory, type, amount, userId);
    }

    public Optional<List<Record>> getJournalRecords(UUID journalId) {
        return fakeLedgerDataStore.getJournalRecords(journalId);
    }

    public Optional<List<Record>> getJournalRecordsByDateRange(UUID journalId, long startDate, long endDate) {
        return fakeLedgerDataStore.getJournalRecordsByDateRange(journalId, startDate, endDate);
    }

    public Optional<Map<RecordType, List<Record>>> getJournalRecordsGroupedByType(UUID journalId) {
        return fakeLedgerDataStore.getJournalRecordsGroupedByType(journalId);
    }

    public Optional<Map<String, List<Record>>> getJournalRecordsGroupedCategory(UUID journalId) {
        return fakeLedgerDataStore.getJournalRecordsGroupedCategory(journalId);
    }

    public Optional<Map<String, BigDecimal>> getSumByCategory(UUID journalId) {
        return fakeLedgerDataStore.getSumByCategory(journalId);
    }

    public Optional<Map<String, BigDecimal>> getSumByMonth(UUID journalId) {
        return fakeLedgerDataStore.getSumByMonth(journalId);
    }

    public Optional<Map<String, BigDecimal>> getSumByMonthByPeriod(UUID journalId, Period period) {
        return fakeLedgerDataStore.getSumByMonthByPeriod(journalId, period);
    }

}
