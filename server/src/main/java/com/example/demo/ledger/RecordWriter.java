package com.example.demo.ledger;

public class RecordWriter extends RecordBase {

    public static class Builder extends RecordBase.Builder<Builder> {

        public Builder() {}

        @Override
        public RecordWriter build() {
            return new RecordWriter(this);
        }

        @Override
        protected Builder self() {
            return this;
        }

    }

    private RecordWriter(Builder builder) {
        super(builder);
    }
}
