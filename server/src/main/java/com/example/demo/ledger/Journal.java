package com.example.demo.ledger;

import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

public class Journal {
    private UUID id;
    private String timeStamp;
    private String description;
    private String status;
    private UUID userId;

    public static class Builder {
        private String description;
        private String status;
        private UUID id;
        private UUID userId;

        public Builder(UUID id) {
            this.id = id;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public Builder withUserId(UUID userId) {
            this.userId = userId;
            return this;
        }

        public Journal build() {
            return new Journal(id, description, status, userId);
        }
    }

    private Journal(UUID id, String description, String status, UUID userId) {
        Objects.requireNonNull(description);
        Objects.requireNonNull(status);
        Objects.requireNonNull(userId);

        this.id = id;
        this.timeStamp = String.valueOf(Instant.now().toEpochMilli());
        this.description = description;
        this.status = status;
        this.userId = userId;
    }

//    public JournalData(UUID id, Instant timeStamp, String description, String status, String userId) {
//        this.id = id;
//        this.timeStamp = timeStamp;
//        this.description = description;
//        this.status = status;
//        this.userId = userId;
//    }

    public UUID getId() {
        return id;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public String getDescription() {
        return description;
    }

    public String getStatus() {
        return status;
    }

    public UUID getUserId() {
        return userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Journal journal = (Journal) o;
        return timeStamp == journal.timeStamp &&
                id.equals(journal.id) &&
                description.equals(journal.description) &&
                status.equals(journal.status) &&
                userId.equals(journal.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, timeStamp, description, status, userId);
    }

    @Override
    public String toString() {
        return "Journal{" +
                "id=" + id +
                ", timeStamp=" + timeStamp +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                ", userId=" + userId +
                '}';
    }
}
