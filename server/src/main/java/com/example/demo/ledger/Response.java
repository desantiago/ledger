package com.example.demo.ledger;

import java.util.Objects;

public class Response {
    private String message;
    private String id;
    private boolean success;

    public Response(String message, String id, boolean success) {
        this.message = message;
        this.id = id;
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public String getId() {
        return id;
    }

    public boolean isSuccess() {
        return success;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Response response = (Response) o;
        return success == response.success &&
                message.equals(response.message) &&
                id.equals(response.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message, id, success);
    }

    @Override
    public String toString() {
        return "Response{" +
                "message='" + message + '\'' +
                ", id='" + id + '\'' +
                ", success=" + success +
                '}';
    }
}
