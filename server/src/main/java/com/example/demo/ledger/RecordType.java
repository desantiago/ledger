package com.example.demo.ledger;

public enum RecordType {
    CREDIT,
    DEBIT
}
