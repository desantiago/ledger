package com.example.demo.ledger;

import java.math.BigDecimal;
import java.util.Objects;

public class Aggregation {
    private String text;
    private BigDecimal value;

    public Aggregation(String text, BigDecimal value) {
        this.text = text;
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public BigDecimal getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Aggregation that = (Aggregation) o;
        return Objects.equals(text, that.text) &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(text, value);
    }

    @Override
    public String toString() {
        return "Aggregation{" +
                "text='" + text + '\'' +
                ", value=" + value +
                '}';
    }
}
