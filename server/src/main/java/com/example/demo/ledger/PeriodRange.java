package com.example.demo.ledger;

import java.util.Objects;

public class PeriodRange {
    private long StartDate;
    private long EndDate;

    public PeriodRange(long startDate, long endDate) {
        StartDate = startDate;
        EndDate = endDate;
    }

    public long getStartDate() {
        return StartDate;
    }

    public long getEndDate() {
        return EndDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PeriodRange that = (PeriodRange) o;
        return StartDate == that.StartDate &&
                EndDate == that.EndDate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(StartDate, EndDate);
    }

    @Override
    public String toString() {
        return "PeriodDates{" +
                "StartDate=" + StartDate +
                ", EndDate=" + EndDate +
                '}';
    }
}
