package com.example.demo.ledger;

import java.time.LocalDate;
import java.time.ZoneId;

public class LocalDateFactory {
    public static LocalDate now(){
        return LocalDate.now(ZoneId.systemDefault());
    }
}