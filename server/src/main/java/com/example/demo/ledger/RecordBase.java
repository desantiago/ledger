package com.example.demo.ledger;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Objects;
import java.util.UUID;

public abstract class RecordBase {
    private String description;
    private String localDate;
    private String utcDate;
    private RecordType type;
    private String category;
    private String subCategory;
    private BigDecimal amount;
    private UUID userId;

    abstract static class Builder<T extends Builder<T>> {
        private String description;
        private String localDate;
        private String utcDate;
        private RecordType type;
        private BigDecimal amount;
        private String category;
        private String subCategory;
        private UUID userId;

        public T withDescription(String description) {
            this.description = description; return self();
        }

        public T withLocalDate(String localDate) {
            this.localDate = localDate; return self();
        }

        public T withUtcDate(String utcDate) {
            this.utcDate = utcDate; return self();
        }

        public T withType(RecordType type) {
            this.type = type; return self();
        }

        public T withAmount(BigDecimal amount) {
            this.amount = amount; return self();
        }

        public T withCategory(String category) {
            this.category = category; return self();
        }

        public T withSubCategory(String subCategory) {
            this.subCategory = subCategory; return self();
        }

        public T withUserId(UUID userId) {
            this.userId = userId; return self();
        }

        abstract RecordBase build();
        protected abstract T self();
    }

    RecordBase(Builder<?> builder) {
        Objects.requireNonNull(builder.description);
        Objects.requireNonNull(builder.localDate);
        Objects.requireNonNull(builder.utcDate);
        Objects.requireNonNull(builder.type);
        Objects.requireNonNull(builder.amount);
        Objects.requireNonNull(builder.userId);

        description = String.valueOf(builder.description);
//        date = Instant.ofEpochMilli(builder.date.toEpochMilli());
        localDate = builder.localDate;
        utcDate = builder.utcDate;
        type = builder.type;
        amount = new BigDecimal(builder.amount.toString());
        category = String.valueOf(builder.category);
        subCategory = String.valueOf(builder.subCategory);
        userId = UUID.fromString(builder.userId.toString());
    }

    public String getDescription() {
        return description;
    }

//    public Instant getDate() {
//        return date;
//    }
    public String getLocalDate() { return localDate; }
    public String getUtcDate() { return utcDate; }

    public RecordType getType() {
        return type;
    }

    public String getCategory() {
        return category;
    }
    public String getMonthYear() {
        Instant date = Instant.ofEpochMilli(Long.parseLong(utcDate));
        return date.atZone(ZoneId.of("UTC")).getMonth().toString()+date.atZone(ZoneId.of("UTC")).getYear();
    }

    public String getSubCategory() {
        return subCategory;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public UUID getUserId() {
        return userId;
    }

    public BigDecimal getAmountSum() {
        return type.equals(RecordType.CREDIT)
                ? amount.negate()
                : amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecordBase that = (RecordBase) o;
        return localDate == that.localDate &&
                utcDate == that.utcDate &&
                description.equals(that.description) &&
                type == that.type &&
                Objects.equals(category, that.category) &&
                Objects.equals(subCategory, that.subCategory) &&
                amount.equals(that.amount) &&
                userId.equals(that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, localDate, utcDate, type, category, subCategory, amount, userId);
    }

    @Override
    public String toString() {
        return "RecordBase{" +
                "description='" + description + '\'' +
                ", localDate=" + localDate +
                ", utcDate=" + utcDate +
                ", type=" + type +
                ", category='" + category + '\'' +
                ", subCategory='" + subCategory + '\'' +
                ", amount=" + amount +
                ", userId=" + userId +
                '}';
    }
}
