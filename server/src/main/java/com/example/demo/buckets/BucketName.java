package com.example.demo.buckets;

public enum BucketName {
    WAV_BUCKET("bucket-fourier-images");

    private final String bucketName;

    BucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getBucketName() {
        return bucketName;
    }
}
