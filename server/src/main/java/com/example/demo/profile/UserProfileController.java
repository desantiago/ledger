package com.example.demo.profile;

import com.example.demo.lambdas.Lambdas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/user-profile")
@CrossOrigin("*")
public class UserProfileController {

    private final UserProfileService userProfileService;
    private final Lambdas lambdas;

    @Autowired
    public UserProfileController(UserProfileService userProfileService, Lambdas lambdas) {
        this.userProfileService = userProfileService;
        this.lambdas = lambdas;
    }

    @GetMapping
    public List<UserProfile> getUserProfiles() {
//        lambdas.testLambda();
//        userProfileService.addUser(userProfileService.getUserProfiles().get(0));
        return userProfileService.getUserProfiles();
    }

    @PostMapping(
            path = "/{userProfileId}/image/upload",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public void uploadUserProfileImage(@PathVariable("userProfileId") UUID userProfileId,
                                       @RequestParam("file") MultipartFile file) {
        userProfileService.uploadUserProfileImage(userProfileId, file);
    }

    @GetMapping("/{userProfileId}/image/download")
    public byte[] downloadUserProfileImage(@PathVariable("userProfileId") UUID userProfileId) {
        return userProfileService.downloadUserProfileImage(userProfileId);
    }

    @GetMapping("/check")
    public long check() throws IOException, InterruptedException {
        return userProfileService.checkRecordCount();
    }

    @GetMapping("/avis")
    public void avis() throws InterruptedException, IOException {
        userProfileService.checkAvisTable();
    }

    @GetMapping("/testlambda")
    public void testlambda() {
        lambdas.testLambda();
//        userProfileService.addUser(userProfileService.getUserProfiles().get(0));
//        return userProfileService.getUserProfiles();
    }
}
