package com.example.demo.profile;

import com.example.demo.buckets.BucketName;
import com.example.demo.filestore.FileStore;
import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;
import sun.jvm.hotspot.ui.tree.SimpleTreeNode;

import java.io.IOException;
import java.util.*;

@Service
public class UserProfileService {
    private final UserProfileDataAccessService userProfileDataAccessService;
    private final FileStore fileStore;

    @Autowired
    public UserProfileService(UserProfileDataAccessService userProfileDataAccessService, FileStore fileStore) {
        this.userProfileDataAccessService = userProfileDataAccessService;
        this.fileStore = fileStore;
    }

    List<UserProfile> getUserProfiles() {
        return userProfileDataAccessService.getUserProfiles();
    }

    public void addUser(UserProfile userProfile) {
        userProfileDataAccessService.addUser(userProfile);
    }

    public void uploadUserProfileImage(UUID userProfileId, MultipartFile file) {
        // Check if file is empty
        // Check if file is image
        // Check if user exists in database
        // Grab metada from file if any
        // Store the image on S3 and update the database
        if (file.isEmpty()) {
            throw new IllegalStateException("Cannot upload empty file");
        }
        if (!Arrays.asList(ContentType.IMAGE_JPEG.getMimeType(), ContentType.IMAGE_PNG.getMimeType()).contains(file.getContentType())) {
            throw new IllegalStateException("file is not image");
        }
        UserProfile user = getUserProfile(userProfileId);

        Map<String, String> metaData = new HashMap<>();
        metaData.put("Content-type", file.getContentType());
        metaData.put("Content-length", String.valueOf(file.getSize()));

        String path = String.format("%s/%s", BucketName.WAV_BUCKET.getBucketName(), user.getUserProfileId());
        String filename = String.format("%s-%s", file.getOriginalFilename(), UUID.randomUUID());
        try {
            fileStore.save(path, filename, Optional.of(metaData), file.getInputStream());
        }
        catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private UserProfile getUserProfile(UUID userProfileId) {
        return userProfileDataAccessService
                    .getUserProfiles()
                    .stream()
                    .filter(userProfile -> userProfile.getUserProfileId().equals(userProfileId))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("User Profile not %d found", userProfileId)));
    }

    public byte[] downloadUserProfileImage(UUID userProfileId) {
        UserProfile userProfile = getUserProfile(userProfileId);
        String path = String.format("%s/%s",
                BucketName.WAV_BUCKET.getBucketName(),
                userProfile.getUserProfileId());

        return userProfile.getUserProfileImageLink()
                .map(key -> fileStore.download(path, key))
                .orElse(new byte[0]);
    }

    public long checkRecordCount() throws IOException, InterruptedException {
        return userProfileDataAccessService.checkRecordCount();
    }

    public void checkAvisTable() throws InterruptedException, IOException {
        userProfileDataAccessService.checkAvisTable();
    }
}