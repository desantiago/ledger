package com.example.demo.profile;

import com.example.demo.datastore.FakeUserProfileDataStore;
import com.example.demo.datastore.UserProfileDynamoDataStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.List;

@Repository
public class UserProfileDataAccessService {
    private final UserProfileDynamoDataStore fakeUserProfileDataStore;

    @Autowired
    public UserProfileDataAccessService(UserProfileDynamoDataStore fakeUserProfileDataStore) {
        this.fakeUserProfileDataStore = fakeUserProfileDataStore;
    }

    List<UserProfile> getUserProfiles() {
        return fakeUserProfileDataStore.getUserProfiles();
    }

    public void addUser(UserProfile userProfile) {
        fakeUserProfileDataStore.addUser(userProfile);
    }

    public long checkRecordCount() throws IOException, InterruptedException {
        return fakeUserProfileDataStore.checkRecordCount();
    }

    public void checkAvisTable() throws InterruptedException, IOException {
        fakeUserProfileDataStore.checkAvisTable();
    }

}
