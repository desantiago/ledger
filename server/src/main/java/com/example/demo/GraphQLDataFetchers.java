package com.example.demo;

import com.example.demo.datastore.FakeLedgerDataStore;
import com.example.demo.ledger.*;
import com.example.demo.utils.Utils;
import com.google.common.collect.ImmutableMap;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class GraphQLDataFetchers {
    private static List<Map<String, String>> books = Arrays.asList(
            ImmutableMap.of("id", "book-1",
                    "name", "Harry Potter and the Philosopher's Stone",
                    "pageCount", "223",
                    "authorId", "author-1"),
            ImmutableMap.of("id", "book-2",
                    "name", "Moby Dick",
                    "pageCount", "635",
                    "authorId", "author-2"),
            ImmutableMap.of("id", "book-3",
                    "name", "Interview with the vampire",
                    "pageCount", "371",
                    "authorId", "author-3")
    );

    private static List<Map<String, String>> authors = Arrays.asList(
            ImmutableMap.of("id", "author-1",
                    "firstName", "Joanne",
                    "lastName", "Rowling"),
            ImmutableMap.of("id", "author-2",
                    "firstName", "Herman",
                    "lastName", "Melville"),
            ImmutableMap.of("id", "author-3",
                    "firstName", "Anne",
                    "lastName", "Rice")
    );

    private final LedgerService ledgerService;

    public GraphQLDataFetchers(LedgerService ledgerService) {
        this.ledgerService = ledgerService;
    }

    public DataFetcher getBookByIdDataFetcher() {
//        System.out.println("getBookByIdDataFetcher");
        return dataFetchingEnvironment -> {
            System.out.println("getBookByIdDataFetcher --request");
            String bookId = dataFetchingEnvironment.getArgument("id");
            return books
                    .stream()
                    .filter(book -> book.get("id").equals(bookId))
                    .findFirst()
                    .orElse(null);
        };
    }

    public DataFetcher getAuthorDataFetcher() {
//        System.out.println("getAuthorDataFetcher");
        return dataFetchingEnvironment -> {
//            System.out.println("getAuthorDataFetcher --request");
//            WebRequest webRequest = dataFetchingEnvironment.getContext();
//            System.out.println("In the fetcher"+ webRequest.getHeader("host"));
            Map<String,String> book = dataFetchingEnvironment.getSource();
            String authorId = book.get("authorId");
            return authors
                    .stream()
                    .filter(author -> author.get("id").equals(authorId))
                    .findFirst()
                    .orElse(null);
        };
    }

    public DataFetcher getAllJournalsDataFetcher() {
        return dataFetchingEnvironment -> ledgerService.getAllJournals(FakeLedgerDataStore.USER_ID);
    }

    public DataFetcher getJournalsByUserIdDataFetcher() {
//        System.out.println("getJournalsByUserIdDataFetcher");
        return dataFetchingEnvironment -> {
            System.out.println("getJournalsByUserIdDataFetcher --request");
            String userId = dataFetchingEnvironment.getArgument("id");
            return ledgerService.getAllJournals(UUID.fromString(userId));
        };
    }

    public DataFetcher getRecordsByJournalIdDataFetcher() {
//        System.out.println("getJournalsByUserIdDataFetcher");
        return dataFetchingEnvironment -> {
            System.out.println("getRecordsByJournalIdDataFetcher --request");
            String journalId = dataFetchingEnvironment.getArgument("id");
            return ledgerService.getJournalRecords(UUID.fromString(journalId));
        };
    }

    public DataFetcher getRecordsByJournalIdAndDateRangeDataFetcher() {
        return dataFetchingEnvironment -> {
            System.out.println("getRecordsByJournalIdAndDateRangeDataFetcher --request");
            String journalId = dataFetchingEnvironment.getArgument("id");
            String startDate = dataFetchingEnvironment.getArgument("startDate");
            String endDate = dataFetchingEnvironment.getArgument("endDate");

            Optional<Long> startDateLong = Utils.parseLong(startDate);
            Optional<Long> endDateLong = Utils.parseLong(endDate);

            if (!startDateLong.isPresent()) return Collections.EMPTY_LIST;
            if (!endDateLong.isPresent()) return Collections.EMPTY_LIST;

            return ledgerService.getJournalRecordsByDateRange(
                    UUID.fromString(journalId),
                    startDateLong.get(),
                    endDateLong.get());
        };
    }

    public DataFetcher getRecordsDataFetcher() {
//        System.out.println("getJournalsByUserIdDataFetcher");
        return dataFetchingEnvironment -> {
//            System.out.println("getRecordsByJournalIdDataFetcher --request");
//            String journalId = dataFetchingEnvironment.getArgument("id");
            Journal journal = dataFetchingEnvironment.getSource();
            return ledgerService.getJournalRecords(journal.getId());

        };
    }

    public DataFetcher insertJournalDataFetcher() {
        return new DataFetcher() {
            @Override
            public Response get(DataFetchingEnvironment environment) {
                //
                // The graphql specification dictates that input object arguments MUST
                // be maps.  You can convert them to POJOs inside the data fetcher if that
                // suits your code better
                //
                // See http://facebook.github.io/graphql/October2016/#sec-Input-Objects
                //
//                Map<String, Object> episodeInputMap = environment.getArgument("episode");
//                Map<String, Object> reviewInputMap = environment.getArgument("review");
                String description = environment.getArgument("description");
                String status = environment.getArgument("status");
                System.out.println(description);
                System.out.println(status);
                //
                // in this case we have type safe Java objects to call our backing code with
                //
//                EpisodeInput episodeInput = EpisodeInput.fromMap(episodeInputMap);
//                ReviewInput reviewInput = ReviewInput.fromMap(reviewInputMap);

                // make a call to your store to mutate your database
//                Review updatedReview = reviewStore().update(episodeInput, reviewInput);
                UUID uuid = ledgerService.addJournal(description, status, FakeLedgerDataStore.USER_ID);
                System.out.println(uuid);
                // this returns a new view of the data
//                return updatedReview;

                return new Response("", uuid.toString(), true);
            }
        };
    }

    public DataFetcher insertRecordDataFetcher() {
        return new DataFetcher() {
            @Override
            public Response get(DataFetchingEnvironment environment) {
                String journalId = environment.getArgument("journalId");
                String description = environment.getArgument("description");
                String category = environment.getArgument("category");
                String subCategory = environment.getArgument("subCategory");
                RecordType type = RecordType.valueOf(environment.getArgument("type"));
                Double d = environment.getArgument("amount");
                BigDecimal amount = BigDecimal.valueOf(d);
                UUID uuid = ledgerService.addRecord(
                        UUID.fromString(journalId),
                        description, category,
                        subCategory,
                        type,
                        amount,
                        FakeLedgerDataStore.USER_ID);
                System.out.println(uuid);
                return new Response("", uuid.toString(), true);
            }
        };
    }

    public DataFetcher getTotalCategoryByJournalIdDataFetcher() {
        return new DataFetcher() {
            @Override
            public List<Aggregation> get(DataFetchingEnvironment environment) {
                String journalId = environment.getArgument("journalId");
                Optional<Map<String, BigDecimal>> records = ledgerService.getSumByCategory(UUID.fromString(journalId));
                if (records.isPresent()) {
                    return records.get()
                            .entrySet()
                            .stream()
                            .map(entry ->  new Aggregation(entry.getKey(), entry.getValue()))
                            .collect(Collectors.toList());
                }
                return Collections.EMPTY_LIST;
            }
        };
    }
}
