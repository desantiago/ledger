package com.example.demo.lambdas;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

/**
 * <h4>
 * SimpleLambdaMessage
 * </h4>
 * <p>
 * A class for sending message strings to an Amazon Web Services Lambda Function
 * </p>
 * <p>
 * Feb 19, 2018
 * </p>
 *
 * @author Ian Kaplan, iank@bearcave.com
 */
class SimpleLambdaMessage {
    private String mMessage;

    /**
     * @param message the message contents for the object
     */
    public void setMessage(final String message) {
        this.mMessage = message;
    }

    /**
     * @return the message string
     */
    public String getMessage() {
        return this.mMessage;
    }

    @Override
    public String toString() {
        return this.mMessage;
    }
}

@Service
public class Lambdas {

    private final AWSLambda lambdaClient;

    @Autowired
    public Lambdas(AWSLambda lambdaClient) {
        this.lambdaClient = lambdaClient;
    }

    private static String objectToJSON( Object obj) {
        String json = "";
        try {
            ObjectMapper mapper = new ObjectMapper();
            json = mapper.writeValueAsString(obj);
        } catch (JsonGenerationException e) {
//            logger.severe("Object to JSON failed: " + e.getLocalizedMessage());
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }

    public void testLambda() {
//        final String aws_access_key_id = "Your AWS ID goes here";
//        final String aws_secret_access_key = "Your AWS secret key goes here";
//        final Logger logger = Logger.getLogger( this.getClass().getName() );
//        final String messageToLambda = "Hello Lambda Function";
//        final SimpleLambdaMessage messageObj = new SimpleLambdaMessage();
//        messageObj.setMessage(messageToLambda);

        HashMap<String, String> message = new HashMap<String, String>() {{
            put("x", "1");
            put("y", "2");
        }};
//        HashMap<String, String> message = new HashMap<String, String>() {{
//            put("key1", "1");
//            put("key2", "2");
//        }};

        String lambdaMessageJSON = objectToJSON( message );
        InvokeRequest req = new InvokeRequest()
//                .withFunctionName("RafaeldoHelloNativeAWSLambda-LambdaFunction-129CLWLR0Q6VP")
                .withFunctionName("CombeeTestLambda-Multiply-1BN6EJV8B6RZF")
                .withPayload(lambdaMessageJSON); // optional

//        new InvokeRequest().
        InvokeResult requestResult = lambdaClient.invoke(req);
        ByteBuffer byteBuf = requestResult.getPayload();
        if (byteBuf != null) {
            String result = StandardCharsets.UTF_8.decode(byteBuf).toString();
            System.out.println(result);
        }

//        String lambdaMessageJSON = objectToJSON( messageObj );
//        InvokeRequest req = new InvokeRequest()
//                .withFunctionName(SimpleLambdaFunctionExample
//                        .lambdaFunctionName)
//                .withPayload( lambdaMessageJSON );
//        InvokeResult requestResult = lambdaClient.invoke(req);
//        ByteBuffer byteBuf = requestResult.getPayload();
//        if (byteBuf != null) {
//            String result = StandardCharsets.UTF_8.decode(byteBuf).toString();
////            logger.info("testLambdaFunction::Lambda result: " + result);
//        } else {
//            logger.severe("testLambdaFunction: result payload is null");
//        }
    }
}
