package com.example.demo;

import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.spring.web.servlet.GraphQLInvocation;
import graphql.spring.web.servlet.GraphQLInvocationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;

import java.util.concurrent.CompletableFuture;

@Component
@Primary
public class GraphQLCustom implements GraphQLInvocation {
    @Autowired
    GraphQL graphQL;
    @Override
    public CompletableFuture<ExecutionResult> invoke(GraphQLInvocationData invocationData, WebRequest webRequest) {
        ExecutionInput executionInput = ExecutionInput.newExecutionInput()
                .query(invocationData.getQuery())
                .context(webRequest)
                .operationName(invocationData.getOperationName())
                .variables(invocationData.getVariables())
                .build();
        System.out.println(webRequest.getHeader("token"));
        System.out.println("hello");

        return graphQL.executeAsync(executionInput);
    }
}