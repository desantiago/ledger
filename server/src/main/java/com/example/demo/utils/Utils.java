package com.example.demo.utils;

import java.util.Optional;

public class Utils {

    public static Optional<Long> parseLong(String input) {
        try {
            return Optional.of(Long.parseLong(input));
        } catch (NumberFormatException ex) {
            return Optional.empty();
        }
    }

}
